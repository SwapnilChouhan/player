package com.theoptimumlabs.playermarket.Wallet;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mercadopago.android.px.core.MercadoPagoCheckout;
import com.mercadopago.android.px.model.Payment;
import com.mercadopago.android.px.model.exceptions.MercadoPagoError;
import com.theoptimumlabs.playermarket.HomeActivity;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.SessionManagement.Session;
import com.theoptimumlabs.playermarket.utilsadded.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Wallet extends BaseActivity {
    private static final int REQUEST_CODE = 1;
    Button btncheckout, btnaddmoney;
    EditText edit_sellprice;
    private FirebaseFirestore firestoreDB;
    Session session;
    String user_id, user_ses_name, user_ses_img, userrole;
    HashMap<String, String> hashMap = new HashMap<>();
    SharedPreferences sharedpreferences;
    public static final String mypreference = "myprefamt";
    public static final String AMT = "amtKey";
    String oldamt;
    int textlength;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        try {
            System.gc();
        }catch (Exception e){}

        session = new Session(getApplicationContext());
        if (session.isLoggedIn() == true){
            session.checkLogin();
            hashMap = session.getUser();
            user_id = hashMap.get(session.KEY_User_id);
            user_ses_name = hashMap.get(session.KEY_NAME);
            user_ses_img = hashMap.get(session.KEY_IMAGE_URL);
            userrole = hashMap.get(session.KEY_Mobile);

        }
            sharedpreferences = getSharedPreferences(mypreference,
                    Context.MODE_PRIVATE);

        edit_sellprice = (EditText)findViewById(R.id.edit_sellprice);

        btnaddmoney = (Button)findViewById(R.id.btnaddmoney);
        btncheckout = (Button)findViewById(R.id.btncheckout);

        if(sharedpreferences.contains(AMT)) {
            btnaddmoney.setBackground(getResources().getDrawable(R.drawable.cyan_cardbox));

            oldamt = sharedpreferences.getString(AMT, "");
            edit_sellprice.setText(oldamt + "");

        }else {
            btnaddmoney.setBackground(getResources().getDrawable(R.drawable.rectbglightgrey));

        }
        if(oldamt.equals("0") || oldamt.equals("") || oldamt.isEmpty())
        {
            btnaddmoney.setBackground(getResources().getDrawable(R.drawable.rectbglightgrey));

        }

        firestoreDB = FirebaseFirestore.getInstance();

        btnaddmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edit_sellprice.getText().toString().equals("")) {
                    btnaddmoney.setBackground(getResources().getDrawable(R.drawable.cyan_cardbox));
                    String curamt;
                    Double ttlamt;
                    curamt = edit_sellprice.getText().toString();
                    ttlamt = Double.parseDouble(oldamt) + Double.parseDouble(curamt);
                    addAmt(String.valueOf(ttlamt), user_id, "later");
                }else {
                    edit_sellprice.setError("Please Add Amount");
                }
            }
        });
        btncheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMercadoPagoCheckout("243962506-0bb62e22-5c7b-425e-a0a6-c22d0f4758a9");
//                startMercadoPagoCheckout("243962506-0bb62e22-5c7b-425e-a0a6-c22d0f4758a9");
//                startMercadoPagoCheckout("243962506-0bb62e22-5c7b-425e-a0a6-c22d0f4758a9");
            }
        });

        edit_sellprice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textlength = edit_sellprice.getText().length();
                if(textlength >= 1){
                    btnaddmoney.setBackground(getResources().getDrawable(R.drawable.cyan_cardbox));

                }else {
                    btnaddmoney.setBackground(getResources().getDrawable(R.drawable.rectbglightgrey));

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void addAmt(final String amt, String wid, final String role) {
        wid = "W"+user_id;
//        Map<String, Object> note = new Note(title, content).toMap();
        Map<String, Object> data1 = new HashMap<>();
        data1.put("Id", wid);
        data1.put("amt", amt);
        firestoreDB.collection("UserInfo").document(user_id).collection("Wallet").document(wid)
                .set(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(AMT, amt);
                editor.commit();

                if(role.equals("initial")){

                }else {
                    addAmtHistory(user_id, amt);
                    ShowNormalmsg("Amount Added in wallet.");
                }
            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sonaliwa", "Error adding Note document", e);
                        Toast.makeText(getApplicationContext(), "Note could not be added!", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void startMercadoPagoCheckout(final String checkoutPreferenceId) {
//        new MercadoPagoCheckout.Builder("TEST-e89b6e10-6bba-49da-87e4-cf081109b476", checkoutPreferenceId).build()
//                .startPayment(Wallet.this, REQUEST_CODE);


        new MercadoPagoCheckout.Builder("TEST-e89b6e10-6bba-49da-87e4-cf081109b476",
                checkoutPreferenceId)
                .setPrivateKey("TEST-4764561719001581-040109-d22d71f553f79af6af151553fed9aa23-420991462").build()
                .startPayment(this, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == MercadoPagoCheckout.PAYMENT_RESULT_CODE) {
                final Payment payment = (Payment) data.getSerializableExtra(MercadoPagoCheckout.EXTRA_PAYMENT_RESULT);
                Log.e("sonapay", " "+payment.getPaymentStatusDetail()+" "+payment.getPaymentStatus());
                ((TextView) findViewById(R.id.mp_results)).setText("Resultado del pago: " + payment.getPaymentStatus());
                //Done!
            } else if (resultCode == RESULT_CANCELED) {
                if (data != null && data.getExtras() != null
                        && data.getExtras().containsKey(MercadoPagoCheckout.EXTRA_ERROR)) {
                    final MercadoPagoError mercadoPagoError =
                            (MercadoPagoError) data.getSerializableExtra(MercadoPagoCheckout.EXTRA_ERROR);
                    ((TextView) findViewById(R.id.mp_results)).setText("Error: " +  mercadoPagoError.getMessage());
                    //Resolve error in checkout
                } else {
                    //Resolve canceled checkout
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(Wallet.this, HomeActivity.class);
        startActivity(i);
        super.onBackPressed();
    }
    public void intentcall(Context from, Class to){
        Intent i = new Intent(getApplicationContext(), to);
        startActivity(i);
    }


    private void addAmtHistory(String id,  String amt) {

//        Map<String, Object> note = new Note(title, content).toMap();
        Map<String, Object> data1 = new HashMap<>();
//        data1.put("Id", randomId()+"");
//        data1.put("amt", amt);
        data1.put("userId", id);
        data1.put("amt", amt);
        data1.put("tranctTime", timforsave());

//        data1.put("Id", id);
        firestoreDB.collection("UserInfo").document(user_id).collection("Wallethistory").document(timestamp()+"")
                .set(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
//                ShowNormalmsg("Added in Portfolio.");
//                intentcall(getApplicationContext(), HomeActivity.class);
                startMercadoPagoCheckout("243962506-0bb62e22-5c7b-425e-a0a6-c22d0f4758a9");

            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sonaliwa", "Error adding amt history document", e);
//                        Toast.makeText(context, "Note could not be added!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public String timestamp(){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        return ts;
    }
    public String timforsave(){
        Date date = new Date();
//        long time = date.getTime();
        Long time = System.currentTimeMillis();

        System.out.println("time in miliseconds  "+time);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        String datestring = formatter.format(new Date(time));
        Log.e("ttime", datestring+" d");
        return time.toString();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        getAmt(edit_sellprice.getText().toString(), user_id, "initial");

    }
}
