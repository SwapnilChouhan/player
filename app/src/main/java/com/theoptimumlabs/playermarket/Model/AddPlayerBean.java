package com.theoptimumlabs.playermarket.Model;

import android.util.Log;

import java.io.Serializable;

public class AddPlayerBean implements Serializable,  Comparable<AddPlayerBean>{
   public String id;
    public String p_n;
    public String t_n;
    public String cntry;
    public String ption;
    public String p_side;
    public String b_date;
    public String wht;
    public String higt;
    public String srtno;
    public String b_plce;
    public String img;
    public String spri;
    public String price;
    public String shr;
    public int intprice;
    public String getPreshr() {
        return preshr;
    }

    public void setPreshr(String preshr) {
        this.preshr = preshr;
    }

    public String preshr;


    public int getIntprice() {
        return intprice;
    }

    public void setIntprice(int intprice) {
        this.intprice = intprice;
    }


    public String getShr() {
        return shr;
    }

    public void setShr(String shr) {
        this.shr = shr;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public String getSpri() {
        return spri;
    }

    public void setSpri(String spri) {
        this.spri = spri;
    }



    public AddPlayerBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getP_n() {
        return p_n;
    }

    public void setP_n(String p_n) {
        this.p_n = p_n;
    }

    public String getT_n() {
        return t_n;
    }

    public void setT_n(String t_n) {
        this.t_n = t_n;
    }

    public String getCntry() {
        return cntry;
    }

    public void setCntry(String cntry) {
        this.cntry = cntry;
    }

    public String getPtion() {
        return ption;
    }

    public void setPtion(String ption) {
        this.ption = ption;
    }

    public String getP_side() {
        return p_side;
    }

    public void setP_side(String p_side) {
        this.p_side = p_side;
    }

    public String getB_date() {
        return b_date;
    }

    public void setB_date(String b_date) {
        this.b_date = b_date;
    }

    public String getWht() {
        return wht;
    }

    public void setWht(String wht) {
        this.wht = wht;
    }

    public String getHigt() {
        return higt;
    }

    public void setHigt(String higt) {
        this.higt = higt;
    }

    public String getSrtno() {
        return srtno;
    }

    public void setSrtno(String srtno) {
        this.srtno = srtno;
    }

    public String getB_plce() {
        return b_plce;
    }

    public void setB_plce(String b_plce) {
        this.b_plce = b_plce;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public AddPlayerBean(String id, String p_n, String t_n , String cntry, String ption,
                         String p_side, String b_date, String wht, String higt, String srtno,
                         String b_plce, String img) {
        this.id = id;
        this.p_n = p_n;
        this.t_n = t_n;
        this.cntry = cntry;
        this.ption = ption;
        this.p_side = p_side;
        this.b_date = b_date;
        this.wht = wht;
        this.higt = higt;
        this.srtno = srtno;
        this.b_plce = b_plce;
        this.img = img;
    }


    public int compareTo(AddPlayerBean compareFruit) {

        int compareQuantity = ((AddPlayerBean) compareFruit).getIntprice();
        Log.e("sonalcom", compareQuantity+ " "+this.intprice);

        //ascending order
//        return this.intprice - compareQuantity;

        //descending order
        return compareQuantity - this.intprice;

    }


}
