package com.theoptimumlabs.playermarket.Model;

import java.io.Serializable;

public class PortfolioBean implements Serializable {
    public String id;
    public String pn;
    public String t_n;
    public String cntry;
    public String ption;
    public String p_side;
    public String b_date;

    public String wht;
    public String higt;
    public String srtno;
    public String b_plce;
    public String pimg;
    public String shr;

    public String getPreshr() {
        return preshr;
    }

    public void setPreshr(String preshr) {
        this.preshr = preshr;
    }

    public String preshr;
    public String buyp;
    public String ttlbp;
    public String bremain;
    public String pid;
    public String sigdif;
    public String ttldif;
    public String curbprce;
    public String ttlCbprce;
    public String diffbprce;
    public String diffttlbpri;
    public String sellp;
    public String getT_n() {
        return t_n;
    }

    public void setT_n(String t_n) {
        this.t_n = t_n;
    }
    public String getCntry() {
        return cntry;
    }

    public void setCntry(String cntry) {
        this.cntry = cntry;
    }

    public String getPtion() {
        return ption;
    }

    public void setPtion(String ption) {
        this.ption = ption;
    }

    public String getP_side() {
        return p_side;
    }

    public void setP_side(String p_side) {
        this.p_side = p_side;
    }

    public String getB_date() {
        return b_date;
    }

    public void setB_date(String b_date) {
        this.b_date = b_date;
    }

    public String getWht() {
        return wht;
    }

    public void setWht(String wht) {
        this.wht = wht;
    }

    public String getHigt() {
        return higt;
    }

    public void setHigt(String higt) {
        this.higt = higt;
    }

    public String getSrtno() {
        return srtno;
    }

    public void setSrtno(String srtno) {
        this.srtno = srtno;
    }

    public String getB_plce() {
        return b_plce;
    }

    public void setB_plce(String b_plce) {
        this.b_plce = b_plce;
    }
    public String getCurbprce() {
        return curbprce;
    }

    public String getSigdif() {
        return sigdif;
    }

    public void setSigdif(String sigdif) {
        this.sigdif = sigdif;
    }

    public String getTtldif() {
        return ttldif;
    }

    public void setTtldif(String ttldif) {
        this.ttldif = ttldif;
    }

    public void setCurbprce(String curbprce) {
        this.curbprce = curbprce;
    }



    public String getTtlCbprce() {
        return ttlCbprce;
    }

    public void setTtlCbprce(String ttlCbprce) {
        this.ttlCbprce = ttlCbprce;
    }

    public String getDiffbprce() {
        return diffbprce;
    }

    public void setDiffbprce(String diffbprce) {
        this.diffbprce = diffbprce;
    }

    public String getDiffttlbpri() {
        return diffttlbpri;
    }

    public void setDiffttlbpri(String diffttlbpri) {
        this.diffttlbpri = diffttlbpri;
    }

    public String getSellp() {
        return sellp;
    }

    public void setSellp(String sellp) {
        this.sellp = sellp;
    }

    public PortfolioBean() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPn() {
        return pn;
    }

    public void setPn(String pn) {
        this.pn = pn;
    }

    public String getPimg() {
        return pimg;
    }

    public void setPimg(String pimg) {
        this.pimg = pimg;
    }

    public String getShr() {
        return shr;
    }

    public void setShr(String shr) {
        this.shr = shr;
    }

    public String getBuyp() {
        return buyp;
    }

    public void setBuyp(String buyp) {
        this.buyp = buyp;
    }

    public String getTtlbp() {
        return ttlbp;
    }

    public void setTtlbp(String ttlbp) {
        this.ttlbp = ttlbp;
    }

    public String getBremain() {
        return bremain;
    }

    public void setBremain(String bremain) {
        this.bremain = bremain;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public PortfolioBean(String id, String pn, String pimg, String shr, String buyp, String ttlbp, String bremain, String pid) {
        this.id = id;
        this.pn = pn;
        this.pimg = pimg;
        this.shr = shr;
        this.buyp = buyp;
        this.ttlbp = ttlbp;
        this.bremain = bremain;
        this.pid = pid;
    }
}
