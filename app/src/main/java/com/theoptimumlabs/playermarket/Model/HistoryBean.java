package com.theoptimumlabs.playermarket.Model;

import java.io.Serializable;

public class HistoryBean implements Serializable {
public String shr;
    public String buyp;
    public String sellp;
    public String pn;
    public String timestamp;
    public String id;

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public String ttl;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String role;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShr() {
        return shr;
    }

    public void setShr(String shr) {
        this.shr = shr;
    }

    public String getBuyp() {
        return buyp;
    }

    public void setBuyp(String buyp) {
        this.buyp = buyp;
    }

    public String getSellp() {
        return sellp;
    }

    public void setSellp(String sellp) {
        this.sellp = sellp;
    }

    public String getPn() {
        return pn;
    }

    public void setPn(String pn) {
        this.pn = pn;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public HistoryBean() {
    }
}
