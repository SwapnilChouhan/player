package com.theoptimumlabs.playermarket.Model;

import java.io.Serializable;

public class SellQueueBean implements Serializable {
    public String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String buyp;
    public String pid;
    public String sellp;
    public String shr;
    public String sts;
    public String tranctdate;

    public String pn;
    public String tn;
    public String cntry;
    public String ption;
    public String pside;
    public String bdate;
    public String wht;

    public String higt;
    public String sno;
    public String sigdif;
    public String ttbp;
    public String curbprce;
    public String ttlCbprce;
    public String difbprce;
    public String difttlbpri;
    public String cselp;
    public String bplce;
    public String cshr;

    public String getCshr() {
        return cshr;
    }

    public void setCshr(String cshr) {
        this.cshr = cshr;
    }

    public String img;

    public String getTtbp() {
        return ttbp;
    }

    public void setTtbp(String ttbp) {
        this.ttbp = ttbp;
    }

    public String getPn() {
        return pn;
    }

    public void setPn(String pn) {
        this.pn = pn;
    }

    public String getTn() {
        return tn;
    }

    public void setTn(String tn) {
        this.tn = tn;
    }

    public String getCntry() {
        return cntry;
    }

    public void setCntry(String cntry) {
        this.cntry = cntry;
    }

    public String getPtion() {
        return ption;
    }

    public void setPtion(String ption) {
        this.ption = ption;
    }

    public String getPside() {
        return pside;
    }

    public void setPside(String pside) {
        this.pside = pside;
    }

    public String getBdate() {
        return bdate;
    }

    public void setBdate(String bdate) {
        this.bdate = bdate;
    }

    public String getWht() {
        return wht;
    }

    public void setWht(String wht) {
        this.wht = wht;
    }

    public String getHigt() {
        return higt;
    }

    public void setHigt(String higt) {
        this.higt = higt;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getSigdif() {
        return sigdif;
    }

    public void setSigdif(String sigdif) {
        this.sigdif = sigdif;
    }



    public String getCurbprce() {
        return curbprce;
    }

    public void setCurbprce(String curbprce) {
        this.curbprce = curbprce;
    }

    public String getTtlCbprce() {
        return ttlCbprce;
    }

    public void setTtlCbprce(String ttlCbprce) {
        this.ttlCbprce = ttlCbprce;
    }

    public String getDifbprce() {
        return difbprce;
    }

    public void setDifbprce(String difbprce) {
        this.difbprce = difbprce;
    }

    public String getDifttlbpri() {
        return difttlbpri;
    }

    public void setDifttlbpri(String difttlbpri) {
        this.difttlbpri = difttlbpri;
    }

    public String getCselp() {
        return cselp;
    }

    public void setCselp(String cselp) {
        this.cselp = cselp;
    }

    public String getBplce() {
        return bplce;
    }

    public void setBplce(String bplce) {
        this.bplce = bplce;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }






    public SellQueueBean() {
    }



    public String getBuyp() {
        return buyp;
    }

    public void setBuyp(String buyp) {
        this.buyp = buyp;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getSellp() {
        return sellp;
    }

    public void setSellp(String sellp) {
        this.sellp = sellp;
    }

    public String getShr() {
        return shr;
    }

    public void setShr(String shr) {
        this.shr = shr;
    }

    public String getSts() {
        return sts;
    }

    public void setSts(String sts) {
        this.sts = sts;
    }

    public String getTranctdate() {
        return tranctdate;
    }

    public void setTranctdate(String tranctdate) {
        this.tranctdate = tranctdate;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String uid;
}
