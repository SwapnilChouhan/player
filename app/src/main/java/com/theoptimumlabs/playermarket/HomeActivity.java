package com.theoptimumlabs.playermarket;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.theoptimumlabs.playermarket.Fragment.HistoricalFragment;
import com.theoptimumlabs.playermarket.Fragment.HistoryAmtTab;
import com.theoptimumlabs.playermarket.Fragment.HomeFragment;
import com.theoptimumlabs.playermarket.Fragment.PortfolioFragment;
import com.theoptimumlabs.playermarket.Fragment.UnderConstruction;
import com.theoptimumlabs.playermarket.LoginRegister.MainActivity;
import com.theoptimumlabs.playermarket.SessionManagement.Session;
import com.theoptimumlabs.playermarket.Wallet.Wallet;
import com.theoptimumlabs.playermarket.utilsadded.BaseActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class HomeActivity extends BaseActivity {

//    ArrayList<Model_country> al_main = new ArrayList<>();
//    ExpandableListView ev_list;
//    CountryAdapter obj_adapter;
    String TAG = "HomeActivity";
    private DrawerLayout mDrawerLayout;
    HomeFragment fragment;
    TextView tv_name;
    RelativeLayout rl_menu, rl_menuback, rlwallet, rllogout;

    Session session;
    String user_id, user_ses_name, user_ses_img, userrole;
    HashMap<String, String> hashMap = new HashMap<>();


    ImageView addmarketmg ,proimg, portimg, historyimg;
    TextView readtxt,proimgtxt, historytxt, adprotxt, toolhead;

    public static String fromwhichacti  ;
    ViewPager viewPager;
    TabLayout tabLayout;
    FrameLayout nav_fragment;


    private Dialog m_dialog; //Dialog instance.
    private LinearLayout m_llMain;
    private ArrayList<String> personsGrpStr;
    private ArrayList<String> personsGrpStrSuggest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_search);
        try {
            System.gc();
        }catch (Exception e){}

        session = new Session(getApplicationContext());
        if (session.isLoggedIn() == true){
            session.checkLogin();
            hashMap = session.getUser();
            user_id = hashMap.get(session.KEY_User_id);
            user_ses_name = hashMap.get(session.KEY_NAME);
            user_ses_img = hashMap.get(session.KEY_IMAGE_URL);
            userrole = hashMap.get(session.KEY_Mobile);


        }

//        fn_data();
        init();

    }

    private void init() {

        getSupportActionBar().hide();
//        ev_list = (ExpandableListView) findViewById(R.id.ev_menu);

        rlwallet = (RelativeLayout) findViewById(R.id.rlwallet);
        rl_menu = (RelativeLayout) findViewById(R.id.rl_menu);
        rllogout = (RelativeLayout) findViewById(R.id.rllogout);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        /////sonali added for transparency toolbar

        addmarketmg = (ImageView)findViewById(R.id.addmarketmg);
        adprotxt = (TextView)findViewById(R.id.addprotxt);
        toolhead = (TextView)findViewById(R.id.toolhead);
        portimg = (ImageView)findViewById(R.id.portimg);
        readtxt = (TextView)findViewById(R.id.readtxt);
        historyimg = (ImageView)findViewById(R.id.historyimg);
        historytxt = (TextView)findViewById(R.id.historytxt);
        proimg = (ImageView)findViewById(R.id.proimg);
        proimgtxt = (TextView)findViewById(R.id.proimgtxt);


        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.viewpager);


        nav_fragment = (FrameLayout) findViewById(R.id.content_frame);
        nav_fragment.setVisibility(View.GONE);
        //  viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setVisibility(View.VISIBLE);
        viewPager.setOffscreenPageLimit(2);
        // viewPager.setOffscreenPageLimit(0);
        // viewPager.setAdapter(new ViewPagerAdapter1(getSupportFragmentManager(), tabLayout.getTabCount(), Home_page.this));

        //  viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        // viewPager.setCurrentItem(0);
        System.out.println("tabpogi"+tabLayout.getTabCount()+"      "+tabLayout.getSelectedTabPosition());





        int[] icons = {

                R.drawable.tranimg,
                R.drawable.tranimg,
                R.drawable.tranimg


        };




        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        viewPager.setCurrentItem(1);
        tabLayout.getTabAt(1).select();

        for (int i = 0; i < icons.length; i++) {
            tabLayout.getTabAt(i).setIcon(icons[i]);

        }

            viewPager.setCurrentItem(0);
            tabLayout.getTabAt(0).select();
            rl_menu.setVisibility(View.GONE);
            rlwallet.setVisibility(View.VISIBLE);



//        fromwhichacti = getIntent().getStringExtra("fromwhichacti");




        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                System.out.println("tabpogi"+tab.getPosition());

                viewPager.setVisibility(View.VISIBLE);
                nav_fragment.setVisibility(View.GONE);
                tabLayout.setVisibility(View.VISIBLE);
//               choose_image.setVisibility(View.GONE);
                if (tab.getPosition() == 0) {

                }
                else if (tab.getPosition() == 1) {

                } else if (tab.getPosition() == 2) {
//                    editor.putString("go_profile", "yes");
//                    editor.commit();
                 /*   choose_image1.setVisibility(View.VISIBLE);
                    ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) viewPager.getLayoutParams();
                    lp.topMargin = 0;
                    cust_text.setText(""+preferences.getString("UserName",""));
                    getSupportActionBar().show();*/
                } else if (tab.getPosition() == 3) {
//                    editor.putString("go_profile", "yes");
//                    editor.commit();
                 /*   choose_image1.setVisibility(View.VISIBLE);
                    ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) viewPager.getLayoutParams();
                    lp.topMargin = 0;
                    cust_text.setText(""+preferences.getString("UserName",""));
                    getSupportActionBar().show();*/
                }
//                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());


            }
        });




        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                nav_fragment.setVisibility(View.GONE);
                viewPager.setVisibility(View.VISIBLE);

            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPageSelected(int position) {
                nav_fragment.setVisibility(View.GONE);
                viewPager.setVisibility(View.VISIBLE);

                //  getSupportActionBar().show();
                System.out.println("fragpogi"+position);

                if (position == 0) {
                    rl_menu.setVisibility(View.GONE);
                    rllogout.setVisibility(View.GONE);
                    rlwallet.setVisibility(View.VISIBLE);


                    addmarketmg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.cyan_200));
                    adprotxt.setTextColor(getResources().getColor(R.color.cyan_200));
                    portimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    readtxt.setTextColor(getResources().getColor(R.color.white));
                    historyimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    historytxt.setTextColor(getResources().getColor(R.color.white));
                    proimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    proimgtxt.setTextColor(getResources().getColor(R.color.white));


//                    protabimg.setImageResource(R.drawable.saveinstacard);
//                    proimgtxt.setTextColor(getResources().getColor(R.color.dgray));
//                    centerimg.setImageResource(R.drawable.card);
//                    centertxt.setTextColor(getResources().getColor(R.color.dgray));
//
                }


                else if (position == 1) {
                    rl_menu.setVisibility(View.GONE);
                    rllogout.setVisibility(View.GONE);
                    rlwallet.setVisibility(View.GONE);


                    portimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.cyan_200));
                    readtxt.setTextColor(getResources().getColor(R.color.cyan_200));
                    addmarketmg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    adprotxt.setTextColor(getResources().getColor(R.color.white));
                    historyimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    historytxt.setTextColor(getResources().getColor(R.color.white));
                    proimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    proimgtxt.setTextColor(getResources().getColor(R.color.white));

                } else if (position == 2) {
                    rl_menu.setVisibility(View.GONE);
                    rllogout.setVisibility(View.GONE);
                    rlwallet.setVisibility(View.GONE);


                    historyimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.cyan_200));
                    historytxt.setTextColor(getResources().getColor(R.color.cyan_200));
                    portimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    readtxt.setTextColor(getResources().getColor(R.color.white));
                    addmarketmg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    adprotxt.setTextColor(getResources().getColor(R.color.white));
                    proimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    proimgtxt.setTextColor(getResources().getColor(R.color.white));


                }else if (position == 3) {
                    rl_menu.setVisibility(View.GONE);
                    rllogout.setVisibility(View.VISIBLE);
                    rlwallet.setVisibility(View.GONE);

                    proimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.cyan_200));
//                            R.color.cyan_200), PorterDuff.Mode.MULTIPLY);
                    proimgtxt.setTextColor(getResources().getColor(R.color.cyan_200));
                    portimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    readtxt.setTextColor(getResources().getColor(R.color.white));
                    historyimg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    historytxt.setTextColor(getResources().getColor(R.color.white));
                    addmarketmg.setColorFilter(ContextCompat.getColor(getApplicationContext(),
                            R.color.white));
                    adprotxt.setTextColor(getResources().getColor(R.color.white));


                }
                // viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });



//        obj_adapter = new CountryAdapter(HomeActivity.this, al_main);
//        ev_list.setAdapter(obj_adapter);
//        ev_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//
//            @Override
//            public boolean onGroupClick(ExpandableListView parent, View v,
//                                        int groupPosition, long id) {
//                setListViewHeight(parent, groupPosition);
//                return false;
//            }
//        });

//        setExpandableListViewHeightBasedOnChildren(ev_list);

        fragment = new HomeFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString("name", al_main.get(0).getStr_country());
//        bundle.putString("des", al_main.get(0).getAl_state().get(0).getStr_description());
//        bundle.putString("dish", al_main.get(0).getAl_state().get(0).getStr_name());
//        bundle.putString("image", al_main.get(0).getAl_state().get(0).getStr_image());
//        tv_name.setText(al_main.get(0).getStr_country());

//        fragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment, "HomeFragment").addToBackStack("null").commit();

        toolhead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nav_fragment.setVisibility(View.GONE);
                viewPager.setVisibility(View.VISIBLE);
            viewPager.setCurrentItem(0);
            tabLayout.getTabAt(0).select();
            }
        });

        rlwallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, Wallet.class);
                startActivity(i);
            }
        });
        rllogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                logoutAlert();
            }
        });




    }



    public void fn_selectedPosition(int group, int child) {

        fragment = new HomeFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString("name", al_main.get(group).getStr_country());
//        bundle.putString("des", al_main.get(group).getAl_state().get(child).getStr_description());
//        bundle.putString("dish", al_main.get(group).getAl_state().get(child).getStr_name());
//        bundle.putString("image", al_main.get(group).getAl_state().get(child).getStr_image());
//        fragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment, "HomeFragment").addToBackStack("null").commit();
        mDrawerLayout.closeDrawer(Gravity.RIGHT);

//        tv_name.setText(al_main.get(group).getStr_country());
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        finish();
        finishAffinity();
//        Intent i = new Intent(HomeActivity.this, Profile.class);
//        startActivity(i);
    }


    /*========================sonali added view pager================*/
    //////sonali added start
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setVisibility(View.VISIBLE);
        nav_fragment.setVisibility(View.GONE);



        adapter.insertNewFragment(new HomeFragment());
        //adapter.insertNewFragment(new TabExplorenew());
        adapter.insertNewFragment(new PortfolioFragment());
        adapter.insertNewFragment(new HistoryAmtTab());

        //fake center fragment, so that it creates place for raised center tab.
        adapter.insertNewFragment(new UnderConstruction());
//         adapter.insertNewFragment(new UnderConstruction());

//        adapter.insertNewFragment(new Tab_Home());
//
//       /* editor.putString("go_profile", "yes");
//        editor.commit();
//
//        Log.e("Tab Log:", "" + preferences.getString("go_profile", "no"));
//        return new Tab_Profile();*/
//
//        adapter.insertNewFragment(new Tab_Home());
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {

            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void insertNewFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }

        @Override
        public int getItemPosition(Object object) {

            return super.getItemPosition(object);
        }
    }



    public void logoutAlert(){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(HomeActivity.this);
        builder.getContext().setTheme(R.style.AlertThem);

        builder.setMessage("Are you sure you want to logout?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
//                                navigationView.getMenu().findItem(R.id.nav_login).setIcon(R.mipmap.loginbtn);
//                                navigationView.getMenu().findItem(R.id.nav_login).setTitle("Login");

                        if (session.isLoggedIn() == true)
                        {
                            session.logoutUser();
                            Intent ii = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(ii);
                            finish();
                            FirebaseAuth.getInstance().signOut();

                        }



//                            Log.e("Login out vlaue", "" + preferences.getString(Constant.LOGINVALUE, "0"));

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
//                            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//                            drawer.closeDrawer(GravityCompat.START);
                    }
                });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();

    }


}
