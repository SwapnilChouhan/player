package com.theoptimumlabs.playermarket.SessionManagement;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

/**
 * Created by Acer on 1/23/2016.
 */
public class Session {
    SharedPreferences pref, pref11;
    Editor editor, editor1;
    Context context, ocontext;
    int PRIVATE_MODE=0,
            PRIVATE_MODE1 = 0;
    private static final String Pref_Name="Loginfile";
    private static final String Pref_Name1="Orderfile";
    private static final String Is_Login="Is Logged In";
    private static final String IS_Image="Is Image In";
    public static final String KEY_User_id = "user_id";
    public static final String KEY_Email = "email";
    public static final String KEY_IMAGE_URL = "imagepath";
    //public static final String KEY_IMAGE_URL2 = "imagepath2";
    public static final String KEY_NAME ="name";
    public static final String KEY_NAME_Last ="lastname";
    public static final String KEY_LAST_NAME ="last_name";
    public static final String KEY_Mobile ="mobile_num";
    public static final String KEY_ProImgUrl ="imgurl";



    public Session(Context context) {
        this.context = context;
        pref=context.getSharedPreferences(Pref_Name,PRIVATE_MODE);
        pref11 = context.getSharedPreferences(Pref_Name1,PRIVATE_MODE1);
        editor = pref.edit();
        editor1 = pref11.edit();
    }

//    public void createLoginSession(String name, String last_name, String user_id,String email, String mobile_num)
//    {
//        editor.putBoolean(Is_Login, true);
//
//        editor.putString(KEY_NAME,name);
//
//        editor.putString(KEY_LAST_NAME,last_name);
//
//        editor.putString(KEY_User_id,user_id);
//
//        editor.putString(KEY_Email,email);
//
//        editor.putString(KEY_Mobile,mobile_num);
//
//        // commit changes
//        editor.commit();
//    }

    public void createLoginSession(String name, String user_id, String email, String mobile_num, String imgurl)
    {
        editor.putBoolean(Is_Login, true);

        editor.putString(KEY_NAME,name);

        // editor.putString(KEY_LAST_NAME,last_name);

        editor.putString(KEY_User_id,user_id);

        editor.putString(KEY_Email,email);

        editor.putString(KEY_Mobile,mobile_num);

        editor.putString(KEY_ProImgUrl, imgurl);

        // commit changes
        editor.commit();
    }

    public void createImageString(String imagepath)
    {
        editor1.putBoolean(IS_Image, true);
        // Storing name in pref
        editor1.putString(KEY_IMAGE_URL,imagepath);
        // editor1.putString(KEY_IMAGE_URL2,imagepath2);
        // Storing email in pref
        // commit changesacv@gmail.com
        editor1.commit();
    }
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            /*Intent i = new Intent(context, Login.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            context.startActivity(i);*/
        }
    }
//    public void checkImage(){
//        // Check login status
//        if(!this.isImageString()){
//            // user is not logged in redirect him to Login Activity
//            Intent i2 = new Intent(context, EditProfile.class);
//            // Closing all the Activities
//            i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            // Add new Flag to start new Activity
//            i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            // Staring Login Activity
//            context.startActivity(i2);
//        }
//    }

//    public HashMap<String, String> getUser(){
//        HashMap<String, String> user = new HashMap<String, String>();
//
//        user.put(KEY_NAME,pref.getString(KEY_NAME,null));
//
//        user.put(KEY_LAST_NAME,pref.getString(KEY_LAST_NAME,null));
//        // return user
//        user.put(KEY_User_id, pref.getString(KEY_User_id, null));
//        // user email id
//        user.put(KEY_Email, pref.getString(KEY_Email, null));
//
//        user.put(KEY_Mobile, pref.getString(KEY_Mobile, null));
//        return user;
//    }

    public HashMap<String, String> getUser(){
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_NAME,pref.getString(KEY_NAME,null));

        // user.put(KEY_LAST_NAME,pref.getString(KEY_LAST_NAME,null));
        // return user
        user.put(KEY_User_id, pref.getString(KEY_User_id, null));
        // user email id
        user.put(KEY_Email, pref.getString(KEY_Email, null));

        user.put(KEY_Mobile, pref.getString(KEY_Mobile, null));

        user.put(KEY_ProImgUrl , pref.getString(KEY_ProImgUrl, null));
        return user;
    }

    public HashMap<String, String> getImageUrl(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user email id
        user.put(KEY_IMAGE_URL, pref.getString(KEY_IMAGE_URL, null));
        // user.put(KEY_IMAGE_URL2, pref.getString(KEY_IMAGE_URL2, null));
        return user;
    }
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
       /* Intent i = new Intent(context, Login.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);*/
    }
//    public void logoutImage(){
//        // Clearing all data from Shared Preferences
//        editor1.clear();
//        editor1.commit();
//        // After logout redirect user to Loing Activity
//        Intent i2 = new Intent(context, EditProfile.class);
//        i2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        // Staring Login Activity
//        context.startActivity(i2);
//    }
    public boolean isLoggedIn()
    {
        return pref.getBoolean(Is_Login, false);
    }
    public boolean isImageString()
    {
        return pref.getBoolean(IS_Image, false);
    }
}


