package com.theoptimumlabs.playermarket.SessionManagement;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatActivity;


/**
 * Created by optimumanddev2 on 19/12/17.
 */

public class MyApplication  extends MultiDexApplication {
    public static String otheruseridapp;
    private static final String TAG = MyApplication.class.getSimpleName();

    private static MyApplication mInstance;
    public AppCompatActivity activity;
    /*//for language start
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
        MultiDex.install(this);
    }
    ///for language end*/

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }



    @Override
    public void onCreate() {
        mInstance=this;
        super.onCreate();

//        AndroidNetworking.initialize(getApplicationContext());
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inPurgeable = true;
//        AndroidNetworking.setBitmapDecodeOptions(options);
//        AndroidNetworking.enableLogging();
//        AndroidNetworking.setConnectionQualityChangeListener(new ConnectionQualityChangeListener() {
//            @Override
//            public void onChange(ConnectionQuality currentConnectionQuality, int currentBandwidth) {
//                Log.d(TAG, "onChange: currentConnectionQuality : " + currentConnectionQuality + " currentBandwidth : " + currentBandwidth);
//            }
//        });

    }



    public static synchronized MyApplication getInstance() {
        return mInstance;
    }


    public static Context getContext(){
        return mInstance.getApplicationContext();
    }



}

