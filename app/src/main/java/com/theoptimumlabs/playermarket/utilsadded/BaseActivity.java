package com.theoptimumlabs.playermarket.utilsadded;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;

import com.theoptimumlabs.playermarket.HomeActivity;
import com.theoptimumlabs.playermarket.R;


public class BaseActivity extends AppCompatActivity {


    ProgressDialog mProgressDialog;

    Integer sessionid, userid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_home);
       // initilizePreferance();
        initilizeProgress();
      //  userid = Integer.parseInt(productResponse.getUserId());
        //sessionid = productResponse.getSessionid();
       // Toast.makeText(getApplicationContext(), userid,Toast.LENGTH_LONG).show();
       // Toast.makeText(getApplicationContext(),sessionid, Toast.LENGTH_LONG).show();
    }

/*
    private void initilizePreferance() {
        if (mPrefermenceManager == null)
            mPrefermenceManager.initializeInstance(getApplicationContext());
    }

    public PreferenceManager getSession() {
        return mPrefermenceManager;
    }

   // session.createLoginSession(userid, s);

    public void setSession(PreferenceManager mPrefermenceManager) {
        this.mPrefermenceManager = mPrefermenceManager;
    }
*/

    private void initilizeProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
        }
    }

    public void showProgress() {
        mProgressDialog.show();
    }

    public void showProgress(String message) {
        mProgressDialog.setMessage(message);
        mProgressDialog.show();
    }

    public void dismissProgress() {
        mProgressDialog.cancel();
    }
    public void hideprogress(){
        mProgressDialog.hide();
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                getCurrentFocus().getWindowToken(), 0);
    }
    public void ShowNormalmsg(String msg){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.getContext().setTheme(R.style.AlertThem);

        builder.setMessage(msg)
                .setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();

    }
}
