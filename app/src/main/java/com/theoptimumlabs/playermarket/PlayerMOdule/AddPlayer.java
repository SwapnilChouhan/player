package com.theoptimumlabs.playermarket.PlayerMOdule;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.theoptimumlabs.playermarket.Model.AddPlayerBean;
import com.theoptimumlabs.playermarket.R;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class AddPlayer extends AppCompatActivity {
    EditText edtplayernm,edtteamName, edtcountry, edtposition, edtposiside, edtbdate, edtweight, edtheight, edtshirt, edtbirthplace;
    String strteamNme,strplayernm, strcountry, strposition, strposiside, strbdate, strweight, strheight,
            strshirt, strbirthplace,strImage;
    Button btnsave,btnCamera,btnGallery;
    private FirebaseFirestore firestoreDB;
    ImageView immgPlayer;
    private static final int REQUEST_GALLERY = 1;
    private static final int REQUEST_CAMERA = 0;
    private static final int MY_PERMISSIONS_REQUESTS = 200;
    Uri uri;
    private int mYear, mMonth, mDay, mHour, mMinute;
    Context ctxAddPlayer;
    RelativeLayout rlprice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.theoptimumlabs.playermarket.R.layout.activity_add_player);
        ctxAddPlayer=this;
        firestoreDB = FirebaseFirestore.getInstance();
        getSupportActionBar().hide();
//        ev_list = (ExpandableListView) findViewById(R.id.ev_menu);

        rlprice = (RelativeLayout) findViewById(R.id.rlprice);
        edtplayernm = (EditText) findViewById(com.theoptimumlabs.playermarket.R.id.edtplayernm);
        edtteamName = (EditText) findViewById(com.theoptimumlabs.playermarket.R.id.edtteamName);
        edtcountry = (EditText) findViewById(com.theoptimumlabs.playermarket.R.id.edtcountry);
        edtposiside = (EditText) findViewById(com.theoptimumlabs.playermarket.R.id.edtposiside);
        edtposition = (EditText) findViewById(com.theoptimumlabs.playermarket.R.id.edtposition);
        edtbdate = (EditText) findViewById(com.theoptimumlabs.playermarket.R.id.edtbdate);
        edtweight = (EditText) findViewById(com.theoptimumlabs.playermarket.R.id.edtweight);
        edtheight = (EditText) findViewById(com.theoptimumlabs.playermarket.R.id.edtheight);
        edtshirt = (EditText) findViewById(com.theoptimumlabs.playermarket.R.id.edtshirt);
        edtbirthplace = (EditText) findViewById(com.theoptimumlabs.playermarket.R.id.edtbirthplace);
        btnsave = (Button)findViewById(com.theoptimumlabs.playermarket.R.id.btnsave);
        btnCamera = (Button)findViewById(com.theoptimumlabs.playermarket.R.id.btnCamera);
        btnGallery = (Button)findViewById(com.theoptimumlabs.playermarket.R.id.btnGallery);
        immgPlayer=(ImageView)findViewById(com.theoptimumlabs.playermarket.R.id.immgPlayer);

        rlprice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddPlayer.this, AddpriceForPlayer.class);
                startActivity(i);
            }
        });
        requestPermissions();
        edtbdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(AddPlayer.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                if(monthOfYear <= 8){
                                    if(dayOfMonth <=9) {
                                        edtbdate.setText("0"+dayOfMonth + "-" + "0" + (monthOfYear + 1) + "-" + year);
                                    }else {
                                        edtbdate.setText(dayOfMonth + "-" + "0" + (monthOfYear + 1) + "-" + year);
                                    }
                                }else{
                                    if(dayOfMonth <=9) {
                                        edtbdate.setText("0"+dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                    }else {
                                        edtbdate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                    }
                                }




                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strplayernm = edtplayernm.getText().toString();
                strcountry = edtcountry.getText().toString();
                strposiside = edtposiside.getText().toString();
                strposition = edtposition.getText().toString();
                strbdate = edtbdate.getText().toString();
                strweight = edtweight.getText().toString()+" kg";
                strheight = edtheight.getText().toString()+ " m";
                strshirt = edtshirt.getText().toString();
                strbirthplace = edtbirthplace.getText().toString();
                strteamNme = edtteamName.getText().toString();
                if(strplayernm.equals("")){

                }else{
                    addNote();
                }
            }
        });

        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,REQUEST_CAMERA);
            }
        });
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, REQUEST_GALLERY);
            }
        });



        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        switch (requestCode) {
            case REQUEST_GALLERY:
                if (resultCode == RESULT_OK ) {

                    uri = data.getData();
                    Log.e("uri", "" + uri);
                    InputStream is = null;
                    Bitmap bitmap = null;
                    try {
                        is = getContentResolver().openInputStream(uri);
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        options.inSampleSize = 2;
                        options.inScreenDensity = DisplayMetrics.DENSITY_HIGH;
                        bitmap = BitmapFactory.decodeStream(is, null, options);
                        Log.e("bitmap", "" + bitmap);

                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                        byte[] imageBytes = baos.toByteArray();
                        strImage=""+Base64.encodeToString(imageBytes, Base64.DEFAULT);
                        Log.e("strImage:-",""+strImage);
                        //immgPlayer.setImageBitmap(bitmap);

                        byte[] imageBytes11 = Base64.decode(strImage, Base64.DEFAULT);
                        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes11, 0, imageBytes.length);
                        immgPlayer.setImageBitmap(decodedImage);

                    } catch (FileNotFoundException e) {
                        Log.e("Failedtofindthefile: ", "" + uri, e);
                    }

                }
                break;
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK ) {


                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                    Log.e("Mohitbitmap:",""+bitmap);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                    byte[] imageBytes = baos.toByteArray();
                    strImage=""+Base64.encodeToString(imageBytes, Base64.DEFAULT);
                    immgPlayer.setImageBitmap(bitmap);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void addNote() {


//        /        Map<String, Object> note = new Note(title, content).toMap();
//        Map<String, Object> note = new Note(title, content).toMap();
        Log.e("randomno",randomId()+""+strImage);
        String strrandomId = "p_"+randomId();
        AddPlayerBean addPlayerBean = new AddPlayerBean(strrandomId, strplayernm, strteamNme, strcountry, strposition, strposiside, strbdate, strweight, strheight,strshirt, strbirthplace,strImage);

        firestoreDB.collection("PlayerInfo").document(strrandomId)
                .set(addPlayerBean).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.e("addplayerdata", "DocumentSnapshot written with ID: " + strplayernm);
                Toast.makeText(getApplicationContext(), "Player Added successfully!", Toast.LENGTH_SHORT).show();

                edtplayernm.setText("");
                edtteamName.setText("");
                edtcountry.setText("");
                edtposiside.setText("");
                edtposition.setText("");
                edtbdate.setText("");
                edtweight.setText("");
                edtheight.setText("");
                edtshirt.setText("");
                edtbirthplace.setText("");
            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("AddpErr", "Error adding Note document", e);
                        Toast.makeText(getApplicationContext(), "Note could not be added!", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    public void hideKeyboard() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                getCurrentFocus().getWindowToken(), 0);
    }
    public int randomId(){
        Random r = new Random(System.currentTimeMillis());
        return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
    }

    private void requestPermissions()
    {
        List<String> requiredPermissions = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requiredPermissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requiredPermissions.add(android.Manifest.permission.CAMERA);
        }

        if (!requiredPermissions.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    requiredPermissions.toArray(new String[]{}),
                    MY_PERMISSIONS_REQUESTS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUESTS:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    Toast.makeText(ctxAddPlayer, "Permission Granted, Now you can access location data and camera.", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(ctxAddPlayer, "Permission Denied, You cannot access location data and camera.", Toast.LENGTH_SHORT).show();
                    // FIXME: Handle this case the user denied to grant the permissions
                }
                break;

            default:
                // TODO: Take care of this case later
                break;

        }
    }



}

//for getting image from database

/*
 String imgString=""+android.graphics.Bitmap@62d1953;
byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        holder.image_background.setImageBitmap(decodedImage);*/
