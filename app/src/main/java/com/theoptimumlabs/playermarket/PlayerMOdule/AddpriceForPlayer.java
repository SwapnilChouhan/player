package com.theoptimumlabs.playermarket.PlayerMOdule;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.theoptimumlabs.playermarket.Adapter.PlayerRVAdapter;
import com.theoptimumlabs.playermarket.Model.AddPlayerBean;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.SessionManagement.Session;
import com.theoptimumlabs.playermarket.utilsadded.CustomItemClickListenerHomepage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddpriceForPlayer extends AppCompatActivity {

    public static List<AddPlayerBean> persons;
    private RecyclerView rv;

    Session session;
    String user_id;
    HashMap<String, String> hashMap = new HashMap<>();
    LinearLayout llworknot;
    private FirebaseFirestore firestoreDB;
    private ListenerRegistration firestoreListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addprice_for_player);
        firestoreDB = FirebaseFirestore.getInstance();

        rv=(RecyclerView)findViewById(R.id.rv);
        llworknot=(LinearLayout)findViewById(R.id.llworknot);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

//        initializeData();
//        initializeAdapter();

        loadNotesList();

        firestoreListener = firestoreDB.collection("PlayerInfo")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.e("err", "Listen failed!", e);
                            return;
                        }
                        persons = new ArrayList<>();
                        List<AddPlayerBean> notesList = new ArrayList<>();

                        for (DocumentSnapshot doc : documentSnapshots) {
                            AddPlayerBean note = doc.toObject(AddPlayerBean.class);
                            note.setId(doc.getId());
                            notesList.add(note);
                            persons.add(note);
                        }
                        PlayerRVAdapter adapter = new PlayerRVAdapter(firestoreDB,"player",AddpriceForPlayer.this, persons, new CustomItemClickListenerHomepage() {
                            @Override
                            public void onItemClick(View v, int position) {
//                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
                            }
                        });
                        rv.setAdapter(adapter);

//                        mAdapter = new NoteRecyclerViewAdapter(notesList, getApplicationContext(), firestoreDB);
//                        recyclerView.setAdapter(mAdapter);
                    }
                });


    }


    private void loadNotesList() {
        firestoreDB.collection("PlayerInfo")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<AddPlayerBean> notesList = new ArrayList<>();
                            persons = new ArrayList<>();

                            for (DocumentSnapshot doc : task.getResult()) {
                                AddPlayerBean note = doc.toObject(AddPlayerBean.class);
                                note.setId(doc.getId());
                                notesList.add(note);
                                Log.e("sona", notesList.size()+""+doc.getId());
                                persons.add(note);
                            }

//                            mAdapter = new NoteRecyclerViewAdapter(notesList, getApplicationContext(), firestoreDB);
//                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//                            recyclerView.setLayoutManager(mLayoutManager);
//                            recyclerView.setItemAnimator(new DefaultItemAnimator());
//                            recyclerView.setAdapter(mAdapter);

                            PlayerRVAdapter adapter = new PlayerRVAdapter(firestoreDB,"player",AddpriceForPlayer.this, persons, new CustomItemClickListenerHomepage() {
                                @Override
                                public void onItemClick(View v, int position) {
//                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
                                }
                            });
                            rv.setAdapter(adapter);
                        } else {
                            Log.d("ERR", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

}
