package com.theoptimumlabs.playermarket.LoginRegister;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.theoptimumlabs.playermarket.HomeActivity;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.SessionManagement.Session;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    SharedPreferences sharedpreferences;

    public static final String mypreference = "mypref";
    public static final String Name = "passkey";
    public static final String Email = "emailKey";

    ProgressBar progressBar;
    EditText editTextEmail, editTextPassword;

    private FirebaseAuth mAuth;
    String storeemail, storepass;
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        mAuth = FirebaseAuth.getInstance();
        session = new Session(getApplicationContext());
        if (session.isLoggedIn() == true){
            session.checkLogin();
            Intent i= new Intent(SignUpActivity.this,HomeActivity.class);
            startActivity(i);
        }
        findViewById(R.id.buttonSignUp).setOnClickListener(this);
        findViewById(R.id.textViewLogin).setOnClickListener(this);
        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Name)) {
           storepass = (sharedpreferences.getString(Name, ""));
        }
        if (sharedpreferences.contains(Email)) {
        storeemail = (sharedpreferences.getString(Email, ""));
            try {
                mAuth.signInWithEmailAndPassword(storeemail, storepass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            final FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {

                                if (user.isEmailVerified()) {
                                    Log.e("sonali", "Email Verified");
                                    Intent i = new Intent(SignUpActivity.this, UserSignup.class);
                                    i.putExtra("email",storeemail);
                                    i.putExtra("pass", storepass);
                                    startActivity(i);
//                                    SharedPreferences.Editor editor = sharedpreferences.edit();
//                                    editor.clear();
//                                    editor.commit();
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }catch (Exception e)
            {}
        }


    }

    private void registerUser() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        if (email.isEmpty()) {
            editTextEmail.setError("Email is required");
            editTextEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError("Please enter a valid email");
            editTextEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            editTextPassword.setError("Password is required");
            editTextPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            editTextPassword.setError("Minimum lenght of password should be 6");
            editTextPassword.requestFocus();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progressBar.setVisibility(View.GONE);
                if (task.isSuccessful()) {
//                    finish();
                    loadUserInformation();

                } else {

                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        Toast.makeText(getApplicationContext(), "You are already registered", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(SignUpActivity.this, UserSignup.class);
                        i.putExtra("email",storeemail);
                        i.putExtra("pass", storepass);
                        startActivity(i);
                    } else {
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSignUp:
                registerUser();
                break;

            case R.id.textViewLogin:
                finish();
                startActivity(new Intent(this, UserSignup.class));
                break;
        }
    }
    private void loadUserInformation() {
        final FirebaseUser user = mAuth.getCurrentUser();

        if (user != null) {

            if (user.isEmailVerified()) {
                Log.e("sonali","Email Verified");
                Intent i = new Intent(SignUpActivity.this, UserSignup.class);
                startActivity(i);
            } else {
                Log.e("sonali","Email Not Verified (Click to Verify)");
                user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String p = editTextPassword.getText().toString();
                        String e = editTextEmail.getText().toString();
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Name, p);
                        editor.putString(Email, e);
                        editor.commit();
                        Toast.makeText(SignUpActivity.this, "Verification Email Sent.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

}
