package com.theoptimumlabs.playermarket.LoginRegister;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.theoptimumlabs.playermarket.HomeActivity;
import com.theoptimumlabs.playermarket.PlayerMOdule.AddPlayer;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.SessionManagement.Session;

public class MainActivity  extends AppCompatActivity implements View.OnClickListener {
    FirebaseFirestore fsdb;
    private  final String TAG = "sona";
    FirebaseAuth mAuth;
    EditText editTextEmail, editTextPassword;
    ProgressBar progressBar;
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        fsdb= FirebaseFirestore.getInstance();

        session = new Session(getApplicationContext());
        if (session.isLoggedIn() == true){
            session.checkLogin();
            Intent i= new Intent(MainActivity.this,HomeActivity.class);
            startActivity(i);
        }
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        findViewById(R.id.textViewSignup).setOnClickListener(this);
        findViewById(R.id.buttonLogin).setOnClickListener(this);

    }

    private void userLogin() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        if (email.isEmpty()) {
            editTextEmail.setError("Email is required");
            editTextEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError("Please enter a valid email");
            editTextEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            editTextPassword.setError("Password is required");
            editTextPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            editTextPassword.setError("Minimum lenght of password should be 6");
            editTextPassword.requestFocus();
            return;
        }
        if(email.equals("pmarket@gmail.com") && password.equals("market")){
            Intent i = new Intent(MainActivity.this, AddPlayer.class);
            startActivity(i);
        }else {

            progressBar.setVisibility(View.VISIBLE);


            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    progressBar.setVisibility(View.GONE);
                    if (task.isSuccessful()) {
                        getFromFirebase(editTextEmail.getText().toString());
                        finish();
                        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    } else {
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);

                    }
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mAuth.getCurrentUser() != null) {
//            finish();
//            startActivity(new Intent(this, HomeActivity.class));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.textViewSignup:
                finish();
                startActivity(new Intent(this, UserSignup.class));
                break;

            case R.id.buttonLogin:
                userLogin();
                break;
        }
    }

    public void getFromFirebase(final String email){
        DocumentReference docRef = fsdb.collection("UserInfo").document(email);
//        DocumentReference docRef = fsdb.collection("UserInfo").document(email);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                String sUName="0",sUniqId="0",sUPassword="0",sEmail="0", phn ="0", cntry = "",stts = "",id;

                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();

                    if (document.exists()) {
                        sUName = "" + document.getData().get("UN");
                        sUPassword = "" + document.getData().get("PSW");
                        sEmail = "" + document.getData().get("EMail");
                        phn = "" + document.getData().get("PHN");
                        cntry = "" + document.getData().get("CNTRY");
                        stts = "" + document.getData().get("STs");
                        id = "" + document.getData().get("Id");
                        sUniqId=""+document.getId();
                        Log.e("sUniqId:--",""+sUniqId+" "+sUName);


                        session.createLoginSession(sUName,id,sEmail,sUPassword, cntry);

//                        if (sUName.equals(UserName) && sUPassword.equals(Password)) {
//                            Intent intent=new Intent(MainActivity.this,HomeActivity.class);
////                            intent.putExtra("Name",""+sUName);
////                            intent.putExtra("Email",""+sEmail);
//                            startActivity(intent);
//                        }
//                        else{
//
//                        }
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }



}
