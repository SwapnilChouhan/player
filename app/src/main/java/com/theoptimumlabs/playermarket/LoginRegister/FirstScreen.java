package com.theoptimumlabs.playermarket.LoginRegister;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.theoptimumlabs.playermarket.HomeActivity;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.SessionManagement.Session;
import com.theoptimumlabs.playermarket.utilsadded.BaseActivity;

public class FirstScreen extends BaseActivity {
    Button signup;
    Button login;
    Session session;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_screen);
        signup=(Button)findViewById(R.id.Signp);
        login=(Button)findViewById(R.id.Login);
        mAuth = FirebaseAuth.getInstance();

        session = new Session(getApplicationContext());
        Log.e("sonal", session.isLoggedIn() +"");
        loadUserInformation();
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(FirstScreen.this,UserSignup.class);
                startActivity(i);
                finish();
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i=new Intent(FirstScreen.this,Home_page.class);
                Intent i=new Intent(FirstScreen.this,MainActivity.class);
                startActivity(i);
                finish();
            }
        });

    }
    private void loadUserInformation() {
        final FirebaseUser user = mAuth.getCurrentUser();

        if (user != null) {

            if (user.isEmailVerified()) {
                Log.e("sonali","Email Verified");
                if (session.isLoggedIn() == true){
                    session.checkLogin();
                    Intent i= new Intent(FirstScreen.this,HomeActivity.class);
                    startActivity(i);
                }
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.clear();
//                editor.commit();
            } else {
                Log.e("sonali","Email Not Verified (Click to Verify)");
//                user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        String p = editTextPassword.getText().toString();
//                        String e = edit_email.getText().toString();
//                        SharedPreferences.Editor editor = sharedpreferences.edit();
//                        editor.putString(Name, p);
//                        editor.putString(Email, e);
//                        editor.commit();
//                        ShowNormalmsg("Verification Email Sent. Please verify for login.");
//                        Toast.makeText(UserSignup.this, "Verification Email Sent.", Toast.LENGTH_SHORT).show();
//                    }
//                });
                ShowNormalmsg("Verification Email Sent. Please verify for login.");
            }
        }
    }

}
