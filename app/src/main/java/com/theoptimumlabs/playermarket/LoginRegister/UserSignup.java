package com.theoptimumlabs.playermarket.LoginRegister;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.theoptimumlabs.playermarket.HomeActivity;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.SessionManagement.Session;
import com.theoptimumlabs.playermarket.utilsadded.BaseActivity;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserSignup extends BaseActivity {
    SharedPreferences sharedpreferences;

    public static final String mypreference = "mypref";
    public static final String Name = "passkey";
    public static final String Email = "emailKey";

    EditText edit_username, edit_Country, edit_email, edphn, editTextPassword, edConfirmPassword;
    Button btnsave;
    String storeemail, storepass;

    String sPswEncrypt;

    FirebaseFirestore fsdb;
    Session session;
    ProgressBar progressBar;

    private FirebaseAuth mAuth;

    private static Pattern pattern;
    private static Matcher matcher;

    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";
//            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";

    public UserSignup(){
        pattern = Pattern.compile(PASSWORD_PATTERN);
    }
    public static boolean validate(final String password){

        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_signup);
        edit_username = (EditText)findViewById(R.id.edit_username);
        edit_Country = (EditText)findViewById(R.id.edit_Country);
        edit_email = (EditText)findViewById(R.id.edit_email);
        editTextPassword = (EditText)findViewById(R.id.editTextPassword);
        edConfirmPassword = (EditText)findViewById(R.id.edConfirmPassword);
        edphn = (EditText)findViewById(R.id.edphn);
        btnsave = (Button)findViewById(R.id.btnsave);
        mAuth = FirebaseAuth.getInstance();
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        session = new Session(getApplicationContext());
        if (session.isLoggedIn() == true){
            session.checkLogin();
            Intent i= new Intent(UserSignup.this,HomeActivity.class);
            startActivity(i);
        }
        sharedpreferences = getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(Name)) {
            storepass = (sharedpreferences.getString(Name, ""));
        }
        if (sharedpreferences.contains(Email)) {
            storeemail = (sharedpreferences.getString(Email, ""));
            try {
                mAuth.signInWithEmailAndPassword(storeemail, storepass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            final FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                user.getUid();

                                if (user.isEmailVerified()) {
                                    Log.e("sonali", "Email Verified");
//                                    Intent i = new Intent(UserSignup.this, MainActivity.class);
//                                    i.putExtra("email",storeemail);
//                                    i.putExtra("pass", storepass);
//                                    startActivity(i);
//                                    SharedPreferences.Editor editor = sharedpreferences.edit();
//                                    editor.clear();
//                                    editor.commit();
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }catch (Exception e)
            {}
        }
//        try {
//            Intent i = getIntent();
//            storeemail = i.getStringExtra("email");
//            storepass = i.getStringExtra("pass");
//            edit_email.setText(storeemail);
//        }catch (Exception e){}

        fsdb= FirebaseFirestore.getInstance();

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                boolean valid = UserSignup.validate(temp);
                if(edit_username.getText().toString().equals("")){
                    edit_username.setError("Please Enter UserName");
                }
                else if (!checkEMailValidation(edit_email)) {

                    Log.e("Validation", "Email not entered properly");
                    edit_email.setError("Please Enter Proper Email.");

                }else if(!UserSignup.validate(editTextPassword.getText().toString())){
//                }else if(editTextPassword.getText().toString().length() < 6){
                    editTextPassword.setError("Password should have one number and one upper and lower case alphabet and minimum 6 length.");

                }else if(!editTextPassword.getText().toString().equals(edConfirmPassword.getText().toString())){
                    edConfirmPassword.setError("Confirm Password not match!");
                }

                else if (!checkMobileNumberValidation(edphn)) {

                    Log.e("Validation", "Mob not entered properly");
                    edphn.setError("Please Enter Proper Mobile.");

                }else if(edit_Country.getText().toString().equals("")){
                    edit_Country.setError("Please Enter Country");
                }else {
                    progressBar.setVisibility(View.VISIBLE);
//                    Toast.makeText(UserSignup.this, "You are Registered Successfully", Toast.LENGTH_SHORT).show();

                    //Encoding the password
                    byte[] bytess = new byte[0];
                    try {
                        bytess = editTextPassword.getText().toString().getBytes("UTF-8");

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    sPswEncrypt = Base64.encodeToString(bytess, Base64.DEFAULT);
                    Log.e("sPswEncrypt:-",""+sPswEncrypt);

                    //Creating account on Firestore Database
                    CollectionReference collReference = fsdb.collection("UserInfo");
                    final String randomId = edit_email.getText().toString();

                    Map<String, Object> data1 = new HashMap<>();
                    data1.put("Id", ""+randomId);
                    data1.put("UN", ""+edit_username.getText().toString());
                    data1.put("PSW", ""+sPswEncrypt);
                    data1.put("EMail", ""+edit_email.getText().toString());
                    data1.put("PHN", edphn.getText().toString());
                    data1.put("CNTRY",edit_Country.getText().toString());
                    data1.put("STs","0");
                    collReference.document(randomId).set(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            mAuth.createUserWithEmailAndPassword(edit_email.getText().toString(), editTextPassword.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    progressBar.setVisibility(View.GONE);
                                    if (task.isSuccessful()) {
//                    finish();
                                        loadUserInformation();
                                        session.createLoginSession(edit_username.getText().toString(),randomId,edit_email.getText().toString(),editTextPassword.getText().toString(), edit_Country.getText().toString());

                                    } else {

                                        if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                            Toast.makeText(getApplicationContext(), "You are already registered", Toast.LENGTH_SHORT).show();
                                            Intent i = new Intent(UserSignup.this, MainActivity.class);
                                            i.putExtra("email",storeemail);
                                            i.putExtra("pass", storepass);
                                            startActivity(i);
                                        } else {
                                            Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                }
                            });

                        }
                    });

                    //Saving the Userrname and email into Shared Preffrence
//                    Utility.setSharedPreference(ctxRegister, Constance.UserName,sUsername);
//                    Utility.setSharedPreference(ctxRegister, Constance.Email,sEmail);

//                    Toast.makeText(UserSignup.this, "You are Registered Successfully", Toast.LENGTH_SHORT).show();

                    //Calling the Login Activity
//                    Intent intent=new Intent(UserSignup.this,HomeActivity.class);
//                    startActivity(intent);
//                    finish();


                }
            }
        });
    }

    public static boolean checkEMailValidation(EditText editText) {
        if (!Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString().trim()).matches()) {
            Log.e("EMailValidation", "false");
            //  if(intmob)
            //  editText.setError("Please Enter proper Email Or Mobile no");

//            intemail = 0;
            return false;
        } else {
            Log.e("EMailValidation", "true");
//            intemail = 1;
            return true;
        }
    }

    public static boolean checkMobileNumberValidation(EditText cellphone) {
        if(!Patterns.PHONE.matcher(cellphone.getText().toString().trim()).matches() || cellphone.getText().toString().trim().length() <= 9){
            // if (cellphone.getText().toString().trim().length() <= 9) {
            Log.e("MobileNumberValidation", "false");
            cellphone.setError("Please Enter proper Mobile Number or Email");
//            intmob = 0;
            return false;
        } else {
            Log.e("MobileNumberValidation", "true");
//            intmob = 1;
            return true;
        }
    }



    public int randomId(){
        Random r = new Random(System.currentTimeMillis());
        return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
    }

    private void loadUserInformation() {
        final FirebaseUser user = mAuth.getCurrentUser();

        if (user != null) {

            if (user.isEmailVerified()) {
                Log.e("sonali","Email Verified");
                Intent i = new Intent(UserSignup.this, MainActivity.class);
                startActivity(i);


//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.clear();
//                editor.commit();
            } else {
                Log.e("sonali","Email Not Verified (Click to Verify)");
                user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String p = editTextPassword.getText().toString();
                        String e = edit_email.getText().toString();
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(Name, p);
                        editor.putString(Email, e);
                        editor.commit();
                        ShowNormalmsg("Verification Email Sent. Please verify for login.");
                        Toast.makeText(UserSignup.this, "Verification Email Sent.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        super.onBackPressed();
    }
}
