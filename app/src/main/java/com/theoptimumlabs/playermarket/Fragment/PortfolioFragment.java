package com.theoptimumlabs.playermarket.Fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextPaint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.theoptimumlabs.playermarket.Adapter.RVAdapterPortpolio;
import com.theoptimumlabs.playermarket.Adapter.RVAdapterPortpolio;
import com.theoptimumlabs.playermarket.Adapter.RVAdapterPortpolioSellQue;
import com.theoptimumlabs.playermarket.Model.PortfolioBean;
import com.theoptimumlabs.playermarket.Model.PortfolioBean;
import com.theoptimumlabs.playermarket.Model.SellQueueBean;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.SessionManagement.Session;
import com.theoptimumlabs.playermarket.utilsadded.CustomItemClickListenerHomepage;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PortfolioFragment extends Fragment {


    public PortfolioFragment() {
        // Required empty public constructor
    }

    public  List<SellQueueBean> persons;
    public  List<SellQueueBean> personsselqfinal;
    public  List<PortfolioBean> personsport;
    public  List<PortfolioBean> personfinal;
    private RecyclerView rv, rvqueue;
    RVAdapterPortpolioSellQue qadapter;
    Session session;
    String user_id, amt;
    HashMap<String, String> hashMap = new HashMap<>();
    LinearLayout llworknot;
    private FirebaseFirestore firestoreDB;
    private ListenerRegistration firestoreListener;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "myprefamt";
    public static final String AMT = "amtKey";
    TextView portfoliosec, sellsec;
    ProgressBar progressBar;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_portfolio, container, false);
        session = new Session(getActivity());
        if(session.isLoggedIn()){
            session.checkLogin();
            hashMap = session.getUser();
            user_id = hashMap.get(session.KEY_User_id);
            Log.e("useridsess", user_id+"");

        }
        sharedpreferences = getActivity().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(AMT)) {
           amt = (sharedpreferences.getString(AMT, ""));
        }
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        rv=(RecyclerView)view.findViewById(R.id.rv);
        rvqueue=(RecyclerView)view.findViewById(R.id.rvqueue);
        llworknot=(LinearLayout)view.findViewById(R.id.llworknot);
        portfoliosec=(TextView) view.findViewById(R.id.portfoliosec);
        sellsec=(TextView) view.findViewById(R.id.sellsec);
        rv.setNestedScrollingEnabled(false);
        rvqueue.setNestedScrollingEnabled(false);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm2 = new LinearLayoutManager(getActivity());
        rvqueue.setLayoutManager(llm2);
        rvqueue.setHasFixedSize(true);

//        initializeData();
//        initializeAdapter();
        firestoreDB = FirebaseFirestore.getInstance();
        try {
            getamtFirebase(user_id);
        }catch (Exception e){
//            try {
//                loadNotesList();
//            }catch (Exception e1){}

        }
        try {
            loadportfolioList();
        }catch (Exception e){}

            try {
                loadsellq();
            }catch (Exception e1){}

        return view;

    }

    private void initializeData(){
        persons = new ArrayList<>();
//        persons.add(new PortfolioBean("Emma Wilson", "23 years old", R.drawable.emma));

    }

    //    private void initializeAdapter(){
//
//        RVAdapterPortpolio adapter = new RVAdapterPortpolio(getActivity(), persons, new CustomItemClickListenerHomepage() {
//            @Override
//            public void onItemClick(View v, int position) {
////                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
//            }
//        });
//        rv.setAdapter(adapter);
//    }




    private void loadsellq() {
        firestoreDB.collection("SellQueue")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<PortfolioBean> notesList = new ArrayList<>();
                            persons = new ArrayList<>();

                            for (DocumentSnapshot doc : task.getResult()) {
                                String resuid = doc.getData().get("uid")+"";
                                if(resuid.equals(user_id)) {
                                    SellQueueBean note = new SellQueueBean();
                                    note.setId(doc.getId());
                                    note.setId(doc.getData().get("Id") + "");
                                    note.setBuyp(doc.getData().get("buyp") + "");
                                    note.setPid(doc.getData().get("pid") + "");
                                    note.setSellp(doc.getData().get("sellp") + "");
                                    note.setShr(doc.getData().get("shr") + "");
                                    note.setSts(doc.getData().get("sts") + "");
                                    note.setTranctdate(doc.getData().get("tranctdate") + "");
                                    note.setUid(doc.getData().get("uid") + "");

                                    final String pid = doc.getData().get("pid") + "";
                                    final String sellshr = doc.getData().get("shr") + "";
                                    final String buypselltime = doc.getData().get("buyp") + "";
                                    final String selpselltime = doc.getData().get("sellp") + "";
                                    final String sts = doc.getData().get("sts") + "";
                                    final String Tranctdate = doc.getData().get("tranctdate") + "";
                                    final String uid = doc.getData().get("uid") + "";

//                                notesList.add(note);
                                    Log.e("sonaport", notesList.size() + "" + doc.getId());


                                    persons.add(note);
                                }
                            }



                            firestoreListener = firestoreDB.collection("PlayerInfo")
                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                        @Override
                                        public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                                            if (e != null) {
                                                Log.e("err", "Listen failed!", e);
                                                return;
                                            }
                                            personsselqfinal = new ArrayList<>();
//                                            List<PortfolioBean> notesList = new ArrayList<>();

                                            for (DocumentSnapshot doc : documentSnapshots) {
                                                SellQueueBean note = new SellQueueBean();
//                            PortfolioBean note = doc.toObject(PortfolioBean.class);
                                                note.setId(doc.getId());
//                                                Log.e("pnm", String.valueOf(doc.getData().get("p_n")+""));
                                                note.setPn(String.valueOf(doc.getData().get("p_n")+""));

                                                for(int i = 0; i < persons.size(); i++){
                                                    SellQueueBean note1 = new SellQueueBean();
//                            PortfolioBean note = doc.toObject(PortfolioBean.class);
                                                    if(persons.get(i).pid.equals(doc.getId())) {
                                                        String strshr = persons.get(i).shr+"";
                                                        Log.e("sona", strshr+" s");
                                                        if(!strshr.equals("0")) {
                                                            Log.e("sonagetdatasellq", strshr+" s");

//                                                        note.setId(personsport.get(i).id);
                                                            double quanshr = 0, curprice = 0, ttlcpri = 0, pastbpri = 0.0, pastttlbpri = 0.0, difbpri = 0.0, difttlbpri = 0.0;

                                                            try {

                                                                quanshr = Double.parseDouble(persons.get(i).getShr());
                                                                String strcurprice = (doc.getData().get("price")) + "";
                                                                curprice = Double.parseDouble(strcurprice);
                                                                ttlcpri = quanshr * curprice;
                                                                pastbpri = Double.parseDouble(persons.get(i).buyp);
//                                                               String strpastttlbpri = personsport.get(i).ttlbp;
//                                                               String strpastttlbpri = personsport.get(i).ttlbp;
//                                                               strpastttlbpri = strpastttlbpri.replace(getResources().getString(R.string.euro)+"","");
                                                                pastttlbpri = pastbpri * quanshr;
                                                                difbpri = curprice - pastbpri;
                                                                difttlbpri = ttlcpri - pastttlbpri;


                                                                Log.e("pnm", String.valueOf(doc.getData().get("p_n") + "" + " " + ttlcpri+" "+pastbpri+"  "+pastttlbpri));

                                                            } catch (Exception ex) {
                                                            }

                                                            note1.setPn(String.valueOf(doc.getData().get("p_n") + ""));
                                                            note1.setTn(String.valueOf(doc.getData().get("t_n") + ""));
                                                            note1.setCntry(String.valueOf(doc.getData().get("cntry") + ""));
                                                            note1.setPtion(String.valueOf(doc.getData().get("ption") + ""));
                                                            note1.setPside(String.valueOf(doc.getData().get("p_side") + ""));
                                                            note1.setBdate(String.valueOf(doc.getData().get("b_date") + ""));
                                                            note1.setWht(String.valueOf(doc.getData().get("wht") + ""));
                                                            note1.setHigt(String.valueOf(doc.getData().get("higt") + ""));
//                                                        note1.setShr(String.valueOf(doc.getData().get("srtno") + ""));
                                                            note1.setBplce(String.valueOf(doc.getData().get("b_plce") + ""));

                                                            note1.setCurbprce(new DecimalFormat(".##").format(curprice) + "");
                                                            note1.setTtlCbprce(new DecimalFormat(".##").format(ttlcpri) + "");
                                                            note1.setCselp(String.valueOf(doc.getData().get("spri") + ""));
                                                            note1.setImg(doc.getData().get("img") + "");
                                                        note1.setCshr(persons.get(i).shr+"");
//                                                        note1.setBuyp(personsport.get(i).buyp);
                                                            note1.setTtbp(getResources().getString(R.string.euro) + "" + pastttlbpri);
//                                                        note1.setBremain(personsport.get(i).bremain);
//                                                        note1.setPid(personsport.get(i).pid);
                                                            note1.setDifbprce(new DecimalFormat(".##").format(difbpri) + "");
                                                            note1.setDifttlbpri(new DecimalFormat(".##").format(difttlbpri) + "");


                                                            note1.setId(persons.get(i).getId());
                                                            note1.setBuyp(persons.get(i).getBuyp());
                                                            note1.setPid(persons.get(i).getPid());
                                                            note1.setSellp(persons.get(i).getSellp());
                                                            note1.setShr(persons.get(i).getShr());
                                                            note1.setSts(persons.get(i).getSts());
                                                            note1.setTranctdate(persons.get(i).getTranctdate());
                                                            note1.setUid(persons.get(i).getUid());
//                                                            notesList.add(note1);
                                                            personsselqfinal.add(note1);
                                                        }
                                                    }
                                                }
//                                                notesList.add(note);

//                                                persons.add(note);


                                            }


                                            qadapter = new RVAdapterPortpolioSellQue(user_id,firestoreDB, amt, "Home",getActivity(), personsselqfinal, new CustomItemClickListenerHomepage() {
                                                @Override
                                                public void onItemClick(View v, int position) {
//                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                            rvqueue.setAdapter(qadapter);
                                            qadapter.notifyDataSetChanged();
                                            //                        mAdapter = new NoteRecyclerViewAdapter(notesList, getApplicationContext(), firestoreDB);
//                        recyclerView.setAdapter(mAdapter);
                                        }
                                    });



                        } else {
                            Log.d("ERR", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }





    private void loadportfolioList() {
        firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            List<PortfolioBean> notesList = new ArrayList<>();
                            personsport = new ArrayList<>();

                            for (DocumentSnapshot doc : task.getResult()) {
                                PortfolioBean note = new PortfolioBean();
//                                PortfolioBean note = doc.toObject(PortfolioBean.class);

                                    note.setId(doc.getId());
                                    note.setPimg(doc.getData().get("pimg") + "");
                                    note.setShr(doc.getData().get("shr") + "");
                                    note.setBuyp(doc.getData().get("buyp") + "");
                                    note.setTtlbp(doc.getData().get("ttlbp") + "");
                                    note.setBremain(doc.getData().get("bremain") + "");
                                    note.setPid(doc.getData().get("pid") + "");
//                                notesList.add(note);
                                    Log.e("sonaport", notesList.size() + "" + doc.getId());


                                    personsport.add(note);

                            }



                            firestoreListener = firestoreDB.collection("PlayerInfo")
                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                        @Override
                                        public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                                            if (e != null) {
                                                Log.e("err", "Listen failed!", e);
                                                return;
                                            }
                                           personfinal = new ArrayList<>();
                                            List<PortfolioBean> notesList = new ArrayList<>();

                                            for (DocumentSnapshot doc : documentSnapshots) {
                                                PortfolioBean note = new PortfolioBean();
//                            PortfolioBean note = doc.toObject(PortfolioBean.class);
                                                note.setId(doc.getId());
//                                                Log.e("pnm", String.valueOf(doc.getData().get("p_n")+""));
                                                note.setPn(String.valueOf(doc.getData().get("p_n")+""));

                                                   for(int i = 0; i < personsport.size(); i++){
                                                PortfolioBean note1 = new PortfolioBean();
//                            PortfolioBean note = doc.toObject(PortfolioBean.class);
                                                       if(personsport.get(i).id.equals(doc.getId())) {
                                                           String strshr = personsport.get(i).shr+"";
                                                           Log.e("sona", strshr+" s");
                                                           if(!strshr.equals("0")) {
                                                               note1.setId(personsport.get(i).id);
                                                               double quanshr = 0, curprice = 0, ttlcpri = 0, pastbpri = 0.0, pastttlbpri = 0.0, difbpri = 0.0, difttlbpri = 0.0;

                                                               try {

                                                                   quanshr = Double.parseDouble(personsport.get(i).shr);
                                                                   String strcurprice = (doc.getData().get("price")) + "";
                                                                   curprice = Double.parseDouble(strcurprice);
                                                                   ttlcpri = quanshr * curprice;
                                                                   pastbpri = Double.parseDouble(personsport.get(i).buyp);
//                                                               String strpastttlbpri = personsport.get(i).ttlbp;
//                                                               String strpastttlbpri = personsport.get(i).ttlbp;
//                                                               strpastttlbpri = strpastttlbpri.replace(getResources().getString(R.string.euro)+"","");
                                                                   pastttlbpri = pastbpri * quanshr;
                                                                   difbpri = curprice - pastbpri;
                                                                   difttlbpri = ttlcpri - pastttlbpri;


                                                                   Log.e("pnm", String.valueOf(doc.getData().get("p_n") + "" + " " + ttlcpri));

                                                               } catch (Exception ex) {
                                                               }
                                                               note1.setPn(String.valueOf(doc.getData().get("p_n") + ""));
                                                               note1.setT_n(String.valueOf(doc.getData().get("t_n") + ""));
                                                               note1.setCntry(String.valueOf(doc.getData().get("cntry") + ""));
                                                               note1.setPtion(String.valueOf(doc.getData().get("ption") + ""));
                                                               note1.setP_side(String.valueOf(doc.getData().get("p_side") + ""));
                                                               note1.setB_date(String.valueOf(doc.getData().get("b_date") + ""));
                                                               note1.setWht(String.valueOf(doc.getData().get("wht") + ""));
                                                               note1.setHigt(String.valueOf(doc.getData().get("higt") + ""));
                                                               note1.setShr(String.valueOf(doc.getData().get("srtno") + ""));
                                                               note1.setB_plce(String.valueOf(doc.getData().get("b_plce") + ""));

                                                               note1.setCurbprce(new DecimalFormat(".##").format(curprice) + "");
                                                               note1.setTtlCbprce(new DecimalFormat(".##").format(ttlcpri) + "");
                                                               note1.setSellp(String.valueOf(doc.getData().get("spri") + ""));
                                                               note1.setPimg(personsport.get(i).pimg);
                                                               note1.setShr(personsport.get(i).shr);
                                                               note1.setBuyp(personsport.get(i).buyp);
                                                               note1.setTtlbp(getResources().getString(R.string.euro) + "" + pastttlbpri);
                                                               note1.setBremain(personsport.get(i).bremain);
                                                               note1.setPid(personsport.get(i).pid);
                                                               note1.setDiffbprce(new DecimalFormat(".##").format(difbpri) + "");
                                                               note1.setDiffttlbpri(new DecimalFormat(".##").format(difttlbpri) + "");
                                                               notesList.add(note1);
                                                               personfinal.add(note1);
                                                           }
                                                       }
                                            }
                                                notesList.add(note);

//                                                persons.add(note);


                                            }


                                            RVAdapterPortpolio adapter = new RVAdapterPortpolio(user_id, firestoreDB, amt,"Home",getActivity(), personfinal, new CustomItemClickListenerHomepage() {
                                                @Override
                                                public void onItemClick(View v, int position) {
//                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                            rv.setAdapter(adapter);

//                        mAdapter = new NoteRecyclerViewAdapter(notesList, getApplicationContext(), firestoreDB);
//                        recyclerView.setAdapter(mAdapter);
                                        }
                                    });



                        } else {
                            Log.d("ERR", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }
    public void getamtFirebase(final String email){

        DocumentReference docRef = firestoreDB.collection("UserInfo").document(user_id).collection("Wallet").document("W"+user_id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                String stramt="0",sUniqId="0",sUPassword="0",sEmail="0", phn ="0", cntry = "",stts = "",id;

                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();

                    if (document.exists()) {
                        stramt = "" + document.getData().get("amt");
                        id = "" + document.getData().get("Id");
                        sUniqId=""+document.getId();
                        Log.e("sUniqId:--",""+sUniqId+" "+stramt);

                        amt = stramt;
//                        try {
//                            loadNotesList();
//                        }catch (Exception e){}


                        Log.d("sonaamt", "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d("sonaamt", "No such document");
                    }
                } else {
                    Log.d("sonaamt", "get failed with ", task.getException());
                }
            }
        });

    }

}
