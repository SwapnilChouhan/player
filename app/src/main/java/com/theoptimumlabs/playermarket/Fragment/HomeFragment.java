package com.theoptimumlabs.playermarket.Fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.theoptimumlabs.playermarket.Adapter.RVAdapter;
import com.theoptimumlabs.playermarket.HomeActivity;
import com.theoptimumlabs.playermarket.Model.AddPlayerBean;
import com.theoptimumlabs.playermarket.Model.SellQueueBean;
import com.theoptimumlabs.playermarket.PlayerMOdule.AddPlayer;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.SessionManagement.Session;
import com.theoptimumlabs.playermarket.utilsadded.CustomItemClickListenerHomepage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    public HomeFragment() {
        // Required empty public constructor
    }
    public static List<AddPlayerBean> persons;
    private RecyclerView rv;

    Session session;
    String user_id, amt;
    HashMap<String, String> hashMap = new HashMap<>();
    LinearLayout llworknot;
    private FirebaseFirestore firestoreDB;
    private ListenerRegistration firestoreListener;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "myprefamt";
    public static final String AMT = "amtKey";
    double dblprice;
    ImageView imgsort;
    List<SellQueueBean> queList;

    private EditText etsearch;
    private String[] moviewList;
    public static ArrayList<AddPlayerBean> movieNamesArrayList;
    public static ArrayList<AddPlayerBean> array_sort;
    int textlength = 0;
    ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_market, container, false);
        session = new Session(getActivity());
        if(session.isLoggedIn()){
            session.checkLogin();
            hashMap = session.getUser();
            user_id = hashMap.get(session.KEY_User_id);
            Log.e("useridsess", user_id+"");

        }
        sharedpreferences = getActivity().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        rv=(RecyclerView)view.findViewById(R.id.rv);
        imgsort=(ImageView) view.findViewById(R.id.imgsort);
        etsearch = (EditText) view.findViewById(R.id.editText);
        llworknot=(LinearLayout)view.findViewById(R.id.llworknot);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        String srrr =  HomeActivity.fromwhichacti;
        Log.e("sonacatch", "  dd"+srrr);
//        initializeData();
//        initializeAdapter();
        firestoreDB = FirebaseFirestore.getInstance();
        try {
            getamtFirebase(user_id);
        }catch (Exception e){
            try {
                loadNotesList("normal");
            }catch (Exception e1){}

        }

        try {
            getsellque();
        }catch (Exception e){}

        try {
            loadNotesList("normal");
        }catch (Exception e){}

        /*firestoreListener = firestoreDB.collection("PlayerInfo")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.e("err", "Listen failed!", e);
                            return;
                        }
                        persons = new ArrayList<>();
                        List<AddPlayerBean> notesList = new ArrayList<>();

                        for (DocumentSnapshot doc : documentSnapshots) {
                            AddPlayerBean note = doc.toObject(AddPlayerBean.class);
                            note.setId(doc.getId());
                            notesList.add(note);
                            persons.add(note);
                        }
                        RVAdapter adapter = new RVAdapter(user_id, firestoreDB, amt,"Home",getActivity(), persons, new CustomItemClickListenerHomepage() {
                            @Override
                            public void onItemClick(View v, int position) {
//                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
                            }
                        });
                        rv.setAdapter(adapter);

//                        mAdapter = new NoteRecyclerViewAdapter(notesList, getApplicationContext(), firestoreDB);
//                        recyclerView.setAdapter(mAdapter);
                    }
                });
*/

        imgsort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout llhightolow, lllowtohigh, llnone;
                View view = getLayoutInflater().inflate(R.layout.custom_dialog, null);
                lllowtohigh = (LinearLayout)view.findViewById(R.id.lllowtohigh);
                llhightolow = (LinearLayout)view.findViewById(R.id.llhightolow);
                llnone = (LinearLayout)view.findViewById(R.id.llnone);

                final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                llhightolow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        try {
                            loadNotesList("hightolow");
                        }catch (Exception e){}
                    }
                });
                llnone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        try {
                            loadNotesList("normal");
                        }catch (Exception e){}
                    }
                });
                lllowtohigh.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        try {
                            loadNotesList("lowtohigh");
                        }catch (Exception e){}
                    }
                });
                dialog.setContentView(view);
                dialog.show();
            }
        });

        etsearch.addTextChangedListener(new TextWatcher() {


            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textlength = etsearch.getText().length();
                try {
                    array_sort.clear();
                    for (int i = 0; i < movieNamesArrayList.size(); i++) {
                        if (textlength <= movieNamesArrayList.get(i).getP_n().length()) {
                            Log.d("ertyyy", movieNamesArrayList.get(i).getP_n().toLowerCase().trim());
                            if (movieNamesArrayList.get(i).getP_n().toLowerCase().trim().contains(
                                    etsearch.getText().toString().toLowerCase().trim())) {
                                array_sort.add(movieNamesArrayList.get(i));
                            }
                        }
                    }
//                adapter = new SearchAdapter(SearchCricket.this, array_sort);
//                recyclerView.setAdapter(adapter);
//                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                    RVAdapter adapter = new RVAdapter(queList, user_id, firestoreDB, amt, "Home", getActivity(), array_sort, new CustomItemClickListenerHomepage() {
                        @Override
                        public void onItemClick(View v, int position) {
//                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
                        }
                    });
                    rv.setAdapter(adapter);
                }catch (Exception e){}
            }
        });

        return view;

    }

    private void initializeData(){
        persons = new ArrayList<>();
//        persons.add(new AddPlayerBean("Emma Wilson", "23 years old", R.drawable.emma));

    }

    //    private void initializeAdapter(){
//
//        RVAdapter adapter = new RVAdapter(getActivity(), persons, new CustomItemClickListenerHomepage() {
//            @Override
//            public void onItemClick(View v, int position) {
////                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
//            }
//        });
//        rv.setAdapter(adapter);
//    }
    private void loadNotesList(final String strlowhigh) {
        firestoreDB.collection("PlayerInfo")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<AddPlayerBean> notesList = new ArrayList<>();
                            persons = new ArrayList<>();
                            progressBar.setVisibility(View.GONE);

                            for (DocumentSnapshot doc : task.getResult()) {
//                                AddPlayerBean note = doc.toObject(AddPlayerBean.class);
                                final AddPlayerBean note = new AddPlayerBean();
                                note.setId(doc.getId());
                                notesList.add(note);
                                final String pid = doc.getId()+"";

                                note.setImg(doc.getData().get("img")+"");
                                note.setP_n(doc.getData().get("p_n")+"");
                                note.setT_n(doc.getData().get("t_n")+"");
                                note.setCntry(doc.getData().get("cntry")+"");
                                note.setPtion(doc.getData().get("ption")+"");
                                note.setP_side(doc.getData().get("p_side")+"");
                                note.setB_date(doc.getData().get("b_date")+"");
                                note.setWht(doc.getData().get("wht")+"");
                                note.setHigt(doc.getData().get("higt")+"");
                                note.setSrtno(doc.getData().get("srtno")+"");
                                note.setB_plce(doc.getData().get("b_plce")+"");
                                note.setSpri(doc.getData().get("spri")+"");
//                                note.setPrice(doc.getData().get("price")+"");
                                try {
                                    String strprice = doc.getData().get("price") + "";

                                    dblprice = Double.parseDouble(strprice);
                                }catch (Exception e){
                                    dblprice  = 0.0;
                                }
                                note.setPrice(dblprice+"");

                                note.setIntprice((int) dblprice);
                                Log.e("pricc", "" + note.getIntprice());

                                try{
                                    DocumentReference docRef = firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio").document(pid);
//        DocumentReference docRef = fsdb.collection("UserInfo").document(email);
                                    docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                            String shrquan="0",sUniqId="0",sUPassword="0",sEmail="0", phn ="0", cntry = "",stts = "",id;

                                            if (task.isSuccessful()) {
                                                DocumentSnapshot document = task.getResult();

                                                if (document.exists()) {
                                                    shrquan = "" + document.getData().get("shr");
//                                                sUPassword = "" + document.getData().get("PSW");
//                                                sEmail = "" + document.getData().get("EMail");
//                                                phn = "" + document.getData().get("PHN");
//                                                cntry = "" + document.getData().get("CNTRY");
//                                                stts = "" + document.getData().get("STs");
//                                                id = "" + document.getData().get("Id");
//                                                sUniqId=""+document.getId();
//                                                Log.e("sUniqId:--",""+sUniqId+" "+sUName);

                                                    note.setShr(shrquan);
                                                    note.setPreshr(shrquan);
                                                    Log.d("sonalishrgetre", "DocumentSnapshot data: " +pid+" "+ shrquan);
//                             Log.d("sonalishrgetre", "DocumentSnapshot data: " +pid+" "+ document.getData()+"shr");
                                                } else {
                                                    Log.d("sonalishrget", "No such document");
                                                }
                                            } else {
                                                Log.d("sonalishrget", "get failed with ", task.getException());
                                            }
                                        }
                                    });
                                }catch (Exception e){}
                                Log.e("sona", notesList.size()+""+doc.getId());
                                persons.add(note);
//                                Collections.sort(persons);
                                if(strlowhigh.equals("hightolow")) {
                                    Collections.sort(persons, new Comparator<AddPlayerBean>() {
                                        @Override
                                        public int compare(AddPlayerBean o1, AddPlayerBean o2) {
                                            String s1 = ((AddPlayerBean) o1).getPrice();
                                            String s2 = ((AddPlayerBean) o2).getPrice();
                                            //asending order
//                                        return s1.compareTo(s2);
                                            //descendin
                                            return s2.compareTo(s1);
                                        }
                                    });
                                    for (AddPlayerBean addPlayerBean : persons) {
                                        System.out.println(addPlayerBean.p_n + " asss");
                                    }
                                }else if(strlowhigh.equals("lowtohigh")){
                                    Collections.sort(persons, new Comparator<AddPlayerBean>() {
                                        @Override
                                        public int compare(AddPlayerBean o1, AddPlayerBean o2) {
                                            String s1 = ((AddPlayerBean) o1).getPrice();
                                            String s2 = ((AddPlayerBean) o2).getPrice();
                                            //asending order
                                            return s1.compareTo(s2);
                                            //descendin
//                                            return s2.compareTo(s1);
                                        }
                                    });
                                    for (AddPlayerBean addPlayerBean : persons) {
                                        System.out.println(addPlayerBean.p_n + " asss");
                                    }
                                }else {}

                            }
                            movieNamesArrayList = populateList();
                            Log.d("search wali api", movieNamesArrayList.size() + ""+populateList());
//                            adapter = new SearchAdapter(getActivity(),movieNamesArrayList);
//                            rv.setAdapter(adapter);
//                            rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

//                            array_sort = new ArrayList<>();
//                            array_sort = populateList();

                            RVAdapter adapter = new RVAdapter(queList, user_id,firestoreDB, amt, "Home",getActivity(), persons, new CustomItemClickListenerHomepage() {
                                @Override
                                public void onItemClick(View v, int position) {
//                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
                                }
                            });
                            rv.setAdapter(adapter);

                            array_sort = new ArrayList<>();
                            array_sort = populateList();

                        } else {
                            Log.d("ERR", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }
    public void getamtFirebase(final String email){

        DocumentReference docRef = firestoreDB.collection("UserInfo").document(user_id).collection("Wallet").document("W"+user_id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                String stramt="0",sUniqId="0",sUPassword="0",sEmail="0", phn ="0", cntry = "",stts = "",id;

                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();

                    if (document.exists()) {
                        stramt = "" + document.getData().get("amt");
                        id = "" + document.getData().get("Id");
                        sUniqId=""+document.getId();
                        Log.e("sUniqIdamt:--",""+sUniqId+" "+stramt);

                        amt = stramt;
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString(AMT, amt);
                        editor.commit();
                        try {
                            loadNotesList("normal");
                        }catch (Exception e){}


                        Log.d("sonaamt", "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d("sonaamt", "No such document");
                    }
                } else {
                    Log.d("sonaamt", "get failed with ", task.getException());
                }
            }
        });

    }

    //////search
    private ArrayList<AddPlayerBean> populateList(){

        ArrayList<AddPlayerBean> list = new ArrayList<>();
        Log.e("sonali", persons.size()+" "+list.size());
        for(int i = 0; i < persons.size(); i++){
//        for(int i = 0; i < 8; i++){
            AddPlayerBean imageModel = new AddPlayerBean();


            imageModel.setId(persons.get(i).getId());
//            notesList.add(imageModel);
            final String pid = persons.get(i).getId()+"";

            imageModel.setImg(persons.get(i).getImg()+"");
            imageModel.setP_n(persons.get(i).getP_n()+"");
            imageModel.setT_n(persons.get(i).getT_n()+"");
            imageModel.setCntry(persons.get(i).getCntry()+"");
            imageModel.setPtion(persons.get(i).getPtion()+"");
            imageModel.setP_side(persons.get(i).getP_side()+"");
            imageModel.setB_date(persons.get(i).getB_date()+"");
            imageModel.setWht(persons.get(i).getWht()+"");
            imageModel.setHigt(persons.get(i).getHigt()+"");
            imageModel.setSrtno(persons.get(i).getSrtno()+"");
            imageModel.setB_plce(persons.get(i).getB_plce()+"");
            imageModel.setSpri(persons.get(i).getSpri()+"");
//                                imageModel.setPrice(doc.getData().get("price")+"");
            try {
                String strprice = persons.get(i).getPrice() + "";

                dblprice = Double.parseDouble(strprice);
            }catch (Exception e){
                dblprice  = 0.0;
            }
            imageModel.setPrice(dblprice+"");

            imageModel.setIntprice((int) dblprice);



//            imageModel.setName(moviewList[i]);



            list.add(imageModel);
        }

        return list;
    }
    public void getsellque(){
        firestoreDB.collection("SellQueue")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.e("sonaliqg", "Listen failed!", e);
                            return;
                        }

                        queList = new ArrayList<>();

                        for (DocumentSnapshot doc : documentSnapshots) {
                            String resuid = doc.getData().get("uid")+"";
//                            if(resuid.equals(user_id)) {
                            SellQueueBean note = new SellQueueBean();
                            note.setId(doc.getId());
                            note.setId(doc.getData().get("Id") + "");
                            note.setBuyp(doc.getData().get("buyp") + "");
                            note.setPid(doc.getData().get("pid") + "");
                            note.setSellp(doc.getData().get("sellp") + "");
                            note.setShr(doc.getData().get("shr") + "");
                            note.setSts(doc.getData().get("sts") + "");
                            note.setTranctdate(doc.getData().get("tranctdate") + "");
                            note.setUid(doc.getData().get("uid") + "");

                            final String pid = doc.getData().get("pid") + "";
                            final String sellshr = doc.getData().get("shr") + "";
                            final String buypselltime = doc.getData().get("buyp") + "";
                            final String selpselltime = doc.getData().get("sellp") + "";
                            final String sts = doc.getData().get("sts") + "";
                            final String Tranctdate = doc.getData().get("tranctdate") + "";
                            final String uid = doc.getData().get("uid") + "";

//                                notesList.add(note);


                            queList.add(note);
                            Log.e("sonaqg", queList.size() + "" + doc.getId());

//                            }


                        }


                    }
                });
    }


}