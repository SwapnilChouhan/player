package com.theoptimumlabs.playermarket.Fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.theoptimumlabs.playermarket.Adapter.HistoryAdapter;
import com.theoptimumlabs.playermarket.Adapter.HistoryWalletAdapter;
import com.theoptimumlabs.playermarket.Adapter.RVAdapterPortpolio;
import com.theoptimumlabs.playermarket.Adapter.RVAdapterPortpolioSellQue;
import com.theoptimumlabs.playermarket.Model.HistoryBean;
import com.theoptimumlabs.playermarket.Model.PortfolioBean;
import com.theoptimumlabs.playermarket.Model.SellQueueBean;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.SessionManagement.Session;
import com.theoptimumlabs.playermarket.utilsadded.CustomItemClickListenerHomepage;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryWalletFragment extends Fragment {


    public HistoryWalletFragment() {
        // Required empty public constructor
    }
     List<HistoryBean> hisbuyList;
    public  List<SellQueueBean> persons;
    public  List<HistoryBean> personsbuysell;
    public  List<PortfolioBean> personsport;
    public  List<PortfolioBean> personfinal;
    private RecyclerView rv;
    Session session;
    String user_id, amt;
    HashMap<String, String> hashMap = new HashMap<>();
    LinearLayout llworknot;
    private FirebaseFirestore firestoreDB;
    private ListenerRegistration firestoreListener;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "myprefamt";
    public static final String AMT = "amtKey";
    ProgressBar progressBar;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_history, container, false);
        session = new Session(getActivity());
        if(session.isLoggedIn()){
            session.checkLogin();
            hashMap = session.getUser();
            user_id = hashMap.get(session.KEY_User_id);
            Log.e("useridsess", user_id+"");

        }
        sharedpreferences = getActivity().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(AMT)) {
           amt = (sharedpreferences.getString(AMT, ""));
        }
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);

        rv=(RecyclerView)view.findViewById(R.id.rv);
        llworknot=(LinearLayout)view.findViewById(R.id.llworknot);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

//        initializeData();
//        initializeAdapter();
        firestoreDB = FirebaseFirestore.getInstance();

        try {
            loaddataforhistory();
        }catch (Exception e){}
//    try {
//            loadportfolioList();
//        }catch (Exception e){}


        return view;

    }

    private void initializeData(){
        persons = new ArrayList<>();
//        persons.add(new PortfolioBean("Emma Wilson", "23 years old", R.drawable.emma));

    }

    //    private void initializeAdapter(){
//
//        RVAdapterPortpolio adapter = new RVAdapterPortpolio(getActivity(), persons, new CustomItemClickListenerHomepage() {
//            @Override
//            public void onItemClick(View v, int position) {
////                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
//            }
//        });
//        rv.setAdapter(adapter);
//    }









    private void loaddataforhistory() {
        personsbuysell = new ArrayList<>();

        firestoreDB.collection("UserInfo").document(user_id).collection("Wallethistory")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            progressBar.setVisibility(View.GONE);

                            for (final DocumentSnapshot doc : task.getResult()) {
//                                PortfolioBean note = new PortfolioBean();
//                                PortfolioBean note = doc.toObject(PortfolioBean.class);
                                final String piddoc = doc.getId()+"";
//                                    note.setId(doc.getId());
//                                    note.setPimg(doc.getData().get("pimg") + "");
//                                    note.setShr(doc.getData().get("shr") + "");
//                                    note.setBuyp(doc.getData().get("buyp") + "");
//                                    note.setTtlbp(doc.getData().get("ttlbp") + "");
//                                    note.setBremain(doc.getData().get("bremain") + "");
//                                    note.setPid(doc.getData().get("pid") + "");
//                                notesList.add(note);
                                    Log.e("sonagetwallethistory",  "" + doc.getId());



//                                                    HistoryBean note = doc.toObject(HistoryBean.class);
                                    HistoryBean note = new HistoryBean();
                                    note.setId(piddoc);
                                    note.setTimestamp(doc.getData().get("tranctTime")+"");
                                    note.setSellp(doc.getData().get("amt")+"");
                                personsbuysell.add(note);
                                }


//                                                mAdapter = new NoteRecyclerViewAdapter(notesList, getApplicationContext(), firestoreDB);
//                                                recyclerView.setAdapter(mAdapter);

                                Collections.sort(personsbuysell, new Comparator<HistoryBean>() {
                                    @Override
                                    public int compare(HistoryBean o1, HistoryBean o2) {
                                        String s1 = ((HistoryBean) o1).getTimestamp();
                                        String s2 = ((HistoryBean) o2).getTimestamp();
                                        //asending order
//                                        return s1.compareTo(s2);
                                        //descendin
                                        return s2.compareTo(s1);
                                    }
                                });
                                for (HistoryBean addPlayerBean : personsbuysell) {
                                    System.out.println(addPlayerBean.pn + " asss");
                                }
                                HistoryWalletAdapter adapter = new HistoryWalletAdapter(getActivity(), personsbuysell, new CustomItemClickListenerHomepage() {
                                    @Override
                                    public void onItemClick(View v, int position) {
//                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
                                    }
                                });
                                rv.setAdapter(adapter);



                        } else {
                            Log.d("ERR", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }


}
