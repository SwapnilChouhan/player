package com.theoptimumlabs.playermarket.Fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.SessionManagement.Session;
import com.theoptimumlabs.playermarket.Wallet.Wallet;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class UnderConstruction extends Fragment {


    public UnderConstruction() {
        // Required empty public constructor
    }
    SharedPreferences sharedpreferences;
    public static final String mypreference = "myprefamt";
    public static final String AMT = "amtKey";
    Button btnpay;
    String amt;
    TextView txtconty, txtmail, txtname;
    Fragment fragment;
    Session session;
    String user_id, usernm, contry;
    HashMap<String, String> hashMap = new HashMap<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_under_construction, container, false);
        sharedpreferences = getActivity().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);

        txtconty=(TextView) view.findViewById(R.id.txtconty);
        txtmail=(TextView) view.findViewById(R.id.txtmail);
        txtname=(TextView) view.findViewById(R.id.txtname);
        btnpay=(Button) view.findViewById(R.id.btnpay);
        if (sharedpreferences.contains(AMT)) {
            amt = (sharedpreferences.getString(AMT, ""));
            btnpay.setText("Wallet "+getResources().getString(R.string.euro)+amt);

        }
        session = new Session(getActivity());
        if(session.isLoggedIn()){
            session.checkLogin();
            hashMap = session.getUser();
            user_id = hashMap.get(session.KEY_User_id);
            usernm = hashMap.get(session.KEY_NAME);
            contry = hashMap.get(session.KEY_IMAGE_URL);
            Log.e("useridsess", user_id+"");

        }
        txtconty.setText(contry);
        txtmail.setText(user_id);
        txtname.setText(usernm);

        int[] color = {Color.DKGRAY, Color.CYAN};
        float[] position = {0, 1};
        Shader shader1 = new LinearGradient(0, 0, 0, 50,color,position, Shader.TileMode.MIRROR);
        txtname.getPaint().setShader(shader1);

//        Shader shader2 = new LinearGradient(0, 0, 0, 200,color,position, Shader.TileMode.REPEAT);
//        tv2.getPaint().setShader(shader2);

        Shader shader3 = new RadialGradient(0, 3, 5, color[0], color[1], Shader.TileMode.REPEAT);
        txtmail.getPaint().setShader(shader3);
        btnpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), Wallet.class);
                startActivity(i);
            }
        });

//        fragment = new HistoricalFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString("name", al_main.get(0).getStr_country());
//        bundle.putString("des", al_main.get(0).getAl_state().get(0).getStr_description());
//        bundle.putString("dish", al_main.get(0).getAl_state().get(0).getStr_name());
//        bundle.putString("image", al_main.get(0).getAl_state().get(0).getStr_image());
//        tv_name.setText(al_main.get(0).getStr_country());

//        fragment.setArguments(bundle);
//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment, "History").addToBackStack("null").commit();

        return  view;
    }
  /*  DocumentReference docRef = firestoreDB.collection("PlayerInfo").document(""+doc.getId());
                                docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
        @Override
        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    Log.d("sonek", "DocumentSnapshot data: " + document.getData().get("p_n"));
                } else {
                    Log.d("sonek", "No such document");
                }
            } else {
                Log.d("sonek", "get failed with ", task.getException());
            }
        }
    });*/
}
