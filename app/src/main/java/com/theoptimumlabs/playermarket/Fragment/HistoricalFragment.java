package com.theoptimumlabs.playermarket.Fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.theoptimumlabs.playermarket.Adapter.HistoryAdapter;
import com.theoptimumlabs.playermarket.Adapter.RVAdapterPortpolio;
import com.theoptimumlabs.playermarket.Adapter.RVAdapterPortpolioSellQue;
import com.theoptimumlabs.playermarket.Model.AddPlayerBean;
import com.theoptimumlabs.playermarket.Model.HistoryBean;
import com.theoptimumlabs.playermarket.Model.PortfolioBean;
import com.theoptimumlabs.playermarket.Model.SellQueueBean;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.SessionManagement.Session;
import com.theoptimumlabs.playermarket.utilsadded.CustomItemClickListenerHomepage;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoricalFragment extends Fragment {


    public HistoricalFragment() {
        // Required empty public constructor
    }
     List<HistoryBean> hisbuyList;
    List<HistoryBean> hissellList;
    public  List<SellQueueBean> persons;
    public  List<HistoryBean> personsbuysell;
    public  List<PortfolioBean> personsport;
    public  List<PortfolioBean> personfinal;
    private RecyclerView rv;
    RVAdapterPortpolioSellQue qadapter;
    Session session;
    String user_id, amt;
    HashMap<String, String> hashMap = new HashMap<>();
    LinearLayout llworknot;
    private FirebaseFirestore firestoreDB;
    private ListenerRegistration firestoreListener;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "myprefamt";
    public static final String AMT = "amtKey";
    ProgressBar progressBar;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_history, container, false);
        session = new Session(getActivity());
        if(session.isLoggedIn()){
            session.checkLogin();
            hashMap = session.getUser();
            user_id = hashMap.get(session.KEY_User_id);
            Log.e("useridsess", user_id+"");

        }
        sharedpreferences = getActivity().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);
        if (sharedpreferences.contains(AMT)) {
           amt = (sharedpreferences.getString(AMT, ""));
        }
        progressBar = (ProgressBar)view.findViewById(R.id.progressBar);

        rv=(RecyclerView)view.findViewById(R.id.rv);
        llworknot=(LinearLayout)view.findViewById(R.id.llworknot);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

//        initializeData();
//        initializeAdapter();
        firestoreDB = FirebaseFirestore.getInstance();

        try {
            loaddataforhistory();
        }catch (Exception e){}
//    try {
//            loadportfolioList();
//        }catch (Exception e){}


        return view;

    }

    private void initializeData(){
        persons = new ArrayList<>();
//        persons.add(new PortfolioBean("Emma Wilson", "23 years old", R.drawable.emma));

    }

    //    private void initializeAdapter(){
//
//        RVAdapterPortpolio adapter = new RVAdapterPortpolio(getActivity(), persons, new CustomItemClickListenerHomepage() {
//            @Override
//            public void onItemClick(View v, int position) {
////                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
//            }
//        });
//        rv.setAdapter(adapter);
//    }









    private void loaddataforhistory() {
        personsbuysell = new ArrayList<>();

        firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            progressBar.setVisibility(View.GONE);

                            for (final DocumentSnapshot doc : task.getResult()) {
//                                PortfolioBean note = new PortfolioBean();
//                                PortfolioBean note = doc.toObject(PortfolioBean.class);
                                final String piddoc = doc.getId()+"";
//                                    note.setId(doc.getId());
//                                    note.setPimg(doc.getData().get("pimg") + "");
//                                    note.setShr(doc.getData().get("shr") + "");
//                                    note.setBuyp(doc.getData().get("buyp") + "");
//                                    note.setTtlbp(doc.getData().get("ttlbp") + "");
//                                    note.setBremain(doc.getData().get("bremain") + "");
//                                    note.setPid(doc.getData().get("pid") + "");
//                                notesList.add(note);
                                    Log.e("sonaget",  "" + doc.getId());
                                firestoreListener = firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio").document(piddoc).collection("AgainBuyLIst")
                                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                            @Override
                                            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                                                if (e != null) {
                                                    Log.e("sonaligetagainlist", "Listen failed!", e);
                                                    return;
                                                }

                                              hisbuyList = new ArrayList<>();

                                                for (DocumentSnapshot doc : documentSnapshots) {
//                                                    HistoryBean note = doc.toObject(HistoryBean.class);
                                                    HistoryBean note = new HistoryBean();
                                                    note.setId(piddoc);
                                                    note.setBuyp(doc.getData().get("buyp")+"");
                                                    note.setShr(doc.getData().get("shr")+"");
                                                    note.setRole(("buy")+"");
                                                    note.setPn(doc.getData().get("pn")+"");
                                                    note.setTimestamp(doc.getData().get("tranctTime")+"");
                                                    note.setSellp("");
                                                    try {
                                                        double dbuy = Double.parseDouble(doc.getData().get("buyp") + "");
                                                        int shr = Integer.parseInt(doc.getData().get("shr") + "");
                                                        double dttl = dbuy * shr;
                                                        note.setTtl(dttl+"");
                                                        Log.e("ttl", dttl+" bb "+dbuy+" "+shr);


                                                    }catch (Exception e1){}
                                                    Log.d("SonaHis", " buy "+doc.getId());
                                                    hisbuyList.add(note);
                                                }
                                                personsbuysell.addAll(hisbuyList);


                                            }
                                        });

                                firestoreListener = firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio").document(piddoc).collection("Sellorder")
                                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                            @Override
                                            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                                                if (e != null) {
                                                    Log.e("sonaligetagainlist", "Listen failed!", e);
                                                    return;
                                                }
                                                 hissellList = new ArrayList<>();

                                                for (DocumentSnapshot doc : documentSnapshots) {
//                                                    HistoryBean note = doc.toObject(HistoryBean.class);
                                                    HistoryBean note = new HistoryBean();
                                                    note.setId(piddoc);
                                                    note.setBuyp(doc.getData().get("buyp")+"");
                                                    note.setShr(doc.getData().get("shr")+"");
                                                    note.setPn(doc.getData().get("pn")+"");
                                                    note.setRole(("sell")+"");
                                                    note.setTimestamp(doc.getData().get("tranctTime")+"");
                                                    note.setSellp(doc.getData().get("sellp")+"");
                                                    try {
                                                        double dsell = Double.parseDouble(doc.getData().get("sellp") + "");
                                                        int shr = Integer.parseInt(doc.getData().get("shr") + "");
                                                        double dttl = dsell * shr;
                                                        Log.e("ttl", dttl+" "+dsell+" "+shr);
                                                        note.setTtl(dttl+"");

                                                    }catch (Exception e1){}
                                                    Log.d("SonaHis", " Sell "+doc.getId());

                                                    hissellList.add(note);
                                                }


//                                                mAdapter = new NoteRecyclerViewAdapter(notesList, getApplicationContext(), firestoreDB);
//                                                recyclerView.setAdapter(mAdapter);
                                                personsbuysell.addAll(hissellList);

                                                Collections.sort(personsbuysell, new Comparator<HistoryBean>() {
                                                    @Override
                                                    public int compare(HistoryBean o1, HistoryBean o2) {
                                                        String s1 = ((HistoryBean) o1).getTimestamp();
                                                        String s2 = ((HistoryBean) o2).getTimestamp();
                                                        //asending order
//                                        return s1.compareTo(s2);
                                                        //descendin
                                                        return s2.compareTo(s1);
                                                    }
                                                });
                                                for (HistoryBean addPlayerBean : personsbuysell) {
                                                    System.out.println(addPlayerBean.pn + " asss");
                                                }
                                                HistoryAdapter adapter = new HistoryAdapter(getActivity(), personsbuysell, new CustomItemClickListenerHomepage() {
                                                    @Override
                                                    public void onItemClick(View v, int position) {
//                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                                rv.setAdapter(adapter);

                                            }
                                        });

                            }



                            /*firestoreListener = firestoreDB.collection("PlayerInfo")
                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                        @Override
                                        public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                                            if (e != null) {
                                                Log.e("err", "Listen failed!", e);
                                                return;
                                            }
                                           personfinal = new ArrayList<>();
                                            List<PortfolioBean> notesList = new ArrayList<>();

                                            for (DocumentSnapshot doc : documentSnapshots) {
                                                PortfolioBean note = new PortfolioBean();
//                            PortfolioBean note = doc.toObject(PortfolioBean.class);
                                                note.setId(doc.getId());
//                                                Log.e("pnm", String.valueOf(doc.getData().get("p_n")+""));
                                                note.setPn(String.valueOf(doc.getData().get("p_n")+""));

                                                   for(int i = 0; i < personsport.size(); i++){
                                                PortfolioBean note1 = new PortfolioBean();
//                            PortfolioBean note = doc.toObject(PortfolioBean.class);
                                                       if(personsport.get(i).id.equals(doc.getId())) {
                                                           String strshr = personsport.get(i).shr+"";
                                                           Log.e("sona", strshr+" s");
                                                           if(!strshr.equals("0")) {
                                                               note1.setId(personsport.get(i).id);
                                                               double quanshr = 0, curprice = 0, ttlcpri = 0, pastbpri = 0.0, pastttlbpri = 0.0, difbpri = 0.0, difttlbpri = 0.0;

                                                               try {

                                                                   quanshr = Double.parseDouble(personsport.get(i).shr);
                                                                   String strcurprice = (doc.getData().get("price")) + "";
                                                                   curprice = Double.parseDouble(strcurprice);
                                                                   ttlcpri = quanshr * curprice;
                                                                   pastbpri = Double.parseDouble(personsport.get(i).buyp);
//                                                               String strpastttlbpri = personsport.get(i).ttlbp;
//                                                               String strpastttlbpri = personsport.get(i).ttlbp;
//                                                               strpastttlbpri = strpastttlbpri.replace(getResources().getString(R.string.euro)+"","");
                                                                   pastttlbpri = pastbpri * quanshr;
                                                                   difbpri = curprice - pastbpri;
                                                                   difttlbpri = ttlcpri - pastttlbpri;


                                                                   Log.e("pnm", String.valueOf(doc.getData().get("p_n") + "" + " " + ttlcpri));

                                                               } catch (Exception ex) {
                                                               }
                                                               note1.setPn(String.valueOf(doc.getData().get("p_n") + ""));
                                                               note1.setT_n(String.valueOf(doc.getData().get("t_n") + ""));
                                                               note1.setCntry(String.valueOf(doc.getData().get("cntry") + ""));
                                                               note1.setPtion(String.valueOf(doc.getData().get("ption") + ""));
                                                               note1.setP_side(String.valueOf(doc.getData().get("p_side") + ""));
                                                               note1.setB_date(String.valueOf(doc.getData().get("b_date") + ""));
                                                               note1.setWht(String.valueOf(doc.getData().get("wht") + ""));
                                                               note1.setHigt(String.valueOf(doc.getData().get("higt") + ""));
                                                               note1.setShr(String.valueOf(doc.getData().get("srtno") + ""));
                                                               note1.setB_plce(String.valueOf(doc.getData().get("b_plce") + ""));

                                                               note1.setCurbprce(new DecimalFormat(".##").format(curprice) + "");
                                                               note1.setTtlCbprce(new DecimalFormat(".##").format(ttlcpri) + "");
                                                               note1.setSellp(String.valueOf(doc.getData().get("spri") + ""));
                                                               note1.setPimg(personsport.get(i).pimg);
                                                               note1.setShr(personsport.get(i).shr);
                                                               note1.setBuyp(personsport.get(i).buyp);
                                                               note1.setTtlbp(getResources().getString(R.string.euro) + "" + pastttlbpri);
                                                               note1.setBremain(personsport.get(i).bremain);
                                                               note1.setPid(personsport.get(i).pid);
                                                               note1.setDiffbprce(new DecimalFormat(".##").format(difbpri) + "");
                                                               note1.setDiffttlbpri(new DecimalFormat(".##").format(difttlbpri) + "");
                                                               notesList.add(note1);
                                                               personfinal.add(note1);
                                                           }
                                                       }
                                            }
                                                notesList.add(note);

//                                                persons.add(note);


                                            }


                                            RVAdapterPortpolio adapter = new RVAdapterPortpolio(user_id, firestoreDB, amt,"Home",getActivity(), personfinal, new CustomItemClickListenerHomepage() {
                                                @Override
                                                public void onItemClick(View v, int position) {
//                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                            rv.setAdapter(adapter);

//                        mAdapter = new NoteRecyclerViewAdapter(notesList, getApplicationContext(), firestoreDB);
//                        recyclerView.setAdapter(mAdapter);
                                        }
                                    });
*/


                        } else {
                            Log.d("ERR", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    private void loadportfolioList() {
        firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<PortfolioBean> notesList = new ArrayList<>();
                            personsport = new ArrayList<>();

                            for (DocumentSnapshot doc : task.getResult()) {
                                PortfolioBean note = new PortfolioBean();
//                                PortfolioBean note = doc.toObject(PortfolioBean.class);

                                    note.setId(doc.getId());
                                    note.setPimg(doc.getData().get("pimg") + "");
                                    note.setShr(doc.getData().get("shr") + "");
                                    note.setBuyp(doc.getData().get("buyp") + "");
                                    note.setTtlbp(doc.getData().get("ttlbp") + "");
                                    note.setBremain(doc.getData().get("bremain") + "");
                                    note.setPid(doc.getData().get("pid") + "");
//                                notesList.add(note);
                                    Log.e("sonaport", notesList.size() + "" + doc.getId());


                                    personsport.add(note);

                            }



                            firestoreListener = firestoreDB.collection("PlayerInfo")
                                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                                        @Override
                                        public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                                            if (e != null) {
                                                Log.e("err", "Listen failed!", e);
                                                return;
                                            }
                                           personfinal = new ArrayList<>();
                                            List<PortfolioBean> notesList = new ArrayList<>();

                                            for (DocumentSnapshot doc : documentSnapshots) {
                                                PortfolioBean note = new PortfolioBean();
//                            PortfolioBean note = doc.toObject(PortfolioBean.class);
                                                note.setId(doc.getId());
//                                                Log.e("pnm", String.valueOf(doc.getData().get("p_n")+""));
                                                note.setPn(String.valueOf(doc.getData().get("p_n")+""));

                                                   for(int i = 0; i < personsport.size(); i++){
                                                PortfolioBean note1 = new PortfolioBean();
//                            PortfolioBean note = doc.toObject(PortfolioBean.class);
                                                       if(personsport.get(i).id.equals(doc.getId())) {
                                                           String strshr = personsport.get(i).shr+"";
                                                           Log.e("sona", strshr+" s");
                                                           if(!strshr.equals("0")) {
                                                               note1.setId(personsport.get(i).id);
                                                               double quanshr = 0, curprice = 0, ttlcpri = 0, pastbpri = 0.0, pastttlbpri = 0.0, difbpri = 0.0, difttlbpri = 0.0;

                                                               try {

                                                                   quanshr = Double.parseDouble(personsport.get(i).shr);
                                                                   String strcurprice = (doc.getData().get("price")) + "";
                                                                   curprice = Double.parseDouble(strcurprice);
                                                                   ttlcpri = quanshr * curprice;
                                                                   pastbpri = Double.parseDouble(personsport.get(i).buyp);
//                                                               String strpastttlbpri = personsport.get(i).ttlbp;
//                                                               String strpastttlbpri = personsport.get(i).ttlbp;
//                                                               strpastttlbpri = strpastttlbpri.replace(getResources().getString(R.string.euro)+"","");
                                                                   pastttlbpri = pastbpri * quanshr;
                                                                   difbpri = curprice - pastbpri;
                                                                   difttlbpri = ttlcpri - pastttlbpri;


                                                                   Log.e("pnm", String.valueOf(doc.getData().get("p_n") + "" + " " + ttlcpri));

                                                               } catch (Exception ex) {
                                                               }
                                                               note1.setPn(String.valueOf(doc.getData().get("p_n") + ""));
                                                               note1.setT_n(String.valueOf(doc.getData().get("t_n") + ""));
                                                               note1.setCntry(String.valueOf(doc.getData().get("cntry") + ""));
                                                               note1.setPtion(String.valueOf(doc.getData().get("ption") + ""));
                                                               note1.setP_side(String.valueOf(doc.getData().get("p_side") + ""));
                                                               note1.setB_date(String.valueOf(doc.getData().get("b_date") + ""));
                                                               note1.setWht(String.valueOf(doc.getData().get("wht") + ""));
                                                               note1.setHigt(String.valueOf(doc.getData().get("higt") + ""));
                                                               note1.setShr(String.valueOf(doc.getData().get("srtno") + ""));
                                                               note1.setB_plce(String.valueOf(doc.getData().get("b_plce") + ""));

                                                               note1.setCurbprce(new DecimalFormat(".##").format(curprice) + "");
                                                               note1.setTtlCbprce(new DecimalFormat(".##").format(ttlcpri) + "");
                                                               note1.setSellp(String.valueOf(doc.getData().get("spri") + ""));
                                                               note1.setPimg(personsport.get(i).pimg);
                                                               note1.setShr(personsport.get(i).shr);
                                                               note1.setBuyp(personsport.get(i).buyp);
                                                               note1.setTtlbp(getResources().getString(R.string.euro) + "" + pastttlbpri);
                                                               note1.setBremain(personsport.get(i).bremain);
                                                               note1.setPid(personsport.get(i).pid);
                                                               note1.setDiffbprce(new DecimalFormat(".##").format(difbpri) + "");
                                                               note1.setDiffttlbpri(new DecimalFormat(".##").format(difttlbpri) + "");
                                                               notesList.add(note1);
                                                               personfinal.add(note1);
                                                           }
                                                       }
                                            }
                                                notesList.add(note);

//                                                persons.add(note);


                                            }


                                            RVAdapterPortpolio adapter = new RVAdapterPortpolio(user_id, firestoreDB, amt,"Home",getActivity(), personfinal, new CustomItemClickListenerHomepage() {
                                                @Override
                                                public void onItemClick(View v, int position) {
//                Toast.makeText(RecyclerViewActivity.this, "Clicked Item: "+position,Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                            rv.setAdapter(adapter);

//                        mAdapter = new NoteRecyclerViewAdapter(notesList, getApplicationContext(), firestoreDB);
//                        recyclerView.setAdapter(mAdapter);
                                        }
                                    });



                        } else {
                            Log.d("ERR", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

}
