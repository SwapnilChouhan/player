package com.theoptimumlabs.playermarket.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.theoptimumlabs.playermarket.HomeActivity;
import com.theoptimumlabs.playermarket.Model.SellQueueBean;
import com.theoptimumlabs.playermarket.Model.SellQueueBean;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.utilsadded.CustomItemClickListenerHomepage;
import com.theoptimumlabs.playermarket.utilsadded.Utility;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RVAdapterPortpolioSellQue extends RecyclerView.Adapter<RVAdapterPortpolioSellQue.PersonViewHolder> {
    FirebaseFirestore firestoreDB;
    String user_id;
    CustomItemClickListenerHomepage listener;
    private Dialog m_dialog; //Dialog instance.
    private LinearLayout m_llMain, lleditbtn;
    Context context;
    String from, amt;
    int minteger = 1;
    int incrementchk;
    double ttlbuyamt = 0.0;
    int quantityShr = 1 ;
    double walletremain ;
    double walletAvail ;
    int a = 0;
    List<SellQueueBean> rowItems;

    int availshrQuan = 0;


    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView txtPlayernm, txtmore, txtfuture, txtdividence, txtoneshramt, txttotalamt, txtoneshramtP, txttotalamtP, txtoneshramtdiff, txttlshramtdiff;
        LinearLayout lincancel,linSell;
        TextView personAge;
        ImageView imgplayer;
        ListView listView;


        PersonViewHolder(View itemView) {
            super(itemView);
//            cv = (CardView)itemView.findViewById(R.id.cv);
            txtmore = (TextView)itemView.findViewById(R.id.txtmore);
            txtPlayernm = (TextView)itemView.findViewById(R.id.txtPlayernm);
            txtfuture = (TextView)itemView.findViewById(R.id.txtfuture);
           txtdividence = (TextView)itemView.findViewById(R.id.txtdividence);
            txtoneshramt = (TextView)itemView.findViewById(R.id.txtoneshramt);
            txttotalamt = (TextView)itemView.findViewById(R.id.txttotalamt);
            txtoneshramtP = (TextView)itemView.findViewById(R.id.txtoneshramtP);
            txttotalamtP = (TextView)itemView.findViewById(R.id.txttotalamtP);
            txtoneshramtdiff = (TextView)itemView.findViewById(R.id.txtoneshramtdiff);
            txttlshramtdiff = (TextView)itemView.findViewById(R.id.txttlshramtdiff);
            lincancel = (LinearLayout) itemView.findViewById(R.id.lincancel);
            linSell = (LinearLayout) itemView.findViewById(R.id.linSell);
            imgplayer = (ImageView)itemView.findViewById(R.id.imgplayer);
            listView = (ListView) itemView .findViewById(R.id.list);

        }
    }

    List<SellQueueBean> persons;

    public RVAdapterPortpolioSellQue(String user_id, FirebaseFirestore firestoreDB, String amt, String from, Context context, List<SellQueueBean> persons, CustomItemClickListenerHomepage listener){
        this.persons = persons;
        this.listener = listener;
        this.context = context;
        this.from = from;
        this.amt = amt;
        this.user_id = user_id;
        this.firestoreDB = firestoreDB;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.portfolio_sellquerow, viewGroup, false);
        final PersonViewHolder pvh = new PersonViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, pvh.getPosition());
            }
        });
        return pvh;
    }

    @Override
    public void onBindViewHolder(final PersonViewHolder personViewHolder, final int i) {
        personViewHolder.txtPlayernm.setText(persons.get(i).pn);
        final int posi = i+1;
//        personViewHolder.txtshirt.setText(""+posi);
        personViewHolder.txtfuture.setText("Futures: "+persons.get(i).shr);
        Log.e("sonahomadp", persons.get(i).sellp+""+amt);

        try{
            double sprice = Double.parseDouble(persons.get(i).sellp);

//            personViewHolder.txtPriceSell.setText(context.getResources().getString(R.string.euro)+new DecimalFormat(".##").format(sprice));
        }catch (Exception e){
//            personViewHolder.txtPriceSell.setText("0");

        }

        personViewHolder.txtoneshramt.setText(context.getResources().getString(R.string.euro)+persons.get(i).buyp);
        personViewHolder.txttotalamt.setText(""+persons.get(i).ttbp);
        personViewHolder.txtoneshramtP.setText(context.getResources().getString(R.string.euro)+persons.get(i).curbprce);
        personViewHolder.txttotalamtP.setText(context.getResources().getString(R.string.euro)+persons.get(i).ttlCbprce);
       String strdifbpri = persons.get(i).difbprce;
        try {
            if (strdifbpri.contains("-")) {
                strdifbpri = strdifbpri.replace("-", "");
                personViewHolder.txtoneshramtdiff.setTextColor(ContextCompat.getColor(context, R.color.red_500));
                personViewHolder.txtoneshramtdiff.setText("-" + context.getResources().getString(R.string.euro) + strdifbpri);

            } else if (strdifbpri.contains("+")) {
                strdifbpri = strdifbpri.replace("+", "");
                personViewHolder.txtoneshramtdiff.setTextColor(ContextCompat.getColor(context, R.color.green_A700));
                personViewHolder.txtoneshramtdiff.setText("+" + context.getResources().getString(R.string.euro) + strdifbpri);

            } else {
                personViewHolder.txtoneshramtdiff.setTextColor(ContextCompat.getColor(context, R.color.green_A700));
                personViewHolder.txtoneshramtdiff.setText(context.getResources().getString(R.string.euro) + strdifbpri);

            }

            String strttldifbpri = persons.get(i).difttlbpri;
            if (strttldifbpri.contains("-")) {
                strttldifbpri = strttldifbpri.replace("-", "");
                personViewHolder.txttlshramtdiff.setTextColor(ContextCompat.getColor(context, R.color.red_500));
                personViewHolder.txttlshramtdiff.setText("-" + context.getResources().getString(R.string.euro) + strttldifbpri);

            } else if (strttldifbpri.contains("+")) {
                strttldifbpri = strttldifbpri.replace("+", "");
                personViewHolder.txttlshramtdiff.setTextColor(ContextCompat.getColor(context, R.color.green_A700));
                personViewHolder.txttlshramtdiff.setText("+" + context.getResources().getString(R.string.euro) + strttldifbpri);

            } else {
                personViewHolder.txttlshramtdiff.setTextColor(ContextCompat.getColor(context, R.color.green_A700));
                personViewHolder.txttlshramtdiff.setText(context.getResources().getString(R.string.euro) + strttldifbpri);

            }
        }catch (Exception e){}


        String imgString=""+persons.get(i).img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        personViewHolder.imgplayer.setImageBitmap(decodedImage);





        personViewHolder.lincancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletequeue(i);

            }
        });
        personViewHolder.linSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showcustomdialogSELL(i);

            }
        });
        personViewHolder.imgplayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showcustomdialogPlayerInfo(i);

            }
        });
        personViewHolder.txtPlayernm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showcustomdialogPlayerInfo(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }

    public void showcustomdialogPlayerInfo(int position)
    {
        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageView cancel;
        TextView txtok, txtPlayernm, txtteamnm, txtCountry, txtposition, txtpositionside,
                txtweight, txtheight, txtbd, txtbirthplace, heading;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.player_info, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtPlayernm = (TextView) m_view.findViewById(R.id.txtPlayernm);
        txtteamnm = (TextView) m_view.findViewById(R.id.txtteamnm);
        txtCountry = (TextView) m_view.findViewById(R.id.txtCountry);
        txtposition = (TextView) m_view.findViewById(R.id.txtposition);
        txtpositionside = (TextView) m_view.findViewById(R.id.txtpositionside);
        txtweight = (TextView) m_view.findViewById(R.id.txtweight);
        txtheight = (TextView) m_view.findViewById(R.id.txtheight);
        txtbd = (TextView) m_view.findViewById(R.id.txtbd);
        txtbirthplace = (TextView) m_view.findViewById(R.id.txtbirthplace);
        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        SellQueueBean addPlayerBean = persons.get(position);
        txtPlayernm.setText(addPlayerBean.pn);
        txtteamnm.setText(addPlayerBean.tn);
        txtCountry.setText(addPlayerBean.cntry);
        txtposition.setText(addPlayerBean.ption);
        txtpositionside.setText(addPlayerBean.pside);
        txtweight.setText(addPlayerBean.wht);
        txtheight.setText(addPlayerBean.higt);
        txtbd.setText(addPlayerBean.bdate);
        txtbirthplace.setText(addPlayerBean.bplce);
        heading.setText(addPlayerBean.pn);

        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);


        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }


    public void showcustomdialogSELLConfirm(int position, final String pid, final int shravil, final String shrsold, final String inssellval, final String cbuyp, final String sinsellp)
    {
        final int remainshr;
        a = 0;

        minteger = 1;

        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ImageView cancel;

        LinearLayout linBuybuydialogcancel, llconfirm;
        final TextView txtok,  txtshrsold, txtinstsellval, txtblnc, heading, txtquota;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.selldialogconfirm, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtshrsold = (TextView) m_view.findViewById(R.id.txtshrsold);
        txtinstsellval = (TextView) m_view.findViewById(R.id.txtinstsellval);
        txtblnc = (TextView) m_view.findViewById(R.id.txtblnc);
        txtquota = (TextView) m_view.findViewById(R.id.txtquota);
        linBuybuydialogcancel = (LinearLayout) m_view.findViewById(R.id.linBuybuydialogcancel);
        llconfirm = (LinearLayout) m_view.findViewById(R.id.llconfirm);

        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        final SellQueueBean addPlayerBean = persons.get(position);
        String euro = context.getResources().getString(R.string.euro)+"";
        txtshrsold.setText(shrsold);
//        txtblnc.setText(euro+amt);
        txtinstsellval.setText(euro+inssellval);
//        txtremainblnc.setText(context.getResources().getString(R.string.euro)+amt);
//        PriceBuydialog.setText(context.getResources().getString(R.string.euro)+addPlayerBean.price);


        heading.setText(addPlayerBean.pn);

        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);
        remainshr =  shravil - Integer.parseInt(shrsold);

        reverseTimer(30, txtquota);
        try {
            double percent98 = Double.parseDouble(inssellval) * 0.98;
            double newwalletamt = percent98 + Double.parseDouble(amt) ;
            Log.e("updatewall", newwalletamt+"  "+Double.parseDouble(amt)+" "+percent98+" "+inssellval);
            txtblnc.setText(euro+new DecimalFormat(".##").format(newwalletamt)+"");


        }catch (Exception e){
            ShowNormalmsg("Please try again.");
        }
        linBuybuydialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        llconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
                try {
                    double percent98 = Double.parseDouble(inssellval) * 0.98;
                    double newwalletamt = percent98 + Double.parseDouble(amt) ;
                    Log.e("updatewall", newwalletamt+"  "+Double.parseDouble(amt)+" "+percent98+" "+inssellval);
                    addnewselltransac(addPlayerBean.id, remainshr+"", cbuyp, sinsellp,"",addPlayerBean.pn);
                    updateShr(String.valueOf(remainshr),pid);

                    updateWallet(new DecimalFormat(".##").format(newwalletamt)+"", user_id, "Sell.");

                }catch (Exception e){
                    ShowNormalmsg("Please try again.");
                }

//                addportfolio(addPlayerBean.id,addPlayerBean.pn,addPlayerBean.img,quan,buyprice,totle, remainbal,addPlayerBean.id);
//                ShowNormalmsg("You just bought "+quan+" "+heading.getText().toString()+" future.\n You can monitor your Futures by visiting your Portfolio.");
            }
        });
        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }

    public void showcustomdialogSELLQueue(int position, final String pid, int shravil, final String shrsold, final String inssellval, final String totle, final String remainbal)
    {
        final int remainshr;
        a = 0;
        final String buyprice, sellprice;

        minteger = 1;

        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ImageView cancel;

        LinearLayout linBuybuydialogcancel, llconfirm, linreserve;
        final TextView txtok,  txtshr, txtbuypr, txtblnc, heading;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.selldialog_queue, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtshr = (TextView) m_view.findViewById(R.id.txtshr);
        txtbuypr = (TextView) m_view.findViewById(R.id.txtbuypr);
        txtblnc = (TextView) m_view.findViewById(R.id.txtblnc);
        linBuybuydialogcancel = (LinearLayout) m_view.findViewById(R.id.linBuybuydialogcancel);
        llconfirm = (LinearLayout) m_view.findViewById(R.id.llconfirm);
        linreserve = (LinearLayout) m_view.findViewById(R.id.linreserve);

        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        final SellQueueBean addPlayerBean = persons.get(position);
        String euro = context.getResources().getString(R.string.euro)+"";
        txtshr.setText(shrsold);
        txtblnc.setText(euro+amt);
        txtbuypr.setText(euro+persons.get(position).curbprce);
        buyprice = persons.get(position).curbprce;
        sellprice = persons.get(position).sellp;
        Log.e("inssellval", inssellval+"s");
//        txtremainblnc.setText(context.getResources().getString(R.string.euro)+amt);
//        PriceBuydialog.setText(context.getResources().getString(R.string.euro)+addPlayerBean.price);


        heading.setText(addPlayerBean.pn);

        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);
        remainshr =  shravil - Integer.parseInt(shrsold);

        linBuybuydialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        linreserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        llconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
                try {
                    double percent98 = Double.parseDouble(inssellval) * 0.98;
                    double newwalletamt = percent98 + Double.parseDouble(amt) ;
                    Log.e("updatewall", newwalletamt+"  "+Double.parseDouble(amt)+" "+percent98+" "+inssellval);
                    addportfolioQue(addPlayerBean.id,shrsold,buyprice,sellprice, remainbal,addPlayerBean.id);
                    updateShr(String.valueOf(remainshr),pid);
                    updateWallet(new DecimalFormat(".##").format(newwalletamt)+"", user_id,"Sell");

                }catch (Exception e){
                    ShowNormalmsg("Please try again.");
                }

//                ShowNormalmsg("You just bought "+quan+" "+heading.getText().toString()+" future.\n You can monitor your Futures by visiting your Portfolio.");
            }
        });
        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }


    public void showcustomdialogSELL(final int position)
    {   final String id;
        minteger = 1;
        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageView cancel, minus, plus;
        final EditText quantity_counter;
        LinearLayout linJoinSQue, lininst;
        final TextView txtok, txtcurrbuypri, txtcurrsellpri, txtshrAvail, PriceBuydialog, instsellpri,
                txtweight, txtheight, txtbd, txtbirthplace, heading;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.selldialog, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtcurrbuypri = (TextView) m_view.findViewById(R.id.txtcurrbuypri);
        txtcurrsellpri = (TextView) m_view.findViewById(R.id.txtcurrsellpri);
        txtshrAvail = (TextView) m_view.findViewById(R.id.txtshrAvail);
        instsellpri = (TextView) m_view.findViewById(R.id.instsellpri);
        linJoinSQue = (LinearLayout) m_view.findViewById(R.id.linJoinSQue);
        lininst = (LinearLayout) m_view.findViewById(R.id.lininst);
        quantity_counter = (EditText) m_view.findViewById(R.id.quantity_counter);
        minus = (ImageView) m_view.findViewById(R.id.minus);
        plus = (ImageView) m_view.findViewById(R.id.plus);
        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        final SellQueueBean addPlayerBean = persons.get(position);
        txtcurrbuypri.setText(context.getResources().getString(R.string.euro)+addPlayerBean.curbprce);
        txtcurrsellpri.setText(context.getResources().getString(R.string.euro)+addPlayerBean.sellp);
        instsellpri.setText(context.getResources().getString(R.string.euro)+addPlayerBean.sellp);
        txtshrAvail.setText(""+addPlayerBean.cshr);

        heading.setText(addPlayerBean.pn);

        id = addPlayerBean.id;
        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);
        try {
            availshrQuan = Integer.parseInt(addPlayerBean.shr);
        }catch (Exception e){}

        quantity_counter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (availshrQuan >= Integer.parseInt(quantity_counter.getText().toString())) {
//                    quantity_counter.setText("" + minteger);
                        double currentsellPri = Double.parseDouble(addPlayerBean.sellp);
                        int quantityShr = Integer.parseInt(quantity_counter.getText().toString());
                        double Totleinstsellpri = currentsellPri * quantityShr;
                        instsellpri.setText(context.getResources().getString(R.string.euro) + new DecimalFormat(".##").format(Totleinstsellpri) + "");

                    } else {
                        ShowNormalmsg("You have " + addPlayerBean.shr + " shares for selling.");
                        instsellpri.setText( "");

                    }
                }catch (Exception e){
                    ShowNormalmsg("You have " + addPlayerBean.shr + " shares for selling.");
                    instsellpri.setText( "");

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    minteger = minteger + 1;
                    if (minteger > 0) {
                        if (availshrQuan >= minteger) {
                            quantity_counter.setText("" + minteger);
                            double currentsellPri = Double.parseDouble(addPlayerBean.sellp);
                            int quantityShr = Integer.parseInt(quantity_counter.getText().toString());
                            double Totleinstsellpri = currentsellPri * quantityShr;
                            instsellpri.setText(context.getResources().getString(R.string.euro)+new DecimalFormat(".##").format(Totleinstsellpri) + "");

                        } else {
                            ShowNormalmsg("You dont have shares for selling.");
                            instsellpri.setText( "");

                        }
                    } else {
                        minteger = 1;
                    }
                }catch (Exception e){}
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (minteger > 1) {
                        minteger = minteger - 1;
                        if (minteger > 0) {
                            if (availshrQuan >= minteger) {
                                quantity_counter.setText("" + minteger);
                                double currentsellPri = Double.parseDouble(addPlayerBean.sellp);
                                int quantityShr = Integer.parseInt(quantity_counter.getText().toString());
                                double Totleinstsellpri = currentsellPri * quantityShr;
                                instsellpri.setText(context.getResources().getString(R.string.euro)+new DecimalFormat(".##").format(Totleinstsellpri) + "");

                            } else {
                                ShowNormalmsg("You have " + addPlayerBean.shr + "shares for selling.");
                                instsellpri.setText( "");

                            }
                        } else {
                            minteger = 1;
                        }

//                    Total_amount();

                    }
                }catch (Exception e){}
            }
        });

        lininst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

                String totolsell = instsellpri.getText().toString();
                totolsell = totolsell.replace(context.getResources().getString(R.string.euro)+"", "");
                if(totolsell.equals("") || totolsell.equals(null) || totolsell.equals("null")) {
                    ShowNormalmsg("Invalid Amount.");
                }else {
                    showcustomdialogSELLConfirm(position, id, availshrQuan, quantity_counter.getText().toString(), totolsell, addPlayerBean.curbprce, addPlayerBean.sellp);
                }
            }
        });

        linJoinSQue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

                String totolsell = instsellpri.getText().toString();
                totolsell = totolsell.replace(context.getResources().getString(R.string.euro)+"", "");
                if(totolsell.equals("") || totolsell.equals(null) || totolsell.equals("null")) {
                    ShowNormalmsg("Invalid Amount.");
                }else {
                    showcustomdialogSELLQueue(position, id, availshrQuan, quantity_counter.getText().toString(), totolsell, "", amt);

                }
            }
        });


        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }

    private void updateShr(final String shr, String pid) {

//        Map<String, Object> note = new Note(title, content).toMap();
        Map<String, Object> data1 = new HashMap<>();
        data1.put("shr", shr);
//        data1.put("amt", amt);
        firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio").document(pid)
                .update(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sonaliwa", "Error adding Note document", e);

                    }
                });
    }
    private void updateWallet(String newamt, String uid, final String frmbuysell) {
        String euro = context.getResources().getString(R.string.euro)+"";

        newamt = newamt.replace(euro+"", "");
//        Map<String, Object> note = new Note(title, content).toMap();
        Map<String, Object> data1 = new HashMap<>();
        data1.put("amt", newamt);
//        data1.put("amt", amt);
        firestoreDB.collection("UserInfo").document(user_id).collection("Wallet").document("W"+user_id)
                .update(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if(frmbuysell.equals("Buy")){
                    ShowNormalmsg("Successfully Buy shares.");

                }else {
                    ShowNormalmsg("Successfully sell this shares.");
                }
            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sonaliwa", "Error adding Note document", e);

                    }
                });
    }

    public void ShowNormalmsg(final String msg){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.getContext().setTheme(R.style.AlertThem);

        builder.setMessage(msg)
                .setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if(msg.equals("Added in Portfolio.")){
                    intentcall(context, HomeActivity.class);
                }else if(msg.equals("Successfully sell this shares.")){
                    intentcall(context, HomeActivity.class);
//
                }
            }
        });

        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();

    }


    private void addnewselltransac(String id,  String shr, String buyp, String sellp, String bremain, String pn) {


//        Map<String, Object> note = new Note(title, content).toMap();
        Map<String, Object> data1 = new HashMap<>();
//        data1.put("Id", randomId()+"");
//        data1.put("amt", amt);
        data1.put("buyp", buyp);
        data1.put("sellp", sellp);
        data1.put("shr", shr);
        data1.put("tranctTime", timforsave());
        data1.put("pn", pn);
//        data1.put("Id", id);
        firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio").document(id).collection("Sellorder").document(timestamp()+"")
                .set(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
//                ShowNormalmsg("Added in Portfolio.");
            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sonaliwa", "Error adding buyagain document", e);
//                        Toast.makeText(context, "Note could not be added!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void addportfolioQue(final String id,  final String shr, final String buyp, final String sellp, String bremain, String pid) {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String CurrentDate = df.format(c);
//        SellQueueBean portfolioBean = new SellQueueBean(id, pn,img, shr, buyp, ttlbp, bremain, pid);

//        Map<String, Object> note = new Note(title, content).toMap();
        String queid = timestamp();
        Map<String, Object> data1 = new HashMap<>();
        data1.put("Id", queid);
        data1.put("uid",user_id);
        data1.put("pid",pid);
        data1.put("sts", "0");
        data1.put("buyp", buyp);
        data1.put("sellp", sellp);
        data1.put("shr", shr);
        data1.put("tranctdate", CurrentDate);
//        data1.put("amt", amt);
        firestoreDB.collection("SellQueue").document(queid)
                .set(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
//                addInListbuyprice(id,shr,buyp,ttlbp, "","");

                ShowNormalmsg("Added in Queue.");
            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sonaliwa", "Error adding Note document", e);
                        Toast.makeText(context, "Note could not be added!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void intentcall(Context from, Class to){
        Intent i = new Intent(context, to);
        context.startActivity(i);
    }

    public void reverseTimer(int Seconds,final TextView tv){

        new CountDownTimer(Seconds* 1000+1000, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                tv.setText(String.format("%02d", seconds)+" sec");
//                tv.setText("TIME : " + String.format("%02d", minutes)
//                        + ":" + String.format("%02d", seconds));
            }

            public void onFinish() {
                tv.setText("Completed");
                m_dialog.dismiss();
            }
        }.start();
    }


    public String timestamp(){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        return ts;
    }

    public String timforsave(){
        Date date = new Date();
//        long time = date.getTime();
        Long time = System.currentTimeMillis();

        System.out.println("time in miliseconds  "+time);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        String datestring = formatter.format(new Date(time));
        Log.e("ttime", datestring+" d");
        return time.toString();
    }
    private void deletequeue(final int position) {
        String queid = persons.get(position).getId();
        Log.e("sonaqdel","d"+queid);
        firestoreDB.collection("SellQueue")
                .document(queid)
                .delete()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        persons.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, persons.size());
                        intentcall(context, HomeActivity.class);
//                        Toast.makeText(context, "Note has been deleted!", Toast.LENGTH_SHORT).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });

    }

}
