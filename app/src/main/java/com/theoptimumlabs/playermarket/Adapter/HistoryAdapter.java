package com.theoptimumlabs.playermarket.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.theoptimumlabs.playermarket.Model.HistoryBean;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.utilsadded.CustomItemClickListenerHomepage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.PersonViewHolder> {

    CustomItemClickListenerHomepage listener;
    Context context;
    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView txtdate, txtflag, txtPlayernm, txtfuture, txtbuyp, txttotal, txtselp;
        LinearLayout llsell, llbuy;
        ImageView personPhoto;

        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card_view);
            txtdate = (TextView)itemView.findViewById(R.id.txtdate);
            txtPlayernm = (TextView)itemView.findViewById(R.id.txtPlayernm);
            txtfuture = (TextView)itemView.findViewById(R.id.txtfuture);
            txtbuyp = (TextView)itemView.findViewById(R.id.txtbuyp);
            txtselp = (TextView)itemView.findViewById(R.id.txtselp);
            txttotal = (TextView)itemView.findViewById(R.id.txttotal);
            txtflag = (TextView)itemView.findViewById(R.id.txtflag);
            llsell = (LinearLayout) itemView.findViewById(R.id.llsell);
            llbuy = (LinearLayout) itemView.findViewById(R.id.llbuy);
//            personPhoto = (ImageView)itemView.findViewById(R.id.person_photo);
        }
    }

    List<HistoryBean> persons;

    public HistoryAdapter(Context context, List<HistoryBean> persons, CustomItemClickListenerHomepage listener){
        this.persons = persons;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.historyrow, viewGroup, false);
        final PersonViewHolder pvh = new PersonViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, pvh.getPosition());
            }
        });
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.txtPlayernm.setText(persons.get(i).pn);
        personViewHolder.txtfuture.setText(persons.get(i).shr);
        personViewHolder.txtbuyp.setText(persons.get(i).getBuyp());
        personViewHolder.txtselp.setText(persons.get(i).getSellp());
        personViewHolder.txttotal.setText(persons.get(i).getTtl());
        String timest = persons.get(i).getTimestamp();
        try {

            Date date = new Date();
//        long time = date.getTime();
            Long time = Long.valueOf(timest);

            System.out.println("time in miliseconds  " + time);
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
            SimpleDateFormat formatter = new SimpleDateFormat("E, dd/MMM/yyyy hh:mm:ss a");
//            String datestring = formatter.format(calendar.getTime());
            String datestring = formatter.format(new Date(time));
            Log.e("ttime", datestring + " adasaa");
            personViewHolder.txtdate.setText(datestring);
        }catch (Exception e){
            personViewHolder.txtdate.setText("Not Specify.");

        }
        if(persons.get(i).getRole().equals("buy")){
            personViewHolder.llbuy.setVisibility(View.VISIBLE);
            personViewHolder.llsell.setVisibility(View.GONE);
            personViewHolder.txtflag.setText("Buy");
            personViewHolder.txtflag.setTextColor(context.getResources().getColor(R.color.amber_500));


        }else {
            personViewHolder.llbuy.setVisibility(View.GONE);
            personViewHolder.llsell.setVisibility(View.VISIBLE);
            personViewHolder.txtflag.setText("Sell");
            personViewHolder.txtflag.setTextColor(context.getResources().getColor(R.color.green_A700));

        }
//        personViewHolder.personPhoto.setImageResource(persons.get(i).photoId);
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }
}
