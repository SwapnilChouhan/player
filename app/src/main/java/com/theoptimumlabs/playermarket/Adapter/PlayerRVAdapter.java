package com.theoptimumlabs.playermarket.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.theoptimumlabs.playermarket.Model.AddPlayerBean;
import com.theoptimumlabs.playermarket.Model.PortfolioBean;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.utilsadded.CustomItemClickListenerHomepage;
import com.theoptimumlabs.playermarket.utilsadded.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlayerRVAdapter extends RecyclerView.Adapter<PlayerRVAdapter.PersonViewHolder> {

    CustomItemClickListenerHomepage listener;
    private Dialog m_dialog; //Dialog instance.
    private LinearLayout m_llMain, lleditbtn;
    Context context;
    String from;
    private FirebaseFirestore firestoreDB;



    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView txtPlayernm, txtshirt, txtCountry, txtincrdecr;
        LinearLayout linBuy,linSell,linaddprice;
        TextView personAge;
        ImageView imgplayer;

        PersonViewHolder(View itemView) {
            super(itemView);
//            cv = (CardView)itemView.findViewById(R.id.cv);
            txtPlayernm = (TextView)itemView.findViewById(R.id.txtPlayernm);
            txtshirt = (TextView)itemView.findViewById(R.id.txtshirt);
            txtCountry = (TextView)itemView.findViewById(R.id.txtCountry);
            txtincrdecr = (TextView)itemView.findViewById(R.id.txtincrdecr);
            linaddprice = (LinearLayout) itemView.findViewById(R.id.linaddprice);
            linBuy = (LinearLayout) itemView.findViewById(R.id.linBuy);
            linSell = (LinearLayout) itemView.findViewById(R.id.linSell);
            imgplayer = (ImageView)itemView.findViewById(R.id.imgplayer);

        }
    }

    List<AddPlayerBean> persons;

    public PlayerRVAdapter(FirebaseFirestore firestoreDB, String from, Context context, List<AddPlayerBean> persons, CustomItemClickListenerHomepage listener){
        this.persons = persons;
        this.listener = listener;
        this.context = context;
        this.from = from;
        this.firestoreDB = firestoreDB;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.market_row, viewGroup, false);
        final PersonViewHolder pvh = new PersonViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, pvh.getPosition());
            }
        });
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, final int i) {
        personViewHolder.txtPlayernm.setText(persons.get(i).p_n);
        final int posi = i+1;
        personViewHolder.txtshirt.setText(""+posi);
        personViewHolder.txtCountry.setText(persons.get(i).t_n);
//        personViewHolder.txtincrdecr.setText(persons.get(i).shirtno);

//        personViewHolder.imgplayer.setImageResource(persons.get(i).photoId);

        String imgString=""+persons.get(i).img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        personViewHolder.imgplayer.setImageBitmap(decodedImage);

        if(from.equals("player")){
            personViewHolder.linaddprice.setVisibility(View.VISIBLE);
            personViewHolder.linBuy.setVisibility(View.GONE);
            personViewHolder.linSell.setVisibility(View.GONE);
            personViewHolder.linaddprice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Addplayerprice(i);
                    }catch (Exception e){}

                }
            });
        }else {
            personViewHolder.linaddprice.setVisibility(View.GONE);
            personViewHolder.linBuy.setVisibility(View.VISIBLE);
            personViewHolder.linSell.setVisibility(View.VISIBLE);

            personViewHolder.txtPlayernm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showcustomdialogPlayerInfo(i);

                }
            });
        }
        personViewHolder.imgplayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    showcustomdialogPlayerInfo(i);
                }catch (Exception e){}

            }
        });

    }

    @Override
    public int getItemCount() {
        return persons.size();
    }


    public void showcustomdialogPlayerInfo(int position)
    {
        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageView cancel;
        TextView txtok, txtPlayernm, txtteamnm, txtCountry, txtposition, txtpositionside,
                txtweight, txtheight, txtbd, txtbirthplace, heading;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.player_info, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtPlayernm = (TextView) m_view.findViewById(R.id.txtPlayernm);
        txtteamnm = (TextView) m_view.findViewById(R.id.txtteamnm);
        txtCountry = (TextView) m_view.findViewById(R.id.txtCountry);
        txtposition = (TextView) m_view.findViewById(R.id.txtposition);
        txtpositionside = (TextView) m_view.findViewById(R.id.txtpositionside);
        txtweight = (TextView) m_view.findViewById(R.id.txtweight);
        txtheight = (TextView) m_view.findViewById(R.id.txtheight);
        txtbd = (TextView) m_view.findViewById(R.id.txtbd);
        txtbirthplace = (TextView) m_view.findViewById(R.id.txtbirthplace);
        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        AddPlayerBean addPlayerBean = persons.get(position);
        txtPlayernm.setText(addPlayerBean.p_n);
        txtteamnm.setText(addPlayerBean.t_n);
        txtCountry.setText(addPlayerBean.cntry);
        txtposition.setText(addPlayerBean.ption);
        txtpositionside.setText(addPlayerBean.p_side);
        txtweight.setText(addPlayerBean.wht);
        txtheight.setText(addPlayerBean.higt);
        txtbd.setText(addPlayerBean.b_date);
        txtbirthplace.setText(addPlayerBean.b_plce);
        heading.setText(addPlayerBean.p_n);

        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);


        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }

    public void Addplayerprice(int position)
    {
        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageView cancel;
        TextView txtok, txtPlayernm, txtteamnm, heading;
        final EditText edit_price, edit_sellprice;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.addpricecustom, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtPlayernm = (TextView) m_view.findViewById(R.id.txtPlayernm);
        txtteamnm = (TextView) m_view.findViewById(R.id.txtteamnm);
        edit_price = (EditText)m_view.findViewById(R.id.edit_price);
        edit_sellprice = (EditText)m_view.findViewById(R.id.edit_sellprice);

        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        final AddPlayerBean addPlayerBean = persons.get(position);
        txtPlayernm.setText(addPlayerBean.p_n);
        txtteamnm.setText(addPlayerBean.t_n);

        heading.setText(addPlayerBean.p_n);

        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);


        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
                if(!edit_price.getText().toString().equals("") && !edit_sellprice.getText().toString().equals("")) {
                    updateprice(addPlayerBean.id, edit_price.getText().toString(), edit_sellprice.getText().toString());
                }else {
                    Toast.makeText(context, "Please enter price", Toast.LENGTH_SHORT).show();
                }

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }

    private void updateprice(String id, String strprice, String sellpri) {
//        Map<String, Object> note = (new Note(id, title, content)).toMap();
        Map<String, Object> newmap = new HashMap<>();
        newmap.put("price",strprice);
        newmap.put("spri",sellpri);
        firestoreDB.collection("PlayerInfo")
                .document(id)
                .update(newmap)
//        firestoreDB.collection("Prices")
//                .document(id)
//                .set(Price)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e("playeradp", "Price document update successful!");
                        Toast.makeText(context, "Player has been updated!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("playeradp", "Error adding Price document", e);
                        Toast.makeText(context, "Price could not be updated!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}
