package com.theoptimumlabs.playermarket.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.theoptimumlabs.playermarket.Model.HistoryBean;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.utilsadded.CustomItemClickListenerHomepage;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class HistoryWalletAdapter extends RecyclerView.Adapter<HistoryWalletAdapter.PersonViewHolder> {

    CustomItemClickListenerHomepage listener;
    Context context;
    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView txtdate, txtamt;

        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card_view);
            txtdate = (TextView)itemView.findViewById(R.id.txtdate);
            txtamt = (TextView)itemView.findViewById(R.id.txtamt);

//            personPhoto = (ImageView)itemView.findViewById(R.id.person_photo);
        }
    }

    List<HistoryBean> persons;

    public HistoryWalletAdapter(Context context, List<HistoryBean> persons, CustomItemClickListenerHomepage listener){
        this.persons = persons;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.historyrowwallet, viewGroup, false);
        final PersonViewHolder pvh = new PersonViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, pvh.getPosition());
            }
        });
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.txtamt.setText(persons.get(i).getSellp());
        String timest = persons.get(i).getTimestamp();
        try {

            Date date = new Date();
//        long time = date.getTime();
            Long time = Long.valueOf(timest);

            System.out.println("time in miliseconds  " + time);
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
            SimpleDateFormat formatter = new SimpleDateFormat("E, dd/MMM/yyyy hh:mm:ss a");
//            String datestring = formatter.format(calendar.getTime());
            String datestring = formatter.format(new Date(time));
            Log.e("ttime", datestring + " adasaa");
            personViewHolder.txtdate.setText(datestring);
        }catch (Exception e){
            personViewHolder.txtdate.setText("Not Specify.");

        }
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }
}
