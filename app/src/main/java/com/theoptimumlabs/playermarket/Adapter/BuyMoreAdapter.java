package com.theoptimumlabs.playermarket.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.theoptimumlabs.playermarket.Model.PortfolioBean;
import com.theoptimumlabs.playermarket.R;

import java.util.List;


public class BuyMoreAdapter extends BaseAdapter {
    Context context;
    List<PortfolioBean> rowItems;
    String frmwhr;

    public BuyMoreAdapter(Context context, List<PortfolioBean> items, String frmwhr) {
        this.context = context;
        this.rowItems = items;
        this.frmwhr = frmwhr;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView, imgeye;
        TextView txtfuture, txtoneshramt;
        TextView txttotalamt, txtoneshramtP, txttotalamtP, txtoneshramtdiff, txttlshramtdiff;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.buymore_row, null);
            holder = new ViewHolder();
            holder.txtfuture = (TextView) convertView.findViewById(R.id.txtfuture);
            holder.txtoneshramt = (TextView) convertView.findViewById(R.id.txtoneshramt);
            holder.txttotalamt = (TextView) convertView.findViewById(R.id.txttotalamt);
            holder.txtoneshramtP = (TextView) convertView.findViewById(R.id.txtoneshramtP);
            holder.txttotalamtP = (TextView) convertView.findViewById(R.id.txttotalamtP);
            holder.txtoneshramtdiff = (TextView) convertView.findViewById(R.id.txtoneshramtdiff);
            holder.txttlshramtdiff = (TextView) convertView.findViewById(R.id.txttlshramtdiff);
//            holder.imgeye = (ImageView) convertView.findViewById(R.id.imgeye);
//            holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        PortfolioBean rowItem = (PortfolioBean) getItem(position);

//        if(frmwhr.equals("maincatnm")){
//            holder.imgeye.setVisibility(View.GONE);
//
//        }else if(frmwhr.equals("maincatnmModelPri")){
//            holder.imgeye.setVisibility(View.VISIBLE);
//        }

        holder.txtfuture.setText(rowItem.shr);
        holder.txtoneshramt.setText(context.getResources().getString(R.string.euro)+rowItem.buyp);
        holder.txttotalamt.setText(rowItem.ttlbp);
        holder.txtoneshramtP.setText(context.getResources().getString(R.string.euro)+rowItem.curbprce);
        holder.txttotalamtP.setText(context.getResources().getString(R.string.euro)+rowItem.ttlCbprce);
        String strdifbpri = rowItem.diffbprce;

        if ( strdifbpri.contains("-")) {
            strdifbpri = strdifbpri.replace("-","");
            holder.txtoneshramtdiff.setTextColor(ContextCompat.getColor(context,R.color.red_500));
            holder.txtoneshramtdiff.setText("-"+context.getResources().getString(R.string.euro) + strdifbpri);

        } else if ( strdifbpri.contains("+")) {
            strdifbpri = strdifbpri.replace("+","");
            holder.txtoneshramtdiff.setTextColor(ContextCompat.getColor(context,R.color.green_A700));
            holder.txtoneshramtdiff.setText("+"+context.getResources().getString(R.string.euro) + strdifbpri);

        } else {
            holder.txtoneshramtdiff.setTextColor(ContextCompat.getColor(context,R.color.green_A700));
            holder.txtoneshramtdiff.setText(context.getResources().getString(R.string.euro) + strdifbpri);

        }

        String strttldifbpri = rowItem.diffttlbpri;
        if ( strttldifbpri.contains("-")) {
            strttldifbpri = strttldifbpri.replace("-","");
            holder.txttlshramtdiff.setTextColor(ContextCompat.getColor(context,R.color.red_500));
            holder.txttlshramtdiff.setText("-"+context.getResources().getString(R.string.euro) + strttldifbpri);

        } else if ( strttldifbpri.contains("+")) {
            strttldifbpri = strttldifbpri.replace("+","");
            holder.txttlshramtdiff.setTextColor(ContextCompat.getColor(context,R.color.green_A700));
            holder.txttlshramtdiff.setText("+"+context.getResources().getString(R.string.euro) + strttldifbpri);

        } else {
            holder.txttlshramtdiff.setTextColor(ContextCompat.getColor(context,R.color.green_A700));
            holder.txttlshramtdiff.setText(context.getResources().getString(R.string.euro) + strttldifbpri);

        }


        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}