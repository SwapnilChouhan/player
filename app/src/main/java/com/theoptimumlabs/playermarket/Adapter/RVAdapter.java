package com.theoptimumlabs.playermarket.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.theoptimumlabs.playermarket.Fragment.HomeFragment;
import com.theoptimumlabs.playermarket.HomeActivity;
import com.theoptimumlabs.playermarket.Model.AddPlayerBean;
import com.theoptimumlabs.playermarket.Model.PortfolioBean;
import com.theoptimumlabs.playermarket.Model.SellQueueBean;
import com.theoptimumlabs.playermarket.R;
import com.theoptimumlabs.playermarket.utilsadded.CustomItemClickListenerHomepage;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.google.android.gms.internal.zzahn.runOnUiThread;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.PersonViewHolder> {
    FirebaseFirestore firestoreDB;
    String user_id;
    CustomItemClickListenerHomepage listener;
    private Dialog m_dialog; //Dialog instance.
    private LinearLayout m_llMain, lleditbtn;
    Context context;
    String from, amt;
    int availshrQuan = 0;
    int minteger = 1;
    int incrementchk;
    double ttlbuyamt = 0.0;
    int quantityShr = 1 ;
    double walletremain ;
    double walletAvail ;
    int a = 0;
    int finalshr = 0;

    public String baseUrlfunction = "https://us-central1-hercules-football-market.cloudfunctions.net";
    private String FIREBASE_CLOUD_FUNCTION_REG_URL = baseUrlfunction + "/walletUpdate";


    private ArrayList<AddPlayerBean> arraylist;


    public static class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView txtPlayernm, txtshirt, txtCountry, txtincrdecr, txtPriceBuy, txtPriceSell;
        LinearLayout linBuy,linSell,linaddprice;
        TextView personAge;
        ImageView imgplayer;

        PersonViewHolder(View itemView) {
            super(itemView);
//            cv = (CardView)itemView.findViewById(R.id.cv);
            txtPlayernm = (TextView)itemView.findViewById(R.id.txtPlayernm);
            txtshirt = (TextView)itemView.findViewById(R.id.txtshirt);
            txtPriceBuy = (TextView)itemView.findViewById(R.id.txtPriceBuy);
            txtPriceSell = (TextView)itemView.findViewById(R.id.txtPriceSell);
            txtCountry = (TextView)itemView.findViewById(R.id.txtCountry);
            txtincrdecr = (TextView)itemView.findViewById(R.id.txtincrdecr);
            linaddprice = (LinearLayout) itemView.findViewById(R.id.linaddprice);
            linBuy = (LinearLayout) itemView.findViewById(R.id.linBuy);
            linSell = (LinearLayout) itemView.findViewById(R.id.linSell);
            imgplayer = (ImageView)itemView.findViewById(R.id.imgplayer);
        }
    }

    List<AddPlayerBean> persons;
    List<SellQueueBean> queList;

    public RVAdapter(List<SellQueueBean> queList, String user_id, FirebaseFirestore firestoreDB, String amt, String from, Context context, List<AddPlayerBean> persons, CustomItemClickListenerHomepage listener){
        this.queList = queList;
        this.persons = persons;
        this.listener = listener;
        this.context = context;
        this.from = from;
        this.amt = amt;
        this.user_id = user_id;
        this.firestoreDB = firestoreDB;

        this.arraylist = new ArrayList<AddPlayerBean>();
        this.arraylist.addAll(HomeFragment.movieNamesArrayList);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.market_row, viewGroup, false);
        final PersonViewHolder pvh = new PersonViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, pvh.getPosition());
            }
        });
        return pvh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, final int i) {
        personViewHolder.txtPlayernm.setText(persons.get(i).p_n);
        final int posi = i+1;
        personViewHolder.txtshirt.setText(""+posi);
        personViewHolder.txtCountry.setText(persons.get(i).t_n);
        Log.e("sonahomadp", persons.get(i).spri+""+amt);
        try{
            double sprice = Double.parseDouble(persons.get(i).spri);

            personViewHolder.txtPriceSell.setText(context.getResources().getString(R.string.euro)+new DecimalFormat(".##").format(sprice));
        }catch (Exception e){
            personViewHolder.txtPriceSell.setText("0");

        }
        try {
            double bprice = Double.parseDouble(persons.get(i).price);
            personViewHolder.txtPriceBuy.setText(context.getResources().getString(R.string.euro) + new DecimalFormat(".##").format(bprice));
        }catch (Exception e){
            personViewHolder.txtPriceBuy.setText("0");
        }
//        personViewHolder.txtincrdecr.setText(persons.get(i).shirtno);

//        personViewHolder.imgplayer.setImageResource(persons.get(i).photoId);

        String imgString=""+persons.get(i).img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        personViewHolder.imgplayer.setImageBitmap(decodedImage);

        if(from.equals("player")){
            personViewHolder.linaddprice.setVisibility(View.VISIBLE);
            personViewHolder.linBuy.setVisibility(View.GONE);
            personViewHolder.linSell.setVisibility(View.GONE);
        }else {
            personViewHolder.linaddprice.setVisibility(View.GONE);
            personViewHolder.linBuy.setVisibility(View.VISIBLE);
            personViewHolder.linSell.setVisibility(View.VISIBLE);
        }
        personViewHolder.linBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    showcustomdialogBUY(i);
                }catch (Exception e){}

            }
        });
        personViewHolder.linSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showcustomdialogSELL(i);

            }
        });
        personViewHolder.imgplayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showcustomdialogPlayerInfo(i);

            }
        });
        personViewHolder.txtPlayernm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showcustomdialogPlayerInfo(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }


    public void showcustomdialogPlayerInfo(int position)
    {
        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageView cancel;
        TextView txtok, txtPlayernm, txtteamnm, txtCountry, txtposition, txtpositionside,
                txtweight, txtheight, txtbd, txtbirthplace, heading;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.player_info, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtPlayernm = (TextView) m_view.findViewById(R.id.txtPlayernm);
        txtteamnm = (TextView) m_view.findViewById(R.id.txtteamnm);
        txtCountry = (TextView) m_view.findViewById(R.id.txtCountry);
        txtposition = (TextView) m_view.findViewById(R.id.txtposition);
        txtpositionside = (TextView) m_view.findViewById(R.id.txtpositionside);
        txtweight = (TextView) m_view.findViewById(R.id.txtweight);
        txtheight = (TextView) m_view.findViewById(R.id.txtheight);
        txtbd = (TextView) m_view.findViewById(R.id.txtbd);
        txtbirthplace = (TextView) m_view.findViewById(R.id.txtbirthplace);
        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        AddPlayerBean addPlayerBean = persons.get(position);
        txtPlayernm.setText(addPlayerBean.p_n);
        txtteamnm.setText(addPlayerBean.t_n);
        txtCountry.setText(addPlayerBean.cntry);
        txtposition.setText(addPlayerBean.ption);
        txtpositionside.setText(addPlayerBean.p_side);
        txtweight.setText(addPlayerBean.wht);
        txtheight.setText(addPlayerBean.higt);
        txtbd.setText(addPlayerBean.b_date);
        txtbirthplace.setText(addPlayerBean.b_plce);
        heading.setText(addPlayerBean.p_n);

        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);


        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }

    public void showcustomdialogBUY(final int position)
    {
        a = 0;

        minteger = 1;

        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ImageView cancel, minus, plus;

        final EditText quantity_counter;
        LinearLayout linBuybuydialog, linBuyMaxdia;
        final TextView txtok, txtbuyprice, txtavailblnc, txtremainblnc, PriceBuydialog, txtpositionside,
                txtweight, txtheight, txtbd, txtbirthplace, heading;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.buydialog, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtbuyprice = (TextView) m_view.findViewById(R.id.txtbuyprice);
        txtavailblnc = (TextView) m_view.findViewById(R.id.txtavailblnc);
        txtremainblnc = (TextView) m_view.findViewById(R.id.txtremainblnc);
        PriceBuydialog = (TextView) m_view.findViewById(R.id.PriceBuydialog);
        linBuybuydialog = (LinearLayout) m_view.findViewById(R.id.linBuybuydialog);
        linBuyMaxdia = (LinearLayout) m_view.findViewById(R.id.linBuyMaxdia);
        quantity_counter = (EditText) m_view.findViewById(R.id.quantity_counter);
        minus = (ImageView) m_view.findViewById(R.id.minus);
        plus = (ImageView) m_view.findViewById(R.id.plus);
        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        final AddPlayerBean addPlayerBean = persons.get(position);
        txtbuyprice.setText(context.getResources().getString(R.string.euro)+addPlayerBean.price);
        txtavailblnc.setText(""+amt);
//        txtavailblnc.setText(context.getResources().getString(R.string.euro)+amt);
//        txtremainblnc.setText(context.getResources().getString(R.string.euro)+amt);
//        PriceBuydialog.setText(context.getResources().getString(R.string.euro)+addPlayerBean.price);


        heading.setText(addPlayerBean.p_n);

        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);
        final double buyprice = Double.parseDouble(addPlayerBean.price);

        ttlbuyamt = 0.0;
        quantityShr = 1 ;
//        walletremain ;

        try {
            walletAvail = Double.parseDouble(amt);

            quantityShr = Integer.parseInt(quantity_counter.getText().toString());
            ttlbuyamt = buyprice * quantityShr;
            walletremain = walletAvail - ttlbuyamt;
            DecimalFormat dfwremain = new DecimalFormat(".##");
            String strwrmain  =  dfwremain.format(walletremain);

            DecimalFormat dfttlbuy = new DecimalFormat(".##");
            String strttlbuy  =  dfttlbuy.format(ttlbuyamt);
            txtremainblnc.setText(context.getResources().getString(R.string.euro) + strwrmain + "");
            PriceBuydialog.setText(context.getResources().getString(R.string.euro) + strttlbuy + "");
        } catch (Exception e) {
        }
        quantity_counter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.e("sonaed", s+"bef");

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.e("sonaed", s+"onc");
                    if(quantity_counter.getText().toString().trim().equals("")){
                        txtremainblnc.setText(context.getResources().getString(R.string.euro) + amt + "");
                        PriceBuydialog.setText(context.getResources().getString(R.string.euro) + "" + "");

                    }else {

                        try {
                            quantityShr = Integer.parseInt(quantity_counter.getText().toString());
                            ttlbuyamt = buyprice * quantityShr;
                            walletremain = walletAvail - ttlbuyamt;
                            DecimalFormat dfwremain = new DecimalFormat(".##");
                            String strwrmain = dfwremain.format(walletremain);

                            DecimalFormat dfttlbuy = new DecimalFormat(".##");
                            String strttlbuy = dfttlbuy.format(ttlbuyamt);

                            if (walletremain > 0) {
                                incrementchk = minteger;
                                txtremainblnc.setText(context.getResources().getString(R.string.euro) + strwrmain + "");
                                PriceBuydialog.setText(context.getResources().getString(R.string.euro) + strttlbuy + "");
                                Log.e("increchk", "" + incrementchk + " " + minteger);
//                        quantity_counter.setText(incrementchk + "");
                            } else {
                                ShowNormalmsg("You dont have sufficient amount to buy one more share.");
                                quantity_counter.setText("");
                            }
                        } catch (Exception e) {
                        }
                    }
            }

            @Override
            public void afterTextChanged(Editable s) {
               Log.e("sonaed", s+"af");

            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
//            double ttlbuyamt = 0.0;
//            int quantityShr = 1 ;
//            double walletremain = Double.parseDouble(addPlayerBean.price);
//            double walletAvail = Double.parseDouble(addPlayerBean.price);

            @Override
            public void onClick(View view) {
                try {

                    String strremainbal = txtremainblnc.getText().toString();
                    strremainbal = strremainbal.replace(context.getResources().getString(R.string.euro) + "", "");
                    double remainbal = Double.parseDouble(strremainbal);
                    Log.e("sonare", walletremain + " " + remainbal);
                    if (walletremain > 0) {
//                if(remainbal > 0) {
                        minteger = minteger + 1;
                        if (minteger > 0) {
                            quantity_counter.setText("" + minteger);

                        } else {
                            minteger = 1;
                        }
                        try {
                            quantityShr = Integer.parseInt(quantity_counter.getText().toString());
                            ttlbuyamt = buyprice * quantityShr;
                            walletremain = walletAvail - ttlbuyamt;
                            DecimalFormat dfwremain = new DecimalFormat(".##");
                            String strwrmain = dfwremain.format(walletremain);

                            DecimalFormat dfttlbuy = new DecimalFormat(".##");
                            String strttlbuy = dfttlbuy.format(ttlbuyamt);

                            if (walletremain > 0) {
                                incrementchk = minteger;
                                txtremainblnc.setText(context.getResources().getString(R.string.euro) + strwrmain + "");
                                PriceBuydialog.setText(context.getResources().getString(R.string.euro) + strttlbuy + "");
                                Log.e("increchk", "" + incrementchk + " " + minteger);
                                quantity_counter.setText(incrementchk + "");
                            }
                        } catch (Exception e) {
                        }

                    } else {
                        if (a == 0) {
                            minteger = minteger - 1;
                            quantity_counter.setText("" + minteger);
//                        remainbal = -1;
//                        Toast toast = Toast.makeText(context, "You dont have sufficient amount to buy one more share.", Toast.LENGTH_LONG);
//                        toast.setGravity(Gravity.CENTER, 0, 0);
//
//                        toast.show();
                            ShowNormalmsg("You dont have sufficient amount to buy one more share.");

//                                        minteger = quantityShr - 1;
//                                        quantity_counter.setText("" + minteger);
                            a = 1;
                        }
                    }
                }catch (Exception e){}
            }

        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                a = 0;

                if (minteger > 1) {
                    minteger = minteger - 1;
                    if (minteger > 0) {
                        quantity_counter.setText("" + minteger);
                    } else {
                        minteger = 1;
                    }
                    try {
                        quantityShr = Integer.parseInt(quantity_counter.getText().toString());
                        ttlbuyamt = buyprice * quantityShr;
                        walletremain = walletAvail - ttlbuyamt;
                        DecimalFormat dfwremain = new DecimalFormat(".##");
                        String strwrmain  =  dfwremain.format(walletremain);

                        DecimalFormat dfttlbuy = new DecimalFormat(".##");
                        String strttlbuy  =  dfttlbuy.format(ttlbuyamt);
                        txtremainblnc.setText(context.getResources().getString(R.string.euro) + strwrmain + "");
                        PriceBuydialog.setText(context.getResources().getString(R.string.euro) + strttlbuy + "");
                    } catch (Exception e) {
                    }
                }

            }
        });

        linBuybuydialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
                try {
                    if (walletAvail > 0 && !quantity_counter.getText().toString().equals("")) {
                        showcustomdialogBUYConfirm(position, String.valueOf(new DecimalFormat(".##").format(buyprice)), quantity_counter.getText().toString(), PriceBuydialog.getText().toString(), txtremainblnc.getText().toString());
                    } else {
                        ShowNormalmsg("You dont have amount in wallet \n Or \ninvalid quantity of share.");
                    }
                }catch (Exception e){
                    ShowNormalmsg("You dont have amount in wallet \n Or \ninvalid quantity of share.");

                }
            }
        });


        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }
    public void showcustomdialogBUYConfirm(int position, final String buyprice, final String quan, final String totle, final String remainbal)
    {
        a = 0;

        minteger = 1;
        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ImageView cancel;

        LinearLayout linBuybuydialogcancel, llconfirm;
        final TextView txtok,  txtshrQuan, txtttlcost, txtremainblnc, heading;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.buydialogconfirm, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtshrQuan = (TextView) m_view.findViewById(R.id.txtshrQuan);
        txtremainblnc = (TextView) m_view.findViewById(R.id.txtremainblnc);
        txtttlcost = (TextView) m_view.findViewById(R.id.txtttlcost);
        linBuybuydialogcancel = (LinearLayout) m_view.findViewById(R.id.linBuybuydialogcancel);
        llconfirm = (LinearLayout) m_view.findViewById(R.id.llconfirm);

        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        final AddPlayerBean addPlayerBean = persons.get(position);
        txtshrQuan.setText(quan +" X "+ buyprice);
        txtremainblnc.setText(""+remainbal);
        txtttlcost.setText(""+totle);
//        txtremainblnc.setText(context.getResources().getString(R.string.euro)+amt);
//        PriceBuydialog.setText(context.getResources().getString(R.string.euro)+addPlayerBean.price);


        heading.setText(addPlayerBean.p_n);

        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);
        linBuybuydialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        llconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
                try {
                    finalshr = Integer.parseInt(addPlayerBean.getPreshr()) + Integer.parseInt(quan);
                }catch (Exception e){
                    finalshr = 0+Integer.parseInt(quan);
                }
                Log.e("Q",queList.size()+"");

                if(queList.size()>0){
                    ArrayList<Integer> list = new ArrayList<>();
                    for(int i= 0; i < queList.size(); i++){
                        String qbuyPri = queList.get(i).getBuyp();
                        String qshr = queList.get(i).getShr();
                        String IntqID = queList.get(i).getId();
                        String qUserIdall = queList.get(i).getUid();

                        list.add(Integer.valueOf(IntqID));

                        if(qbuyPri.equals(buyprice) && qshr.equals(quan)){
                            System.out.println("maxso: " + Collections.max(list));
                            String FirstMatchSeller = String.valueOf(Collections.min(list));
                            System.out.println("minso: " + Collections.min(list)+" "+FirstMatchSeller+"  "+qUserIdall);

                            if(FirstMatchSeller.equals(IntqID)) {
                                String qUserId = queList.get(i).getUid();;
                                String qID = queList.get(i).getId();


                                Log.e("quser", "" + qUserId);
                                Log.e("Eql", "Eql " + queList.get(i).getUid() + " " + queList.get(i).getId());

                                try {
                                    double ttlsellget = Double.parseDouble(buyprice) * Integer.parseInt(quan);
                                    String Quamt = getamtFirebaseQU(qUserId, ttlsellget, qID);
                                    updateWallet(remainbal, user_id, "Buy");
                                    addportfolio(addPlayerBean.id, addPlayerBean.p_n, addPlayerBean.img, quan, finalshr + "", buyprice, totle, remainbal, addPlayerBean.id);
                                    ShowNormalmsg("You just bought " + quan + " " + heading.getText().toString() + " future.\n You can monitor your Futures by visiting your Portfolio.");

                                } catch (Exception e) {
                                    updateWallet(remainbal, user_id, "Buy");
                                    addportfolio(addPlayerBean.id, addPlayerBean.p_n, addPlayerBean.img, quan, finalshr + "", buyprice, totle, remainbal, addPlayerBean.id);

                                    ShowNormalmsg("Nottm You just bought " + quan + " " + heading.getText().toString() + " future.\n You can monitor your Futures by visiting your Portfolio.");
                                }
                            }

                        }
                        else {
                            //////not matched

                        updateWallet(remainbal, user_id, "Buy");
                        addportfolio(addPlayerBean.id, addPlayerBean.p_n, addPlayerBean.img, quan, finalshr + "", buyprice, totle, remainbal, addPlayerBean.id);

                        ShowNormalmsg("Nottm You just bought " + quan +  " " + heading.getText().toString() + " future.\n You can monitor your Futures by visiting your Portfolio.");
                    }
                    }

                }else {
                    updateWallet(remainbal, user_id, "Buy");
                    addportfolio(addPlayerBean.id, addPlayerBean.p_n, addPlayerBean.img, quan, finalshr + "", buyprice, totle, remainbal, addPlayerBean.id);

                    ShowNormalmsg("Nottm You just bought " + quan +  " " + heading.getText().toString() + " future.\n You can monitor your Futures by visiting your Portfolio.");
                }
                }
        });
        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }
    public void showcustomdialogSELLConfirm(int position, final String pid, final int shravil, final String shrsold, final String inssellval, final String cbuyp, final String sinsellp)
    {
        final int remainshr;
        a = 0;

        minteger = 1;

        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ImageView cancel;

        LinearLayout linBuybuydialogcancel, llconfirm;
        final TextView txtok,  txtshrsold, txtinstsellval, txtblnc, heading, txtquota;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.selldialogconfirm, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtshrsold = (TextView) m_view.findViewById(R.id.txtshrsold);
        txtinstsellval = (TextView) m_view.findViewById(R.id.txtinstsellval);
        txtblnc = (TextView) m_view.findViewById(R.id.txtblnc);
        txtquota = (TextView) m_view.findViewById(R.id.txtquota);
        linBuybuydialogcancel = (LinearLayout) m_view.findViewById(R.id.linBuybuydialogcancel);
        llconfirm = (LinearLayout) m_view.findViewById(R.id.llconfirm);

        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        final AddPlayerBean addPlayerBean = persons.get(position);
        String euro = context.getResources().getString(R.string.euro)+"";
        txtshrsold.setText(shrsold);
//        txtblnc.setText(euro+amt);
        txtinstsellval.setText(euro+inssellval);
//        txtremainblnc.setText(context.getResources().getString(R.string.euro)+amt);
//        PriceBuydialog.setText(context.getResources().getString(R.string.euro)+addPlayerBean.price);


        heading.setText(addPlayerBean.p_n);

        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);
        remainshr =  shravil - Integer.parseInt(shrsold);

        reverseTimer(30, txtquota);
        try {
            double percent98 = Double.parseDouble(inssellval) * 0.98;
            double newwalletamt = percent98 + Double.parseDouble(amt) ;
            Log.e("updatewall", newwalletamt+"  "+Double.parseDouble(amt)+" "+percent98+" "+inssellval);
            txtblnc.setText(euro+new DecimalFormat(".##").format(newwalletamt)+"");


        }catch (Exception e){
            ShowNormalmsg("Please try again.");
        }
        linBuybuydialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        llconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
                try {

                    double percent98 = Double.parseDouble(inssellval) * 0.98;
                    double newwalletamt = percent98 + Double.parseDouble(amt) ;
                    Log.e("updatewall", newwalletamt+"  "+Double.parseDouble(amt)+" "+percent98+" "+inssellval);
                    addnewselltransac(addPlayerBean.id, remainshr+"", cbuyp, sinsellp,"",addPlayerBean.p_n);
                    updateShr(String.valueOf(remainshr),pid);

                    updateWallet(new DecimalFormat(".##").format(newwalletamt)+"", user_id, "Sell.");

                }catch (Exception e){
                    ShowNormalmsg("Please try again.");
                }

//                addportfolio(addPlayerBean.id,addPlayerBean.pn,addPlayerBean.pimg,quan,buyprice,totle, remainbal,addPlayerBean.id);
//                ShowNormalmsg("You just bought "+quan+" "+heading.getText().toString()+" future.\n You can monitor your Futures by visiting your Portfolio.");
            }
        });
        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }

    public void showcustomdialogSELLQueue(int position, final String pid, int shravil, final String shrsold, final String inssellval, final String totle, final String remainbal)
    {
        final int remainshr;
        a = 0;
        final String buyprice, sellprice;

        minteger = 1;

        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        final ImageView cancel;

        LinearLayout linBuybuydialogcancel, llconfirm, linreserve;
        final TextView txtok,  txtshr, txtbuypr, txtblnc, heading;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.selldialog_queue, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtshr = (TextView) m_view.findViewById(R.id.txtshr);
        txtbuypr = (TextView) m_view.findViewById(R.id.txtbuypr);
        txtblnc = (TextView) m_view.findViewById(R.id.txtblnc);
         linBuybuydialogcancel = (LinearLayout) m_view.findViewById(R.id.linBuybuydialogcancel);
        llconfirm = (LinearLayout) m_view.findViewById(R.id.llconfirm);
        linreserve = (LinearLayout) m_view.findViewById(R.id.linreserve);

        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        final AddPlayerBean addPlayerBean = persons.get(position);
        String euro = context.getResources().getString(R.string.euro)+"";
        txtshr.setText(shrsold);
        txtblnc.setText(euro+amt);
        txtbuypr.setText(euro+persons.get(position).price);
        buyprice = persons.get(position).price;
        sellprice = persons.get(position).spri;
//        txtremainblnc.setText(context.getResources().getString(R.string.euro)+amt);
//        PriceBuydialog.setText(context.getResources().getString(R.string.euro)+addPlayerBean.price);


        heading.setText(addPlayerBean.p_n);

        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);
        remainshr =  shravil - Integer.parseInt(shrsold);

        linBuybuydialogcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        linreserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        llconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
                try {
//                    double percent98 = Double.parseDouble(inssellval) * 0.98;
//                    double newwalletamt = percent98 + Double.parseDouble(amt) ;
//                    Log.e("updatewall", newwalletamt+"  "+Double.parseDouble(amt)+" "+percent98+" "+inssellval);

                    updateShr(String.valueOf(remainshr),pid);
//                    updateWallet(new DecimalFormat(".##").format(newwalletamt)+"", user_id, "Sell");

                }catch (Exception e){
                    ShowNormalmsg("Please try again.");
                }

                addportfolioQue(addPlayerBean.id,shrsold,buyprice,sellprice, remainbal,addPlayerBean.id);
//                ShowNormalmsg("You just bought "+quan+" "+heading.getText().toString()+" future.\n You can monitor your Futures by visiting your Portfolio.");
            }
        });
        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }


    public void showcustomdialogSELL(final int position)
    {   final String id;
        minteger = 1;
        m_dialog = new Dialog(context, R.style.Dialog_No_Border);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ImageView cancel, minus, plus;
        final EditText quantity_counter;
        LinearLayout linJoinSQue, lininst;
        final TextView txtok, txtcurrbuypri, txtcurrsellpri, txtshrAvail, PriceBuydialog, instsellpri,
                txtweight, txtheight, txtbd, txtbirthplace, heading;
        ImageView imgplayer;
        LayoutInflater m_inflater = LayoutInflater.from(context);
        final View m_view = m_inflater.inflate(R.layout.selldialog, null);
        m_llMain = (LinearLayout) m_view.findViewById(R.id.cadllMain);
//        m_llMain.setBackgroundResource(R.drawable.cartbox3);
        cancel = (ImageView)m_view.findViewById(R.id.cancel);
        txtok = (TextView) m_view.findViewById(R.id.txtok);
        heading = (TextView) m_view.findViewById(R.id.heading);
        txtcurrbuypri = (TextView) m_view.findViewById(R.id.txtcurrbuypri);
        txtcurrsellpri = (TextView) m_view.findViewById(R.id.txtcurrsellpri);
        txtshrAvail = (TextView) m_view.findViewById(R.id.txtshrAvail);
        instsellpri = (TextView) m_view.findViewById(R.id.instsellpri);
        linJoinSQue = (LinearLayout) m_view.findViewById(R.id.linJoinSQue);
        lininst = (LinearLayout) m_view.findViewById(R.id.lininst);
        quantity_counter = (EditText) m_view.findViewById(R.id.quantity_counter);
        minus = (ImageView) m_view.findViewById(R.id.minus);
        plus = (ImageView) m_view.findViewById(R.id.plus);
        imgplayer = (ImageView) m_view.findViewById(R.id.imgplayer);

        final AddPlayerBean addPlayerBean = persons.get(position);
        txtcurrbuypri.setText(context.getResources().getString(R.string.euro)+addPlayerBean.price);
        txtcurrsellpri.setText(context.getResources().getString(R.string.euro)+addPlayerBean.spri);
        instsellpri.setText(context.getResources().getString(R.string.euro)+addPlayerBean.spri);
        txtshrAvail.setText(""+addPlayerBean.shr);

        heading.setText(addPlayerBean.p_n);

        id = addPlayerBean.id;
        String imgString=""+addPlayerBean.img;
        byte[] imageBytes = Base64.decode(imgString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

        imgplayer.setImageBitmap(decodedImage);
        try {
            availshrQuan = Integer.parseInt(addPlayerBean.shr);
        }catch (Exception e){}

        quantity_counter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (availshrQuan >= Integer.parseInt(quantity_counter.getText().toString())) {
//                    quantity_counter.setText("" + minteger);
                        double currentsellPri = Double.parseDouble(addPlayerBean.spri);
                        int quantityShr = Integer.parseInt(quantity_counter.getText().toString());
                        double Totleinstsellpri = currentsellPri * quantityShr;
                        instsellpri.setText(context.getResources().getString(R.string.euro) + new DecimalFormat(".##").format(Totleinstsellpri) + "");

                    } else {
                        ShowNormalmsg("You have " + addPlayerBean.shr + " shares for selling.");
                        instsellpri.setText( "");

                    }
                }catch (Exception e){
                    ShowNormalmsg("You have " + addPlayerBean.shr + " shares for selling.");
                    instsellpri.setText( "");

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    minteger = minteger + 1;
                    if (minteger > 0) {
                        if (availshrQuan >= minteger) {
                            quantity_counter.setText("" + minteger);
                            double currentsellPri = Double.parseDouble(addPlayerBean.spri);
                            int quantityShr = Integer.parseInt(quantity_counter.getText().toString());
                            double Totleinstsellpri = currentsellPri * quantityShr;
                            instsellpri.setText(context.getResources().getString(R.string.euro)+new DecimalFormat(".##").format(Totleinstsellpri) + "");

                        } else {
                            ShowNormalmsg("You dont have shares for selling.");
                            instsellpri.setText( "");

                        }
                    } else {
                        minteger = 1;
                    }
                }catch (Exception e){}
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (minteger > 1) {
                        minteger = minteger - 1;
                        if (minteger > 0) {
                            if (availshrQuan >= minteger) {
                                quantity_counter.setText("" + minteger);
                                double currentsellPri = Double.parseDouble(addPlayerBean.spri);
                                int quantityShr = Integer.parseInt(quantity_counter.getText().toString());
                                double Totleinstsellpri = currentsellPri * quantityShr;
                                instsellpri.setText(context.getResources().getString(R.string.euro)+new DecimalFormat(".##").format(Totleinstsellpri) + "");

                            } else {
                                ShowNormalmsg("You have " + addPlayerBean.shr + "shares for selling.");
                                instsellpri.setText( "");

                            }
                        } else {
                            minteger = 1;
                        }

//                    Total_amount();

                    }
                }catch (Exception e){}
            }
        });

        lininst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

                String totolsell = instsellpri.getText().toString();
                String strtxtcurrsellpri = txtcurrsellpri.getText().toString();
                totolsell = totolsell.replace(context.getResources().getString(R.string.euro)+"", "");
                strtxtcurrsellpri = strtxtcurrsellpri.replace(context.getResources().getString(R.string.euro)+"", "");
                if(totolsell.equals("") || totolsell.equals(null) || totolsell.equals("null") || totolsell.equals("0")) {
                    ShowNormalmsg("Invalid Amount.");
                } if (availshrQuan <= 0) {
                    ShowNormalmsg("Invalid Shares.");

                }else if(!strtxtcurrsellpri.equals("") || !strtxtcurrsellpri.equals(null) || !strtxtcurrsellpri.equals("null") || !strtxtcurrsellpri.equals("0"))
                {
                    Log.e("sonalins", totolsell);

                    showcustomdialogSELLConfirm(position, id, availshrQuan, quantity_counter.getText().toString(), totolsell, addPlayerBean.price, addPlayerBean.spri);
                }
            }
        });

        linJoinSQue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

                String totolsell = instsellpri.getText().toString();
                totolsell = totolsell.replace(context.getResources().getString(R.string.euro)+"", "");
                if(totolsell.equals("") || totolsell.equals(null) || totolsell.equals("null")) {
                    ShowNormalmsg("Invalid Amount.");
                }else {
                    showcustomdialogSELLQueue(position, id, availshrQuan, quantity_counter.getText().toString(), totolsell, "", amt);
                }
            }
        });


        //Change the background of the dialog according to the layout.
        /*if (p_index == BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border);
        }
        else if (p_index == ROUNDE_CORNER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.cartbox3);
        }
        else if (p_index == ROUNDE_CORNER_BORDER_DIALOG)
        {
            m_llMain.setBackgroundResource(R.drawable.btn_style_border_roundcorner);
        }
*/
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        txtok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();

            }
        });


        m_dialog.setContentView(m_view);
        m_dialog.show();
        m_dialog.setCancelable(true);
        m_dialog.setCanceledOnTouchOutside(true);

    }

    private void updateShr(final String shr, String pid) {

//        Map<String, Object> note = new Note(title, content).toMap();
        Map<String, Object> data1 = new HashMap<>();
        data1.put("shr", shr);
//        data1.put("amt", amt);
        firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio").document(pid)
                .update(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sonaliwa", "Error adding Note document", e);

                    }
                });
    }
    private void updateWallet(String newamt, String uid, final String frmbuysell) {
        String euro = context.getResources().getString(R.string.euro)+"";

        newamt = newamt.replace(euro+"", "");
//        Map<String, Object> note = new Note(title, content).toMap();
        Map<String, Object> data1 = new HashMap<>();
        data1.put("amt", newamt);
//        data1.put("amt", amt);
        firestoreDB.collection("UserInfo").document(user_id).collection("Wallet").document("W"+user_id)
                .update(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                if(frmbuysell.equals("Buy")){
                    ShowNormalmsg("Successfully Buy shares.");

                }else {
                    ShowNormalmsg("Successfully sell this shares.");
                }
            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sonaliwa", "Error adding Note document", e);

                    }
                });
    }

    public void ShowNormalmsg(final String msg){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.getContext().setTheme(R.style.AlertThem);

        builder.setMessage(msg)
                .setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if(msg.equals("Added in Portfolio.")){
                    intentcall(context, HomeActivity.class);
                }else if(msg.equals("Successfully sell this shares.")){
                    intentcall(context, HomeActivity.class);
                }
            }
        });

        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();

    }
    private void addportfolio(final String id, final String pn, String pimg, final String shrforlist, final String shr, final String buyp, final String ttlbp, String bremain, String pid) {

            PortfolioBean portfolioBean = new PortfolioBean(id, pn, pimg, shr, buyp, ttlbp, bremain, pid);

//        Map<String, Object> note = new Note(title, content).toMap();
//        Map<String, Object> data1 = new HashMap<>();
//        data1.put("Id", wid);
//        data1.put("amt", amt);
            firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio").document(pid)
                    .set(portfolioBean).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    addInListbuyprice(id, shrforlist, buyp, ttlbp, "", pn);

                    ShowNormalmsg("Added in Portfolio.");
                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("sonaliwa", "Error adding Note document", e);
                            Toast.makeText(context, "Note could not be added!", Toast.LENGTH_SHORT).show();
                        }
                    });

    }
    private void addportfolioQue(final String id,  final String shr, final String buyp, final String sellp, String bremain, String pid) {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
       String CurrentDate = df.format(c);
//        PortfolioBean portfolioBean = new PortfolioBean(id, pn,pimg, shr, buyp, ttlbp, bremain, pid);
        String queid = timestamp();

//        Map<String, Object> note = new Note(title, content).toMap();
        Map<String, Object> data1 = new HashMap<>();
        data1.put("Id", queid);
         data1.put("uid",user_id);
        data1.put("pid",pid);
        data1.put("sts", "0");
        data1.put("buyp", buyp);
        data1.put("sellp", sellp);
        data1.put("shr", shr);
        data1.put("tranctdate", CurrentDate);
//        data1.put("amt", amt);
        firestoreDB.collection("SellQueue").document(queid)
                .set(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
//                addInListbuyprice(id,shr,buyp,ttlbp, "","");

                ShowNormalmsg("Added in Queue.");
            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sonaliwa", "Error adding Note document", e);
                        Toast.makeText(context, "Note could not be added!", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void addInListbuyprice(String id,  String shr, String buyp, String ttlbp, String bremain, String pn) {


//        Map<String, Object> note = new Note(title, content).toMap();
        Map<String, Object> data1 = new HashMap<>();
//        data1.put("Id", randomId()+"");
//        data1.put("amt", amt);
        data1.put("buyp", buyp);
        data1.put("ttlbp", ttlbp);
        data1.put("shr", shr);
        data1.put("tranctTime", timforsave());
        data1.put("pn", pn);

//        data1.put("Id", id);
        firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio").document(id).collection("AgainBuyLIst").document(timestamp()+"")
                .set(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
//                ShowNormalmsg("Added in Portfolio.");
            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sonaliwa", "Error adding buyagain document", e);
//                        Toast.makeText(context, "Note could not be added!", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void addnewselltransac(String id,  String shr, String buyp, String sellp, String bremain, String pn) {


//        Map<String, Object> note = new Note(title, content).toMap();
        Map<String, Object> data1 = new HashMap<>();
//        data1.put("Id", randomId()+"");
//        data1.put("amt", amt);
        data1.put("buyp", buyp);
        data1.put("sellp", sellp);
        data1.put("shr", shr);
        data1.put("tranctTime", timforsave());
        data1.put("pn", pn);

//        data1.put("Id", id);
        firestoreDB.collection("UserInfo").document(user_id).collection("Portfolio").document(id).collection("Sellorder").document(timestamp()+"")
                .set(data1).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
//                ShowNormalmsg("Added in Portfolio.");
            }
        })
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());
//                        Toast.makeText(getApplicationContext(), "Note has been added!", Toast.LENGTH_SHORT).show();
//                    }
//                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("sonaliwa", "Error adding buyagain document", e);
//                        Toast.makeText(context, "Note could not be added!", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    public void intentcall(Context from, Class to){
        Intent i = new Intent(context, to);
        context.startActivity(i);
    }

    public void reverseTimer(int Seconds,final TextView tv){

        new CountDownTimer(Seconds* 1000+1000, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                tv.setText(String.format("%02d", seconds)+" sec");
//                tv.setText("TIME : " + String.format("%02d", minutes)
//                        + ":" + String.format("%02d", seconds));
            }

            public void onFinish() {
                tv.setText("Completed");
                m_dialog.dismiss();
            }
        }.start();
    }

    public String timestamp(){
        Long tsLong = System.currentTimeMillis()/1000;
        String ts = tsLong.toString();
        return ts;
    }
    public String timforsave(){
        Date date = new Date();
//        long time = date.getTime();
        Long time = System.currentTimeMillis();

        System.out.println("time in miliseconds  "+time);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        String datestring = formatter.format(new Date(time));
        Log.e("ttime", datestring+" d");
        return time.toString();
    }


    void sendMessageToCloudFunction(HttpUrl.Builder httpBuilder, final String qIDForDel) {
        OkHttpClient httpClient = new OkHttpClient();
        Request request = new Request.Builder().url(httpBuilder.build()).build();

        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("FireFunction", "error response firebase cloud functions");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, "Action failed, please try again", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                ResponseBody responseBody = response.body();
                String resp = "";
                if (!response.isSuccessful()){
                    Log.e("FireFunction", "action failed");
                    resp = "Failed perform the action, please try again";
                } else {
                    try {
                        resp = responseBody.string();
                        Log.e("FireFunction", "Response " + resp);
                    } catch (IOException e){
                        resp = "Problem in reading resposne";
                        Log.e("FireFunction", "Problem in reading response " + e);
                    }
                }

                runOnUiThread(responseRunnable(resp, qIDForDel));
            }
        });
    }
//    abstract Runnable responseRunnable(final String responseStr);


    ////add function sell wallet
    private void registration(String user_id, String amtfunctSell, String  walletid, String qID) {

        String name = user_id;
        String email = user_id;
        String password = amtfunctSell;
        String qIDForDel = qID;


        HttpUrl.Builder httpBuilder = prepareRegRequestBuilder(name, email, password);
        sendMessageToCloudFunction(httpBuilder, qIDForDel);
    }

    private HttpUrl.Builder prepareRegRequestBuilder(String name, String email, String password){
        HttpUrl.Builder httpBuider =
                HttpUrl.parse(FIREBASE_CLOUD_FUNCTION_REG_URL).newBuilder();
        httpBuider.addQueryParameter("name", name);
//        httpBuider.addQueryParameter("email", "optimumanddev2@gmail.com");
        httpBuider.addQueryParameter("email", email);
        httpBuider.addQueryParameter("amt", password);
        return httpBuider;
    }

    public Runnable responseRunnable(final String responseStr, final String qIDForDel) {
        Runnable resRunnable = new Runnable() {
            public void run() {
                Log.d("firefuncRegis", responseStr);
                //login success
                if(responseStr.contains("token")){
                    //retrieve token from response and save it in shared preference
                    //so that token can be sent in the request to services

                    String tokenStr[] = responseStr.split(":");
                    Log.d("firefuncRegis", tokenStr[1]);
                   /* SharedPreferences sharedPref = PreferenceManager.
                            getDefaultSharedPreferences(
                                    RegistrationLoginActivity.this.getApplication());

                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(getString(R.string.auth_email), loginEmail);
                    editor.putString(getString(R.string.auth_token), tokenStr[1]);
                    editor.commit();

                    Toast.makeText(RegistrationLoginActivity.this,
                            "Login Successful.",
                            Toast.LENGTH_SHORT).show();

                    clearUI();
                    getAccountInfo();*/
                }else if(responseStr.contains("account created")){

                    delsellqcompleted(qIDForDel);
                    Toast.makeText(context,
                            "wallet Updated of seller",
                            Toast.LENGTH_SHORT).show();

//                    clearUI();
//                    showLogin();
                }else {
                    Toast.makeText(context,
                            responseStr,
                            Toast.LENGTH_SHORT).show();
                }
            }
        };
        return resRunnable;
    }

    public void delsellqcompleted (String sellQId){
        firestoreDB.collection("SellQueue").document(sellQId)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("qdelcom", "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("qdelcom", "Error deleting document", e);
                    }
                });

    }

    ///////get amt of sell q user
    public String getamtFirebaseQU(final String email, final Double totalamtget, final String qID){
        Log.e("getmtuser",email);
        final String[] stramtQU = new String[1];
        DocumentReference docRef = firestoreDB.collection("UserInfo").document(email).collection("Wallet").document("W"+email);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                String sUniqIdQU="0",sUPassword="0",sEmail="0", phn ="0", cntry = "",stts = "",id;

                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();

                    if (document.exists()) {
                        stramtQU[0] = "" + document.getData().get("amt");
                        id = "" + document.getData().get("Id");
                        sUniqIdQU=""+document.getId();
                        Log.e("sUniqIdamt:--",""+sUniqIdQU+" "+ stramtQU[0]);

                        Log.d("sonaQUamt", "DocumentSnapshot data: " + document.getData()+"  "+totalamtget);
                        try {

                            double percent98 = totalamtget * 0.98;
                            String wallamtseller = stramtQU[0];
                            double newwalletamt = percent98 + Double.parseDouble(wallamtseller) ;
                            Log.e("ttlamtgetbysellRandomly", totalamtget+"  "+newwalletamt+"  "+Double.parseDouble(wallamtseller)+" "+percent98+" "+totalamtget);

                            registration(email, new DecimalFormat(".##").format(newwalletamt)+"","W"+email, qID);

                        }catch (Exception e){
                            ShowNormalmsg("Please try again.");
                        }

                    } else {
                        Log.d("sonaQUamt", "No such document");
                    }
                } else {
                    Log.d("sonaQUamt", "get failed with ", task.getException());
                }
            }
        });
        return stramtQU[0];
    }
}

